/*
 * ThreadPolicyImpl.cpp
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "ThreadPolicyImpl.h"

namespace elw { namespace threading {

ThreadPolicyImpl::ThreadPolicyImpl(void) throw() :
    m_hThread(0)
{
}

ThreadPolicyImpl::~ThreadPolicyImpl(void) throw()
{
}

void ThreadPolicyImpl::start(IRunnable& rRunnable)
{
    if (0 != ::pthread_create(&m_hThread, NULL, main, &rRunnable))
        throw lang::SystemErrorException("::pthread_create");

}

void ThreadPolicyImpl::join() throw()
{
    if (m_hThread == 0)
        return;
    ::pthread_join(m_hThread, NULL);
    m_hThread = 0;
}

dword ThreadPolicyImpl::getid() const throw()
{
    return (dword) m_hThread;
}

//////////////////////////////////////////////////////////////////////////

void* ThreadPolicyImpl::main(void* pvParam)
{
    IRunnable* pThis = (IRunnable*)pvParam;
    pThis->run();
    pthread_exit(NULL);

    return NULL;
}

//////////////////////////////////////////////////////////////////////////

void sleep(dword dwMiliseconds) throw()
{
    struct timespec timeout = {
            (__time_t)(dwMiliseconds / 1000),
            (long int)((dwMiliseconds % 1000) * 1000 * 1000)
            };
    ::nanosleep(&timeout, NULL);
}

dword selfid(void) throw()
{
    return (dword) ::pthread_self();
}

dword tick(void) throw()
{
    struct timespec ts = { 0 };
    if(::clock_gettime(CLOCK_MONOTONIC, &ts) != 0)
        return 0;
    return (ts.tv_sec * 1000) + (ts.tv_nsec/1000000);
}

void yield(void) throw()
{
    ::sched_yield();
}

//REGISTER_ELW_THREAD_POLICY_IMPL(ThreadPolicyImpl);

}}
