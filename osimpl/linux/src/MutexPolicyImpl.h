/*
 * MutexPolicyImpl.h
 *
 *  Created on: Sep 30, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingMutexPolicyImpl_h__
#define ElwingThreadingMutexPolicyImpl_h__

namespace elw { namespace threading {

class ELW_HIDE MutexPolicyImpl :
    public IMutexPolicy
{
    pthread_mutex_t m_mtx;

public:
    MutexPolicyImpl() throw();
    virtual ~MutexPolicyImpl() throw();

public:
    bool create(mutex::Type t) throw();

public:
    bool lock() throw();
    bool unlock() throw();

    mutex::Result trylock() throw();
    mutex::Result trylock(dword dwmilis) throw();

    void* getobj() throw();
};

}}

#endif /* ElwingMutexPolicyImpl_h__ */
