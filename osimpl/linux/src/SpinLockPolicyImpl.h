/*
 * SpinLockPolicyImpl.h
 *
 *  Created on: Oct 7, 2011
 *      Author: altar
 */

#ifndef ElwingThreadingSpinLockImpl_h__
#define ElwingThreadingSpinLockImpl_h__

namespace elw { namespace threading {

class ELW_HIDE SpinLockPolicyImpl :
    public ISpinLockPolicy
{
    pthread_spinlock_t m_spin;
public:
    SpinLockPolicyImpl();
    virtual ~SpinLockPolicyImpl() throw();

    void lock() throw();
    void unlock() throw();
    bool trylock() throw();
};

/*
class SpinLockPolicyImpl :
    public elw::ISpinLockPolicy
{
    pthread_mutex_t m_mtx;
public:
    SpinLockPolicyImpl();
    virtual ~SpinLockPolicyImpl();

    void lock();
    void unlock();
    bool tryLock();
};
*/
}}

#endif /* SPINLOCKPOLICYIMPL_H_ */
