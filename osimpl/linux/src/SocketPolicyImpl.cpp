/*
 * SocketPolicyImpl.cpp
 *
 *  Created on: Oct 13, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net { namespace sock {

bool close(Handle hSocket)
{
    return (-1 != ::close(hSocket));
}

dword error()
{
    return errno;
}

bool ioctl(Handle hSocket, dword dwRequest, void* pv)
{
    return -1 != ::ioctl(hSocket, dwRequest, pv);
}

const Family af_unspec                      = Family(PF_UNSPEC);
const Family af_ipv4                        = Family(PF_INET);
const Family af_ipv6                        = Family(PF_INET6);
const Family af_local                       = Family(PF_LOCAL);
const Family af_netlink                     = Family(PF_NETLINK);
const Family af_packet                      = Family(PF_PACKET);
const Family af_can                         = Family(PF_CAN);

const Type sock_stream                      = Type(SOCK_STREAM);
const Type sock_datagram                    = Type(SOCK_DGRAM);
const Type sock_raw                         = Type(SOCK_RAW);
const Type sock_rdm                         = Type(SOCK_RDM);
const Type sock_seqpacket                   = Type(SOCK_SEQPACKET);

const Protocol ipproto_tcp                  = Protocol(IPPROTO_TCP);
const Protocol ipproto_udp                  = Protocol(IPPROTO_UDP);

const Shutmode sm_r                         = Shutmode(SHUT_RD);
const Shutmode sm_w                         = Shutmode(SHUT_WR);
const Shutmode sm_rw                        = Shutmode(SHUT_RDWR);

const Flag msg_none                         = Flag(0);
const Flag msg_oob                          = Flag(MSG_OOB);
const Flag msg_dontroute                    = Flag(MSG_DONTROUTE);
const Flag msg_dontwait                     = Flag(MSG_DONTWAIT);
const Flag msg_nosignal                     = Flag(MSG_NOSIGNAL);
const Flag msg_peek                         = Flag(MSG_PEEK);
const Flag msg_waitall                      = Flag(MSG_WAITALL);
const Flag msg_trunc                        = Flag(MSG_TRUNC);
const Flag msg_errqueue                     = Flag(MSG_ERRQUEUE);

//const Interface any         = Interface(INADDR_ANY);
//const Interface loopback    = Interface(INADDR_LOOPBACK);

const AddrInfo ai_passive                   = AddrInfo(AI_PASSIVE);
const AddrInfo ai_canonname                 = AddrInfo(AI_CANONNAME);
const AddrInfo ai_numhost                   = AddrInfo(AI_NUMERICHOST);
const AddrInfo ai_v4mapped                  = AddrInfo(AI_V4MAPPED);
const AddrInfo ai_all                       = AddrInfo(AI_ALL);
const AddrInfo ai_addrconf                  = AddrInfo(AI_ADDRCONFIG);

const Error err_egain                       = Error(EAGAIN);

const Handle invalid_handle                 = Handle(-1);

}
}
}
