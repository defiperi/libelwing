/*
 * SpinLockPolicyImpl.cpp
 *
 *  Created on: Oct 7, 2011
 *      Author: altar
 */

#include "stdafx.h"
#include "SpinLockPolicyImpl.h"

namespace elw { namespace threading {

SpinLockPolicyImpl::SpinLockPolicyImpl()
{
    if (0 != ::pthread_spin_init(&m_spin, 0))
        throw lang::SystemErrorException("::pthread_spin_init()");
}

SpinLockPolicyImpl::~SpinLockPolicyImpl() throw()
{
    ::pthread_spin_destroy(&m_spin);
}

void SpinLockPolicyImpl::lock() throw()
{
    ::pthread_spin_lock(&m_spin);
}

void SpinLockPolicyImpl::unlock() throw()
{
    ::pthread_spin_unlock(&m_spin);
}

bool SpinLockPolicyImpl::trylock() throw()
{
    return 0 == ::pthread_spin_trylock(&m_spin);
}
/*

SpinLockPolicyImpl::SpinLockPolicyImpl()
{
    ::pthread_mutex_init(&m_mtx, NULL);
}

SpinLockPolicyImpl::~SpinLockPolicyImpl()
{
    ::pthread_mutex_destroy(&m_mtx);
}

void SpinLockPolicyImpl::lock()
{
    ::pthread_mutex_lock(&m_mtx);
}

void SpinLockPolicyImpl::unlock()
{
    ::pthread_mutex_unlock(&m_mtx);
}

bool SpinLockPolicyImpl::tryLock()
{
    return 0 == ::pthread_mutex_trylock(&m_mtx);
}
*/
//REGISTER_ELW_SPIN_LOCK_POLICY_IMPL(SpinLockPolicyImpl);

}}
