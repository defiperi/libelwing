/*
 * MutexPolicyImpl.cpp
 *
 *  Created on: Sep 30, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "MutexPolicyImpl.h"

namespace elw { namespace threading {

namespace mutex { namespace type {

const Type recursive    = Type(PTHREAD_MUTEX_RECURSIVE_NP);
const Type fast         = Type(PTHREAD_MUTEX_FAST_NP);
const Type timed        = Type(PTHREAD_MUTEX_TIMED_NP);

}}

static mutex::Result __result__(int iRslt)
{
    switch (iRslt)
    {
    case 0:
        return mutex::success;
    case EBUSY:
        return mutex::busy;
    case ETIMEDOUT:
        return mutex::timedout;
    }

    return mutex::failure;
}

MutexPolicyImpl::MutexPolicyImpl() throw()
{
}

MutexPolicyImpl::~MutexPolicyImpl() throw()
{
    ::pthread_mutex_destroy(&m_mtx);
}

bool MutexPolicyImpl::create(mutex::Type t) throw()
{
    pthread_mutexattr_t attr;

    ::pthread_mutexattr_init(&attr);
    ::pthread_mutexattr_settype(&attr, t);
    bool fRslt = 0 == ::pthread_mutex_init(&m_mtx, &attr);
    ::pthread_mutexattr_destroy(&attr);
    return fRslt;
}

bool MutexPolicyImpl::lock() throw()
{
    return 0 == ::pthread_mutex_lock(&m_mtx);
}

bool MutexPolicyImpl::unlock() throw()
{
    return 0 == ::pthread_mutex_unlock(&m_mtx);
}

mutex::Result MutexPolicyImpl::trylock() throw()
{
    return __result__(::pthread_mutex_trylock(&m_mtx));
}

mutex::Result MutexPolicyImpl::trylock(dword dwmilis) throw()
{
    struct timespec ts;
    struct timeval tv;
    ::gettimeofday(&tv, NULL);
    ts.tv_sec = tv.tv_sec + dwmilis / 1000;
    ts.tv_nsec = tv.tv_usec*1000 + (dwmilis%1000) * 1000000;
    if (ts.tv_nsec >= 1000000000)
    {
        ts.tv_nsec -= 1000000000;
        ts.tv_sec++;
    }

    return __result__(::pthread_mutex_timedlock(&m_mtx, &ts));
}

void* MutexPolicyImpl::getobj() throw()
{
    return &m_mtx;
}

//REGISTER_ELW_MUTEX_POLICY_IMPL(MutexPolicyImpl);

}}
