/*
 * ISocketPolicy.h
 *
 *  Created on: Oct 13, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingISocketPolicy_h__
#define ElwingISocketPolicy_h__

namespace elw { namespace net { namespace sock {

enum __Family { };
enum __Type { };
enum __Protocol { };
enum __Interface { };
enum __AddrInfo { };
enum __Flag { };
enum __Option {};
enum __Shutmode { };
enum __Error { };

enum Family { };
enum Type { };
enum Protocol { };
enum Interface { };
enum AddrInfo { };
enum Flag { };
enum Option {};
enum Shutmode { };
enum Error { };

//typedef __Family      Family;
//typedef __Type        Type;
//typedef __Protocol    Protocol;
//typedef __AddrInfo    AddrInfo;
//typedef __Flag        Flag;
//typedef __Option      Option;
//typedef __Shutmode    Shutmode;
//typedef __Error       Error;

ELW_EXPORT extern const Family af_unspec;
ELW_EXPORT extern const Family af_ipv4;
ELW_EXPORT extern const Family af_ipv6;
ELW_EXPORT extern const Family af_local;
ELW_EXPORT extern const Family af_netlink;
ELW_EXPORT extern const Family af_packet;
ELW_EXPORT extern const Family af_can;

ELW_EXPORT extern const Type sock_stream;
ELW_EXPORT extern const Type sock_datagram;
ELW_EXPORT extern const Type sock_raw;
ELW_EXPORT extern const Type sock_rdm;
ELW_EXPORT extern const Type sock_seqpacket;

ELW_EXPORT extern const Protocol ipproto_tcp;
ELW_EXPORT extern const Protocol ipproto_udp;

ELW_EXPORT extern const Shutmode sm_r;
ELW_EXPORT extern const Shutmode sm_w;
ELW_EXPORT extern const Shutmode sm_rw;

ELW_EXPORT extern const Flag msg_none;
ELW_EXPORT extern const Flag msg_oob;
ELW_EXPORT extern const Flag msg_dontroute;
ELW_EXPORT extern const Flag msg_dontwait;
ELW_EXPORT extern const Flag msg_nosignal;
ELW_EXPORT extern const Flag msg_peek;
ELW_EXPORT extern const Flag msg_waitall;
ELW_EXPORT extern const Flag msg_trunc;
ELW_EXPORT extern const Flag msg_errqueue;

//extern const Interface inetaddr_any;
//extern const Interface inetaddr_loopback;

ELW_EXPORT extern const AddrInfo ai_passive;
ELW_EXPORT extern const AddrInfo ai_canonname;
ELW_EXPORT extern const AddrInfo ai_numhost;
ELW_EXPORT extern const AddrInfo ai_v4mapped;
ELW_EXPORT extern const AddrInfo ai_all;
ELW_EXPORT extern const AddrInfo ai_addrconf;

ELW_EXPORT extern const Error err_egain;

ELW_EXPORT extern const Handle invalid_handle;

dword ELW_HIDE error();

bool ELW_HIDE close(
        Handle hSocket
        );

bool ELW_HIDE ioctl(
        Handle hSocket,
        dword dwRequest,
        void* pvArgs
        );

}
}
}

#endif /* ElwingISocketPolicy_h__ */
