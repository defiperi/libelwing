/*
 * IAtomicIntegerPolicy.h
 *
 *  Created on: Oct 6, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingIAtomicIntegerPolicy_h__
#define ElwingThreadingIAtomicIntegerPolicy_h__

namespace elw { namespace threading { namespace atomic {

ELW_EXPORT long add(long volatile* l0,  long l1) throw();
ELW_EXPORT long set(long volatile* l0, long l1) throw();

}}}

#if 0

class ELW_EXPORT IAtomicIntegerPolicy :
    public lang::IUnknown
{
public:
    virtual long increment(long iVal) throw() = 0;
    virtual long decrement(long iVal) throw() = 0;

    virtual long getValue() throw() = 0;
    virtual long setValue(long iVal) throw() = 0;
};

#define ELW_ATOMIC_INTEGER_POLICY_IMPL_KEYWORD  "elw::threading::AtomicIntegerPolicyImpl"
#if 0
#define REGISTER_ELW_ATOMIC_INTEGER_POLICY_IMPL(__impl) \
    REGISTER_CLASS(\
        ELW_ATOMIC_INTEGER_POLICY_IMPL_KEYWORD,\
        IAtomicIntegerPolicy,\
        __impl,\
        false,\
        ClassFactory::eStandart\
        )
#endif
}}
#endif
#endif /* ElwingIAtomicIntegerPolicy_h__ */
