/*
 * IThread.h
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwThreadingIThread_h__
#define ElwThreadingIThread_h__

namespace elw { namespace threading {

class ELW_EXPORT IRunnable :
    public lang::IUnknown
{
public:
    virtual void run() = 0;
};

class ELW_EXPORT IThreadPolicy :
    public lang::IUnknown
{
public:
    virtual void    start(IRunnable& rRunnable) = 0;
    virtual void    join() throw() = 0;
    virtual dword   getid() const throw() = 0;
};

ELW_EXPORT void    sleep(dword dwMiliseconds) throw();
ELW_EXPORT dword   selfid() throw();
ELW_EXPORT dword   tick() throw();
ELW_EXPORT void    yield() throw();


#define ELW_THREAD_POLICY_IMPL_KEYWORD   ("elw::threading::ThreadPolicyImpl")
#if 0
#define REGISTER_ELW_THREAD_POLICY_IMPL(__impl) \
    REGISTER_CLASS(\
        ELW_THREAD_POLICY_IMPL_KEYWORD,\
        IThreadPolicy,\
        __impl,\
        false,\
        ClassFactory::eStandart\
        )
#endif
}}

#endif /* ElwingIThread_h__ */
