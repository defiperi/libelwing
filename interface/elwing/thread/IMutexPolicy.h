/*
 * IMutexPolicy.h
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingIMutexPolicy_h__
#define ElwingThreadingIMutexPolicy_h__

namespace elw { namespace threading {

namespace mutex {

    enum Type {};
    enum Result {
        success,
        failure,
        busy,
        timedout
    };

namespace type {
    extern const Type recursive;
    extern const Type fast;
    extern const Type timed;
}

}

class ELW_EXPORT IMutexPolicy :
    public lang::IUnknown
{
public:
    virtual bool create(mutex::Type t) throw() = 0;
public:
    virtual bool lock() throw() = 0;
    virtual bool unlock() throw() = 0;

    virtual mutex::Result trylock() throw() = 0;
    virtual mutex::Result trylock(dword dwmilis) throw() = 0;

    virtual void* getobj() throw() = 0;
};

#define ELW_MUTEX_POLICY_IMPL_KEYWORD   ("elw::threading::MutexPolicyImpl")

}}

#endif /* ElwingIMutexPolicy_h__ */
