/*
 * Exception.h
 *
 *  Created on: Sep 30, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingLangException_h__
#define ElwingLangException_h__

namespace elw  { namespace lang {

class ELW_EXPORT Exception :
    public std::exception
{
    std::string m_strDescription;
    std::string m_strLocation;
    std::string m_strWhat;

public:
    Exception() throw();
    Exception(std::string const& str) throw();

    virtual ~Exception() throw();

    virtual pcstr what() const throw();

    pcstr location() const throw();
    pcstr description() const throw();

public:
    void location(std::string const& strFile, int iLine) throw();
};

template <class _TyException>
inline void __throw__(_TyException const& e, std::string const& f, int l)
{
    const_cast<_TyException&>(e).location(f, l);
    throw e;
}

class ELW_EXPORT ClassNotFoundException :
    public Exception
{
public:
    ClassNotFoundException(std::string const& str) throw();
};

class ELW_EXPORT SystemErrorException :
    public Exception
{
public:
    SystemErrorException() throw();
    SystemErrorException(std::string const& str) throw();
};


class ELW_EXPORT OutOfBoundsException :
    public Exception
{
public:
    OutOfBoundsException() throw();
    OutOfBoundsException(std::string const& msg) throw();
};

class ELW_EXPORT IllegalStateException :
    public Exception
{
public:
    IllegalStateException() throw();
    IllegalStateException(std::string const& msg) throw();
};

}}

#define elw_throw_exception(e) \
    do  { elw::lang::__throw__(e, __FILE__, __LINE__); } while(0)

#endif /* ElwingException_h__ */
