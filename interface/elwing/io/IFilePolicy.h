/*
 * IFilePolicy.h
 *
 *  Created on: Oct 10, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIFilePolicy_h__
#define ElwingIFilePolicy_h__

namespace elw { namespace io {

namespace base
{
    enum Type
    {
        file        = 0x01,
        dir         = 0x02,
        dev         = 0x03,

        headen      = 0xe0,
        link        = 0xf0,
    };
}

class ELW_EXPORT IFilePolicy :
    public lang::IUnknown
{
public:
    virtual int getType(
            const std::string& strPath
            ) = 0;

    virtual dword getLength(
            const std::string& strPath
            ) const = 0;

    virtual bool canRead(
            const std::string& strPath
            ) const = 0;

    virtual bool canWrite(
            const std::string& strPath
            ) const = 0;

    virtual bool canExecute(
            const std::string& strPath
            ) const = 0;

    virtual bool setReadable(
            const std::string& strPath,
            bool fReadable,
            bool fOwner
            ) = 0;

    virtual bool setWritable(
            const std::string& strPath,
            bool fWritable,
            bool fOwner
            ) = 0;

    virtual bool setExecutable(
            const std::string& strPath,
            bool fExecutable,
            bool fOwner
            ) = 0;

    virtual bool mkdir(
            const std::string& strPath
            ) = 0;

    virtual bool create(
            const std::string& strPath
            ) = 0;

    virtual bool mklink(
            const std::string& strSrc,
            const std::string& strDst
            ) = 0;

    virtual bool remove(
            const std::string& strPath
            ) = 0;

    virtual bool move(
            const std::string& strPath,
            const std::string& strDst
            ) = 0;

    virtual void readDir(
            const std::string& strPath,
            std::vector<std::string>& lst,
            const std::string& strFilter
            ) = 0;

    virtual bool readLink(
            const std::string& strPath,
            std::string& strLink
            ) = 0;

    virtual bool realPath(
            const std::string& strPath,
            std::string& strReal
            ) = 0;
};

#define ELW_FILE_POLICY_IMPL_KEYWORD ("elw::io::FilePolicyImpl")
#if 0
#define REGISTER_ELW_FILE_POLICY_IMPL(__impl) \
    REGISTER_CLASS(\
        ELW_FILE_POLICY_IMPL_KEYWORD,\
        IFilePolicy,\
        __impl,\
        false,\
        ClassFactory::eSingelton\
        )
#endif
}

namespace path {

ELW_EXPORT std::string getwd();
ELW_EXPORT bool chdir(const std::string& str);
ELW_EXPORT std::string root();
ELW_EXPORT std::string sep();

}
}

#endif /* ElwingIFilePolicy_h__ */
