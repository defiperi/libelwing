/*
 * BasicArray001.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "BasicArray001.h"


std::string BasicArray001::getDescription() const
{
    return "Buffer";
}

bool BasicArray001::run()
{
    std::string str0("0123456789");

    elw::lang::Buffer b0(2);
    b0.resize(str0.length());
    memcpy(b0.ptr(), str0.c_str(), str0.length());
    b0.count(str0.length());

    elw::lang::Buffer b1(b0);

    if (b0.length() != str0.length())
        return false;

    if (std::string(b0.ptr(), b0.length()) != str0)
        return false;

    uint j = 0;
    for (uint i = str0.length()-1; i > 0 ; --i, ++j)
        b0[j] = str0[i];
    b0[j] = str0[0];

    if (b0.length() != str0.length())
        return false;

    if (std::string(b0.ptr(), b0.length()) != "9876543210")
        return false;

    b0 += b1;

    if (std::string(b0.ptr(), b0.length()) != "98765432100123456789")
        return false;

    if (b0.length() != std::string("98765432100123456789").length())
        return false;

    b0.reset();

    if (b0.length() != 0)
        return false;

    b0 = b1;

    if (b0.length() != str0.length())
        return false;

    if (std::string(b0.ptr(), b0.length()) != str0)
        return false;

    char buff[4096];
    for (uint i = 0; i < sizeof(buff); ++i)
        buff[i] = (char)i;

    b0.reset();
    b0.resize(4096);

    memcpy(b0.ptr(), buff, sizeof(buff));

    b0.count(sizeof(buff));

    if (b0.length() != sizeof(buff))
        return false;

    if (0 != memcmp(b0.ptr(), buff, sizeof(buff)))
        return false;

    b0.reset();
    dword dw0 = 1550;
    memcpy(b0.ptr(), &dw0, sizeof(dw0));
    b0.count(sizeof(dw0));

    dword dw1 = *(dword*)b0.ptr();
    if (dw0 != dw1)
        return false;

    return true;
}
