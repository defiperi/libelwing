/*
 * Timer001.cpp
 *
 *  Created on: Jul 20, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Timer001.h"

std::string Timer001::getDescription() const
{
    return "Basically timer test.";
}

bool Timer001::run()
{
    class TimerTask : public elw::threading::Timer::Task
    {
        int m_iExpireCnt;
    public:
        int getExpireCnt() const
        {
            return m_iExpireCnt;
        }

    public:
        void onExpire()
        {
            ++m_iExpireCnt;
        }

        TimerTask() : m_iExpireCnt(0)
        {

        }
    };

    elw::threading::Timer t;

    for (int i = 0; i < 2; ++i)
    {

        TimerTask task0, task1;
        t.start(task0, 100);
        t.start(task1, 250);

        elw::threading::sleep(2050);
        t.cancel();

        elw::threading::sleep(500);

        if (task0.getExpireCnt() != 20)
        {
            std::cout << "task0 expected 20 ->" << task0.getExpireCnt() << std::endl;
            return false;
        }

        if (task1.getExpireCnt() != 8)
        {
            std::cout << "task1 expected 8 ->" << task0.getExpireCnt() << std::endl;
            return false;
        }
    }

    return true;
}

