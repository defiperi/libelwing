/*
 * Timer004.cpp
 *
 *  Created on: Jul 20, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Timer004.h"

std::string Timer004::getDescription() const
{
    return "Basically timer test.";
}

bool Timer004::run()
{
    class TimerTask : public elw::threading::Timer::Task
    {
        int m_iExpireCnt;
        elw::threading::Event& m_evnt;
    public:
        int getExpireCnt() const
        {
            return m_iExpireCnt;
        }

    public:
        void onExpire()
        {
            m_evnt.notify();
            elw::threading::sleep(1000);
            std::cout << "onExpire exit" << std::endl;
        }

        TimerTask(elw::threading::Event& evnt) : m_iExpireCnt(0), m_evnt(evnt)
        {

        }
    };

    elw::threading::Event evnt;
    elw::threading::Timer timer;
    TimerTask* t = new TimerTask(evnt);;
    timer.start(*t, 100);
    evnt.wait();
    std::cout << "delete enter" << std::endl;
    delete t;
    std::cout << "delete exit" << std::endl;

    return true;
}

