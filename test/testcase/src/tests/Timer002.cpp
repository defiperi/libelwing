/*
 * Timer002.cpp
 *
 *  Created on: Jul 20, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Timer002.h"

std::string Timer002::getDescription() const
{
    return "timer test. task cancel";
}

bool Timer002::run()
{
    class TimerTask : public elw::threading::Timer::Task
    {
        int m_iExpireCnt;
    public:
        int getExpireCnt() const
        {
            return m_iExpireCnt;
        }

    public:
        void onExpire()
        {
            ++m_iExpireCnt;
        }

        TimerTask() : m_iExpireCnt(0)
        {

        }
    };

    TimerTask t;
    elw::threading::Timer timer(1);
    timer.start(t, 2);
    elw::threading::sleep(202);
    t.cancel();
    elw::threading::sleep(1000);
    if (t.getExpireCnt() != 20)
    {
        std::cout << "task:" << t.getExpireCnt() << std::endl;
        return false;
    }

    return true;
}

