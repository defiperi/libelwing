/*
 * BasicArray002.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "BasicArray002.h"


std::string BasicArray002::getDescription() const
{
    return "BasicArray";
}

struct Element {
    uint i;
    std::string str;
};

bool BasicArray002::run()
{
    return true;
    elw::lang::BasicArray<Element> elems;

    for (uint i = 0; i < 32; ++i)
    {
        Element& e = elems.at(i);
        e.i = i;
        e.str = MKSTR(i);
    }

    elems.resize(64);

    for (uint i = 32; i < 64; ++i)
    {
        Element& e = elems.at(i);
        e.i = i;
        e.str = MKSTR(i);
    }

    for (uint i = 0; i < 64; ++i)
    {
        Element& e = elems[i];
        if (e.i != i)
            return false;
        if ((uint)atoi(e.str.c_str()) != i)
            return false;
    }


    return true;
}
