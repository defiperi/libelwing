/*
 * TCPSocket001.cpp
 *
 *  Created on: Oct 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "TCPSocket001.h"

std::string TCPSocket001::getDescription() const
{
    return "elw::Socket() test.";
}

bool TCPSocket001::run()
{
    class ClientSocket :
        public elw::threading::IRunnable
    {
        std::string m_strRemoteAddr;
        int m_iRemotePort;
        bool m_fSuccess;
    public:
        ClientSocket(const std::string& strRemoteAddr, int iRemotePort) :
            m_strRemoteAddr(strRemoteAddr),
            m_iRemotePort(iRemotePort),
            m_fSuccess(true)
        {
        }

    protected:
        void run()
        {
            elw::io::MemoryOutputStream strm;

            elw::net::Socket sock = elw::net::sock::create();
            if (!sock.isValid())
                goto _ERROR;

            if (!sock.connect(elw::net::Inet4SockAddr(m_strRemoteAddr, m_iRemotePort)))
                goto _ERROR;

            strm << "123456789";
            strm.flush();
            if (9 != sock.send(*(strm.buffer())))
                goto _ERROR;
            strm.reset();
            strm << "0";
            strm.flush();
            if (1 != sock.send(*(strm.buffer())))
                goto _ERROR;
            strm.reset();
            if (3 != sock.send("987", 3))
                goto _ERROR;
            if (2 != sock.send("65", 2))
                goto _ERROR;
            if (4 != sock.send("4321", 4))
                goto _ERROR;

            sock.shutdown();
            sock.close();

            return;

        _ERROR:
            m_fSuccess = false;
            sock.shutdown();
            sock.close();
        }

    public:
        bool isSuccess() const
        {
            return m_fSuccess;
        }
    } clnt("127.0.0.1", 22000);

    class ServerSocket :
        public elw::threading::IRunnable
    {
        int m_iListeningPort;
        bool m_fSuccess;
    public:
        ServerSocket(int wListeningPort) :
            m_iListeningPort(wListeningPort),
            m_fSuccess(true)
        {
        }

    protected:
        void run()
        {
            elw::net::SockAddr addr;
            elw::net::Socket clnt;
            char sz[32] = { 0 };
            uint uTotalRead = 0;
            std::string str;
            elw::net::Socket serv = elw::net::sock::create();
            if (!serv.isValid())
                goto _ERROR;
            if (!serv.bind(elw::net::Inet4SockAddr(m_iListeningPort)))
                goto _ERROR;
            if (!serv.listen())
                goto _ERROR;

            clnt = serv.accept(addr);
            for ( ; ; )
            {
                int iRead = clnt.recv(sz + uTotalRead, (size_t)sizeof(sz) - uTotalRead);
                if (iRead <= 0)
                    break;
                uTotalRead += iRead;
                if (uTotalRead >= sizeof(sz))
                    break;
            }

            str = sz;
            if (str != "1234567890987654321")
                goto _ERROR;

            clnt.shutdown();
            clnt.close();
            serv.close();

            return;

         _ERROR:
            m_fSuccess = false;
            clnt.shutdown();
            clnt.close();
            serv.close();
        }
    public:
        bool isSuccess() const
        {
            return m_fSuccess;
        }
    } srv(22000);

    elw::threading::Thread thrd0(&srv);
    thrd0.start();
    elw::threading::sleep(250);
    elw::threading::Thread thrd1(&clnt);
    thrd1.start();
    thrd1.join();
    thrd0.join();

    if (!clnt.isSuccess())
        return false;
    if (!srv.isSuccess())
        return false;
    return true;
}
