/*
 * InetAddress001.cpp
 *
 *  Created on: Oct 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "InetAddress001.h"

std::string InetAddress001::getDescription() const
{
    return"elw::InetAddress() test. www.sf.net";
}

bool InetAddress001::run()
{
    return true;
    const std::string strHost0("www.sf.net");
    const std::string strHost1("www.sourceforge.net");

    elw::net::Inet4SockAddr addr0(strHost0);
    if (addr0.getHostName() != strHost0)
        return false;
    elw::net::Inet4SockAddr addr1(strHost1);
    if (addr1.getHostName() != strHost1)
        return false;
    std::vector<uint8> ip0;
    addr0.getAddress(ip0);

    std::vector<uint8> ip1;
    addr0.getAddress(ip1);

    bool fRslt = true;
    for (int i = 0; i < 3; ++i)
    {
        if (ip0[i] != ip1[i])
        {
            fRslt = false;
            break;
        }
    }

    return fRslt;
}
