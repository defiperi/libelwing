/*
 * SocketSelector001.cpp
 *
 *  Created on: Apr 17, 2013
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "SocketSelector001.h"

std::string SocketSelector001::getDescription() const
{
    return "Socket selector";
}

bool SocketSelector001::run()
{
    bool fRslt = true;

    elw::net::SocketSelector selector;
    selector.start();

    class ServentListener :
        public elw::net::ISocketObserver
    {
        elw::net::Socket m_sock;
    public:
        ServentListener(elw::net::Socket& s) :
            m_sock(s)
        {
        }
    public:
        void onNotify()
        {
            elw::net::SockAddr addr;
            m_sock.getpeername(addr);


            elw::lang::Buffer b;
            int iRslt = m_sock.recv(b);
            if (0 != iRslt)
            {
                std::cout << "RECEIVE:" << addr.tostr() << " " << b << std::endl;
            }
            else
            {
                std::cout << "CLOSE FROM:" << addr.tostr() << " " << b << std::endl;
                m_sock.close();
            }
        }

    public:
        elw::net::sock::Handle sockfd() const
        {
            return m_sock.handle();
        }
    };

    class ServerListener :
        public elw::net::ISocketObserver
    {
        elw::net::Socket& m_sock;
        elw::net::SocketSelector& m_selector;
    public:
        ServerListener(elw::net::Socket& s, elw::net::SocketSelector& selector) :
            m_sock(s),
            m_selector(selector)
        {
        }

    public:

        void onNotify()
        {
            elw::net::SockAddr addr;
            elw::net::Socket s = m_sock.accept(addr);
            elw::lang::SmartPtr<elw::net::ISocketObserver> ptr(new ServentListener(s));
            std::cout << "CONNECT FROM:" << addr.tostr() << " Handle:" << s.handle() << std::endl;

            m_selector.attach(ptr);
        }

    public:
        elw::net::sock::Handle sockfd() const
        {
            return m_sock.handle();
        }
    };

    elw::net::Socket s0 = elw::net::sock::create();

    s0.bind(elw::net::Inet4SockAddr(12000));
    s0.listen();

    ServerListener server(s0, selector);
    elw::lang::SmartPtr<elw::net::ISocketObserver> ptr(&server, 2);

    selector.attach(ptr);


    elw::net::Inet4SockAddr addr("localhost", 12000);

    std::cout << "CONNECT TO:" << addr.tostr() << std::endl;
    elw::net::Socket s1 = elw::net::sock::create();

    s1.connect(addr);

    elw::net::SockAddr addr1;
    s1.getpeername(addr1);

    std::cout << "CLOSING:" << addr1.tostr() << std::endl;
    s1.close();

    std::cout << "CONNECT TO:" << addr.tostr() << std::endl;
    elw::net::Socket s2 = elw::net::sock::create();
    s2.connect(addr);


    elw::net::SockAddr addr2;
    s2.getpeername(addr2);

    std::cout << "SEND TO:" << addr2.tostr() << std::endl;
    s2.send("1", 1);
    std::cout << "SEND TO:" << addr2.tostr() << std::endl;
    s2.send("2", 1);
    std::cout << "CLOSING:" << addr2.tostr() << std::endl;
    s2.shutdown();
    s2.close();

    elw::threading::sleep(100);

    selector.stop();

    return fRslt;
}
