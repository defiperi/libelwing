/*
 * Timer005.cpp
 *
 *  Created on: Jul 20, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Timer005.h"

std::string Timer005::getDescription() const
{
    return "Basically timer test.";
}

bool Timer005::run()
{
    class TimerTask : public elw::threading::Timer::Task
    {
        int& m_iExpireCnt;

    public:
        void onExpire()
        {
            ++m_iExpireCnt;
        }

        TimerTask(int& iExpireCnt) : m_iExpireCnt(iExpireCnt)
        {

        }
    };

    int iExpireCnt = 0;
    elw::threading::Timer timer(1);
    for (int i = 0; i < 1000; ++i)
    {
        TimerTask t1(iExpireCnt);
        timer.start(t1, 10);

        {
            TimerTask t2(iExpireCnt);
            timer.start(t2, 20);

            elw::threading::sleep(9);
        }
    }

    std::cout << "expire cnt:" << iExpireCnt << std::endl;

    return true;
}

