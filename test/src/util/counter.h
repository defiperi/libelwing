/*
 * thread.h
 *
 *  Created on: Jan 20, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef test_counter_h__
#define test_counter_h__

namespace test {

template <class _TySync>
class AtomicCounter
{
    long m_iVal;
    _TySync m_sync;
public:
    AtomicCounter(long l = 0) :
        m_iVal(l)
    {
    }

    long operator ++()
    {
        m_sync.lock();
        int iRslt = ++m_iVal;
        m_sync.unlock();
        return iRslt;
    }

    long operator ++(int)
    {
        m_sync.lock();
        int iRslt = m_iVal++;
        m_sync.unlock();
        return iRslt;
    }

    long operator --()
    {
        m_sync.lock();
        int iRslt = --m_iVal;
        m_sync.unlock();
        return iRslt;
    }

    long operator --(int)
    {
        m_sync.lock();
        int iRslt = m_iVal--;
        m_sync.unlock();
        return iRslt;
    }

    operator long()
    {
        m_sync.lock();
        long iRslt = m_iVal;
        m_sync.unlock();
        return iRslt;
    }
};

template <>
class AtomicCounter<elw::threading::ReadWriteLock>
{
    long m_iVal;
    elw::threading::ReadWriteLock m_sync;
public:
    AtomicCounter(long l = 0) :
        m_iVal(l)
    {
    }

    long operator ++()
    {
        m_sync.wlock();
        int iRslt = ++m_iVal;
        m_sync.unlock();
        return iRslt;
    }

    long operator ++(int)
    {
        m_sync.wlock();
        int iRslt = m_iVal++;
        m_sync.unlock();
        return iRslt;
    }

    long operator --()
    {
        m_sync.wlock();
        int iRslt = --m_iVal;
        m_sync.unlock();
        return iRslt;
    }

    long operator --(int)
    {
        m_sync.wlock();
        int iRslt = m_iVal--;
        m_sync.unlock();
        return iRslt;
    }

    operator long()
    {
        m_sync.rlock();
        long iRslt = m_iVal;
        m_sync.unlock();
        return iRslt;
    }
};


template <class _TyAtomic>
class Incrementor : public elw::threading::IRunnable
{
    _TyAtomic& m_cnt;
public:
    Incrementor(_TyAtomic& cnt) :
        m_cnt(cnt)
    {
    }

    void run()
    {
        for (int i = 0; i < 100000; ++i)
        {
            ++m_cnt;
        }

        for (int i = 0; i < 100000; ++i)
        {
            m_cnt++;
        }

    }
};

template <class _TyAtomic>
class Decrementor : public elw::threading::IRunnable
{
    _TyAtomic& m_cnt;
public:
    Decrementor(_TyAtomic& cnt) :
        m_cnt(cnt)
    {
    }

    void run()
    {
        for (int i = 0; i < 100000; ++i)
        {
            --m_cnt;
        }

        for (int i = 0; i < 100000; ++i)
        {
            m_cnt--;
        }

    }
};


template <class _TyCounter>
void runcounter()
{
    _TyCounter cnt(17);
    test::Incrementor<_TyCounter> i0(cnt), i1(cnt);
    test::Decrementor<_TyCounter> d0(cnt), d1(cnt);

    elw::threading::Thread t0(&i0), t1(&i1), t2(&d0), t3(&d1);
    t0.start();
    t1.start();
    t2.start();
    t3.start();

    t0.join();
    t1.join();
    t2.join();
    t3.join();

    if (17 != (long) cnt)
        elw_throw_exception(elw::lang::Exception("multi thread counting"));

}

}

#endif /* test_counter_h__ */
