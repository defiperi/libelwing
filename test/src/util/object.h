/*
 * object.h
 *
 *  Created on: Jan 20, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef test_object_h__
#define test_object_h__

namespace test {

template <class _TyLockPolicy>
class SafeObject :
    public _TyLockPolicy
{
    int m_i;
public:
    SafeObject(int i = 0) :
        m_i(i)
    {
    }

    SafeObject(SafeObject const& that) :
        m_i(that.m_i)
    {
    }

    void value(int i)
    {
        elw::threading::ScopeGuard(*this);
        m_i = i;
    }

    int value()
    {
        elw::threading::ScopeGuard(*this);
        return m_i;
    }

    int value0()
    {
        elw::threading::ScopeGuard(*this);
        elw::threading::sleep(5);
        return m_i;
    }

};

template <>
class SafeObject<elw::threading::ReadWriteLock> :
    public elw::threading::ReadWriteLock
{
    int m_i;
public:
    SafeObject(int i = 0) :
        m_i(i)
    {
    }

    SafeObject(SafeObject const& that) :
        m_i(that.m_i)
    {
    }

    void value(int i)
    {
        elw::threading::ScopeWriteGuard(*this);
        m_i = i;
    }

    int value()
    {
        elw::threading::ScopeReadGuard(*this);
        return m_i;
    }

    int value0()
    {
        elw::threading::ScopeReadGuard(*this);
        elw::threading::sleep(0);
        return m_i;
    }

};

class NullLock : public elw::threading::ILockable
{
public:
    void lock(){}
    void unlock(){}
    bool trylock(){return true;}

};

typedef SafeObject<NullLock> Object;

typedef elw::lang::SmartPtr<Object> ObjectPtr;


}

#endif /* object_h__ */
