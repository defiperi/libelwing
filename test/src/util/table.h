/*
 * table.h
 *
 *  Created on: Mar 20, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef test_table_h__
#define test_table_h__

namespace test {

template <class _TyListPolicy, class _TyLockPolicy>
class Table
{
    _TyListPolicy m_map;
    _TyLockPolicy m_lock;

public:
    typedef typename _TyListPolicy::value_type   Value;
    typedef typename _TyListPolicy::iterator     Iterator;
public:
    Value get(std::string const& key)
    {
        elw::threading::ScopeGuard g(m_lock);
        for (Iterator it = m_map.begin(); it != m_map.end(); ++it)
        {
            if ((*it).get("key") == key)
                return (*it);
        }

        elw_throw_exception(elw::lang::Exception("value not found !!!"));
    }

    size_t put(Value const& v)
    {
        elw::threading::ScopeGuard g(m_lock);
        m_map.push_back(v);
        return m_map.size();
    }

    void remove(std::string const& key)
    {
        elw::threading::ScopeGuard g(m_lock);
        for (Iterator it = m_map.begin(); it != m_map.end(); ++it)
        {
            if ((*it).get("key") == key)
            {
                m_map.erease(it);
                return;
            }
        }
    }

    bool empty()
    {
        elw::threading::ScopeGuard g(m_lock);
        return m_map.size() == 0;
    }
};

template <class _TyListPolicy>
class Table<_TyListPolicy, elw::threading::ReadWriteLock>
{
    _TyListPolicy m_map;
    elw::threading::ReadWriteLock m_lock;

public:
    typedef typename _TyListPolicy::iterator     Iterator;
public:
    elw::util::Properties get(std::string const& key)
    {
        elw::threading::ScopeReadGuard g(m_lock);
        for (Iterator it = m_map.begin(); it != m_map.end(); ++it)
        {
            if ((*it).get("key") == key)
                return (*it);
        }

        elw_throw_exception(elw::lang::Exception("value not found !!!"));
    }

    size_t put(elw::util::Properties const& v)
    {
        elw::threading::ScopeWriteGuard g(m_lock);
        m_map.push_back(v);
        return m_map.size();
    }

    void remove(std::string const& key)
    {
        elw::threading::ScopeWriteGuard g(m_lock);
        for (Iterator it = m_map.begin(); it != m_map.end(); ++it)
        {
            if ((*it).get("key") == key)
            {
                m_map.erease(it);
                return;
            }
        }
    }

    bool empty()
    {
        elw::threading::ScopeReadGuard g(m_lock);
        return m_map.size() == 0;
    }
};

}

#endif /* table_h__ */
