/*
 * access.h
 *
 *  Created on: Jan 20, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef test_performance_h__
#define test_performance_h__

#include "table.h"

namespace test {

template <class _TyLockPolicy>
void runperformance()
{
    typedef Table<std::list<elw::util::Properties>, _TyLockPolicy> ListClass;

    ListClass m;
    for (int i = 0; i < 500; ++i)
    {
        elw::util::Properties p;
        p.put("key", MKSTR(i));
        p.put("h1", "h1");
        p.put("h2", "h2");
        p.put("h3", "h4");

        m.put(p);
    }

    class Accessor :
        public elw::threading::IRunnable
    {
        ListClass& m_rMap;
        bool m_fFailed;
    public:
        virtual void run()
        {
            for (int i = 0; i <= 5000; ++i)
            {
                try
                {
                    elw::util::Properties p = m_rMap.get("357");
                    if (p.get<int>("key") != 357)
                        elw_throw_exception(elw::lang::Exception("p.get<int>(\"key\") != 357"));
                }
                catch (elw::lang::Exception& e)
                {
                    std::cout << e.what() << std::endl;
                    break;
                }
            }

            m_fFailed = false;
        }


        bool isFailed()
        {
            return m_fFailed;
        }

        Accessor(ListClass& rMap) :
            m_rMap(rMap),
            m_fFailed(true)
        {
        }
    } a0(m), a1(m), a2(m), a3(m);


    elw::threading::Thread t0(&a3), t2(&a0), t3(&a1), t4(&a2);

    t0.start(), t2.start(), t3.start(), t4.start();
    t0.join(), t2.join(), t3.join(), t4.join();

    if (a0.isFailed() || a1.isFailed() || a2.isFailed() || a3.isFailed())
        elw_throw_exception(elw::lang::Exception("access failed !!!"));
}

}

#endif /* access_h__ */
