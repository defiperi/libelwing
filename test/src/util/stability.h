/*
 * access.h
 *
 *  Created on: Jan 20, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef test_stability_h__
#define test_stability_h__


#include "object.h"

namespace test {

template <class _TyMapPolicy, class _TyLockPolicy>
class SafeMap
{
    _TyMapPolicy m_map;
    _TyLockPolicy m_lock;

public:
    typedef typename _TyMapPolicy::mapped_type   Value;
    typedef typename _TyMapPolicy::key_type      Key;
    typedef typename _TyMapPolicy::iterator      Iterator;
public:
    Value get(Key k, Value d)
    {
        m_lock.lock();
        Iterator it = m_map.find(k);
        if (it == m_map.end())
        {
            m_lock.unlock();
            return d;
        }

        Value v = (*it).second;
        m_lock.unlock();
        return v;
    }

    size_t put(Key k, Value const& v)
    {
        m_lock.lock();
        m_map[k] = v;
        m_lock.unlock();
        size_t n = m_map.size();

        return n;
    }

    void remove(Key k)
    {
        m_lock.lock();
        m_map.erase(k);
        m_lock.unlock();
    }

    bool empty()
    {
        m_lock.lock();
        bool fRslt = m_map.size() == 0;
        m_lock.unlock();

        return fRslt;
    }
};

template <class _TyMapPolicy>
class SafeMap<_TyMapPolicy, elw::threading::ReadWriteLock>
{
    _TyMapPolicy m_map;
    elw::threading::ReadWriteLock m_lock;

public:
    typedef typename _TyMapPolicy::mapped_type   Value;
    typedef typename _TyMapPolicy::key_type      Key;
    typedef typename _TyMapPolicy::iterator      Iterator;
public:
    Value get(Key k, Value d)
    {
        m_lock.rlock();
        Iterator it = m_map.find(k);
        if (it == m_map.end())
        {
            m_lock.unlock();
            return d;
        }

        Value v = (*it).second;
        m_lock.unlock();
        return v;
    }

    size_t put(Key k, Value const& v)
    {
        m_lock.wlock();
        m_map[k] = v;
        m_lock.unlock();
        size_t n = m_map.size();

        return n;
    }

    void remove(Key k)
    {
        m_lock.wlock();
        m_map.erase(k);
        m_lock.unlock();
    }

    bool empty()
    {
        m_lock.rlock();
        bool fRslt = m_map.size() == 0;
        m_lock.unlock();

        return fRslt;
    }
};

namespace stability {

template <class _TyLockPolicy>
void run()
{
    typedef SafeMap<std::map<int, elw::lang::SmartPtr<test::SafeObject<_TyLockPolicy> > >, _TyLockPolicy> MapClass;

    MapClass m;
    for (int i = 0; i <= 10; ++i)
    {
        m.put(i, new SafeObject<_TyLockPolicy>(i));
    }

    class Accessor :
        public elw::threading::IRunnable
    {
        MapClass& m_rMap;
        bool m_fFailed;
    public:
        virtual void run()
        {
            while (!m_rMap.empty())
            {
                for (int i = 0; i <= 10; ++i)
                {
                    typename MapClass::Value ptr = m_rMap.get(i, NULL);
                    if (ptr != NULL)
                    {
                        elw::threading::yield();
                        try
                        {
                            ptr->value();
                        }
                        catch (elw::lang::Exception& e)
                        {
                            m_fFailed = true;
                        }
                    }
                }
            }
        }

        bool isFailed()
        {
            return m_fFailed;
        }

        Accessor(MapClass& rMap) :
            m_rMap(rMap),
            m_fFailed(false)
        {
        }
    } a0(m), a1(m), a2(m), a3(m);

    class Remover :
        public elw::threading::IRunnable
    {
        MapClass& m_rMap;
    public:
        virtual void run()
        {
            while (!m_rMap.empty())
            {
                for (int i = 10; i >= 0; --i)
                {
                    typename MapClass::Value ptr = m_rMap.get(i, NULL);
                    if (ptr != NULL && ptr.count() > 4)
                    {
                        m_rMap.remove(i);
                        elw::threading::yield();
                    }
                }
            }
        }

        Remover(MapClass& rMap) :
            m_rMap(rMap)
        {
        }
    } r0(m), r1(m);

    elw::threading::Thread t0(&r0), t2(&r1), t3(&a1), t4(&a2), t5(&a0), t6(&a3);

    t0.start(), t2.start(), t3.start(), t4.start(), t5.start(), t6.start();
    t0.join(), t2.join(), t3.join(), t4.join(), t5.join(), t6.join();

    if (a0.isFailed() || a1.isFailed() || a2.isFailed() || a3.isFailed())
        elw_throw_exception(elw::lang::Exception("access failed !!!"));
}

}

template <class _TyLockPolicy>
void runstability()
{
    for (int i = 0; i < 2; ++i)
    {
        stability::run<_TyLockPolicy>();
    }

}

}

#endif /* access_h__ */
