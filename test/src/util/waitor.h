/*
 * waitor.h
 *
 *  Created on: Mar 25, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef waitor_h__
#define waitor_h__


template <class _TyWaitor>
class Waitor : public elw::threading::Thread
{
    _TyWaitor& m_waitor;
    dword m_dwWait;
    dword m_dwRunningTime;
    int m_iWaitRslt;
    bool m_fStarted;
public:
    Waitor(_TyWaitor& c, dword dwWait=0) :
        m_waitor(c),
        m_dwWait(dwWait),
        m_dwRunningTime(0),
        m_iWaitRslt(-1),
        m_fStarted(false)
    {
    }

public:
    void run()
    {
        m_fStarted = true;
        dword dw = elw::threading::tick();
        m_iWaitRslt = m_dwWait != 0 ? m_waitor.wait(m_dwWait) : m_waitor.wait();
        m_dwRunningTime = elw::threading::tick() - dw;
    }

public:
    dword runtime() const
    {
        return m_dwRunningTime;
    }

    int result() const
    {
        return m_iWaitRslt;
    }

    void waitForStarted()
    {
        while (!m_fStarted)
            elw::threading::yield();
    }
};


#endif /* waitor_h__ */
