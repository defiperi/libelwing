/*
 * tpool.h
 *
 *  Created on: Mar 30, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef tpool_h__
#define tpool_h__

namespace test {

namespace tpool {

class Message
{
    dword m_dwSender;
    dword m_dwParam;
public:
    Message(dword dwSender, dword dwParam) :
        m_dwSender(dwSender),
        m_dwParam(dwParam)
    {
    }

    dword getSender() const
    {
        return m_dwSender;
    }

    dword getParam() const
    {
        return m_dwParam;
    }
};

class Handler :
    public elw::threading::ThreadPool<uint, Message*>::IConsumer
{
    uint m_uKey;
    dword m_dwSelfId;
    bool m_fSuccess;
    uint m_uWorkCnt;
    uint m_uMsgCnt;
    std::vector<dword> m_lst;
    elw::threading::Event m_evnt;
public:
    Handler(uint uKey) :
        m_uKey(uKey),
        m_dwSelfId(0),
        m_fSuccess(true),
        m_uWorkCnt(0),
        m_uMsgCnt(0)
    {
    }

    std::vector<dword> const& getProducers() const
    {
        return m_lst;
    }

    bool isSuccess() const
    {
        return (m_fSuccess && (m_uMsgCnt == m_uWorkCnt));
    }

    uint getWorkCnt() const
    {
        return m_uWorkCnt;
    }

    uint getExpectedMsgCnt() const
    {
        return m_uMsgCnt;
    }

    void addMsgCnt(uint uMsg)
    {
        m_uMsgCnt += uMsg;
    }

    bool waitForCompleted(dword dw)
    {
        bool fRslt = true;
        if (m_uMsgCnt != 0)
            if (elw::threading::wait::timeout == m_evnt.wait(dw))
                fRslt = false;
        return fRslt;
    }

    void onMessage(Message* pmsg)
    {
        if (m_dwSelfId == 0)
        { // Mesaj hep aynı threaden gelmeli
            m_dwSelfId = elw::threading::selfid();
        }
        else
        {
            if (m_dwSelfId != elw::threading::selfid())
            {
                m_fSuccess = false;
            }
        }

        bool fFound = false;
        dword dwSender = pmsg->getSender();
        for (uint i = 0; i < m_lst.size(); ++i)
        {
            if (m_lst[i] == dwSender)
            {
                fFound = true;
                break;
            }
        }

        if (!fFound)
        {
            m_lst.push_back(dwSender);
        }

        ++m_uWorkCnt;
        delete pmsg;

        if (m_uMsgCnt == m_uMsgCnt)
            m_evnt.notify();
    }
};

class Producer :
    public elw::threading::Thread
{
    uint m_uKey;
    uint m_uMsgCnt;
    uint m_uFailedWork;
    elw::threading::ThreadPool<uint, Message*>& m_tp;
public:
    Producer(uint uKey, uint uMsgCnt, elw::threading::ThreadPool<uint, Message*>& tp) :
        m_uKey(uKey),
        m_uMsgCnt(uMsgCnt),
        m_uFailedWork(0),
        m_tp(tp)
    {
        start();
    }

    uint getFailedCnt() const
    {
        return m_uFailedWork;
    }

protected:
    void run()
    {
        for (uint i = 0; i < m_uMsgCnt; ++i)
        {
            if (!m_tp.post(m_uKey, new Message(elw::threading::selfid(), i)))
                ++m_uFailedWork;
        }
    }
};

}

inline
void runtpool(uint iThreadCnt, uint iConsumer, uint iProducer, uint iMsgCnt)
{
    elw::threading::ThreadPool<uint, tpool::Message*> tp;
    tp.start(iThreadCnt);
    std::map<int, tpool::Handler*> lstHandlers;
    for (uint i = 0; i < iConsumer; ++i)
    {
        tpool::Handler* p = new tpool::Handler((i+1)*1000);
        std::cout << "consumer-" << i << " key:"<< (i+1)*1000  << std::endl;

        if (!tp.addConsumer((i+1)*1000, *p))
            elw_throw_exception(elw::lang::Exception("addConsumer"));
        lstHandlers[(i+1)*1000] = p;
    }

    uint iThreadMsgCnt = iMsgCnt / iProducer;
    uint iFirstThreadMsgCnt = iThreadMsgCnt + (iMsgCnt%iProducer);

    std::vector<tpool::Producer*> lstThreads;
    lstHandlers[1000]->addMsgCnt(iFirstThreadMsgCnt);
    lstThreads.push_back(new tpool::Producer(1000, iFirstThreadMsgCnt, tp));
    std::cout << "producer-" << 0 << " key:1000 msg:" << iFirstThreadMsgCnt << std::endl;

    uint j = 1;
    for (uint i = 1; i < iProducer; ++i)
    {
        if (j >= lstHandlers.size())
            j = 0;
        std::cout << "producer-" << i << " key:"<< (j+1)*1000 << " msg:" << iThreadMsgCnt << std::endl;
        lstHandlers[(j+1)*1000]->addMsgCnt(iThreadMsgCnt);
        lstThreads.push_back(new tpool::Producer((j+1)*1000, iThreadMsgCnt, tp));
        ++j;
    }

    for (uint i = 0; i < lstThreads.size(); ++i)
        lstThreads[i]->join();

    for (uint i = 0; i < lstHandlers.size(); ++i)
    {
        tpool::Handler* p = lstHandlers[(i+1)*1000];
        p->waitForCompleted(1000);
    }

    tp.killAll();

    uint iFailedCnt = 0;
    for (uint i = 0; i < lstThreads.size(); ++i)
    {
        uint iFailed = lstThreads[i]->getFailedCnt();
        std::cout << "producer-" << i << " failed:" << iFailed << std::endl;
        iFailedCnt += iFailed;
    }

    if (iFailedCnt != 0)
        elw_throw_exception(elw::lang::Exception("post failed"));
    uint iHandledMsg = 0;
    for (uint i = 0; i < lstHandlers.size(); ++i)
    {
        tpool::Handler* p = lstHandlers[(i+1)*1000];
        uint iWorkCnt = p->getWorkCnt();
        std::cout << "consumer-" << i << " expected:" << p->getExpectedMsgCnt() << " msg:" << iWorkCnt << std::endl;

        if (!p->isSuccess())
            elw_throw_exception(elw::lang::Exception("handler failed !!!"));

        iHandledMsg += iWorkCnt;
    }

    if (iHandledMsg != iMsgCnt)
        elw_throw_exception(elw::lang::Exception("msg cnt"));
}


}
#endif /* tpool_h__ */
