/*
 * tqueue.h
 *
 *  Created on: Mar 26, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef tqueue_h__
#define tqueue_h__

#include "object.h"

namespace test {

typedef elw::threading::ThreadQueue<test::ObjectPtr> TQueue;

namespace tqueue {

class Consumer : public elw::threading::Thread
{
    TQueue& m_q;
    std::vector<test::ObjectPtr> m_lst;

public:
    Consumer(TQueue& q) :
        m_q(q)
    {
        start();
    }

    void run()
    {
        test::ObjectPtr ptr;
        while ((ptr = m_q.pop()) != NULL)
        {
            m_lst.push_back(ptr);
        }
    }

    std::vector<test::ObjectPtr> const& list() const
    {
        return m_lst;
    }
};

class Producer : public elw::threading::Thread
{
    TQueue& m_q;
    int m_iCnt;
    int m_iBegin;
public:
    Producer(TQueue& q, int iBegin, int iCnt) :
        m_q(q),
        m_iCnt(iCnt),
        m_iBegin(iBegin)
    {
        std::cout << "producer " << iBegin << ":" << iCnt << std::endl;
        start();
    }

    void run()
    {
        for (int i = 0; i < m_iCnt; ++i)
        {
            m_q.push(new test::Object(m_iBegin+i));
        }
    }
};

}

inline
void runtqueue(int iConsumer, int iProducer, int iMsgCnt)
{
    TQueue queue;

    std::vector<tqueue::Consumer*> lstConsumer;
    for (int i = 0; i < iConsumer; ++i)
        lstConsumer.push_back(new tqueue::Consumer(queue));

    std::vector<tqueue::Producer*> lstProducer;
    int iMsg = iMsgCnt/iProducer;
    int iBegin = 0;
    lstProducer.push_back(new tqueue::Producer(queue, iBegin, iMsg + (iMsgCnt%iProducer)));
    iBegin += iMsg + (iMsgCnt%iProducer);
    for (int i = 1; i < iProducer; ++i)
    {
        lstProducer.push_back(new tqueue::Producer(queue, iBegin, iMsg));
        iBegin += iMsg;
    }

    for (int i = 0; i < iProducer; ++i)
    {
        lstProducer[i]->join();
    }

    for (int i = 0; i < iConsumer; ++i)
        queue.push(NULL);

    for (int i = 0; i < iConsumer; ++i)
    {
        lstConsumer[i]->join();
    }

    std::map<int, test::ObjectPtr> table;

    for (int i = 0; i < iConsumer; ++i)
    {
        std::vector<test::ObjectPtr>const& lst = lstConsumer[i]->list();
        std::cout << "consumer-" << i << " -> " << lst.size() << std::endl;
        for (int l = 0; l < lst.size(); ++l)
        {
            std::map<int, test::ObjectPtr>::iterator it = table.find(lst[l]->value());
            if (it != table.end())
                elw_throw_exception(elw::lang::Exception("table"));
            table[lst[l]->value()] = lst[l];
        }
    }

    std::cout << table.size() << "-" << iMsgCnt << std::endl;

    if (table.size() != iMsgCnt)
        elw_throw_exception(elw::lang::Exception("msg count"));


    for (int i = 0; i < iConsumer; ++i)
        delete lstConsumer[i];
    for (int i = 0; i < iProducer; ++i)
        delete lstProducer[i];

}


}

#endif /* tqueue_h__ */
