/*
 * lock.h
 *
 *  Created on: Mar 26, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef lock_h__
#define lock_h__

namespace test {

template <class _Ty>
class LockTester :
    public elw::threading::Thread
{
    _Ty& m_lock;
    bool m_fStarted;
public:
    LockTester(_Ty& l) :
        m_lock(l),
        m_fStarted(false)
    {
        start();
        while (!m_fStarted)
            elw::threading::yield();
    }

    void run()
    {
        m_lock.lock();
        m_fStarted = true;
        elw::threading::sleep(200);
        m_lock.unlock();
    }
};

template<class _TyLockPolicy>
void runlock()
{
    _TyLockPolicy l;
    dword dwInit = elw::threading::tick();
    LockTester<_TyLockPolicy> t(l);
    dwInit = elw::threading::tick() - dwInit;
    dword dw = elw::threading::tick();
    l.lock();
    dword dwTime = elw::threading::tick() - dw;
    std::cout << dwTime << std::endl;
    l.unlock();

    if (dwTime < (200 - dwInit))
        elw_throw_exception(elw::lang::Exception("lock time"));

    t.join();
}


}

#endif /* lock_h__ */
