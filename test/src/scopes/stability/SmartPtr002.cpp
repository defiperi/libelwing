/*
 * SmartPtr002.cpp
 *
 *  Created on: Jan 19, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "SmartPtr002.h"

namespace functionality { namespace lang {

SmartPtr002::SmartPtr002()
{
    // TODO Auto-generated constructor stub

}

SmartPtr002::~SmartPtr002()
{
    // TODO Auto-generated destructor stub
}

std::string SmartPtr002::getName() const
{
    return "SmartPtr2";
}

std::string SmartPtr002::getDescription() const
{
    return "SmartPtr reference count stress test, 4 threads";
}

void SmartPtr002::run()
{
    test::ObjectPtr ptrObj(new test::Object(666));

    {
        class RefCount :
            public elw::threading::IRunnable
        {
            test::ObjectPtr m_ptr;

        public:
            RefCount(test::ObjectPtr ptr) :
                m_ptr(ptr)
            {
            }

            void run()
            {
                for (int i = 0; i < 1000; ++i)
                {
                    std::vector<elw::lang::SmartPtr<elw::lang::IUnknown> > lst(10000);
                    for (int l = 0; l < 10000; ++l)
                        lst[i] = m_ptr.cast_dynamic<elw::lang::IUnknown>();
                }
            }

        } r0(ptrObj), r1(ptrObj), r2(ptrObj), r3(ptrObj);

        elw::threading::Thread t0(&r0);
        elw::threading::Thread t1(&r1);
        elw::threading::Thread t2(&r2);
        elw::threading::Thread t3(&r3);

        t0.start(), t1.start(), t2.start(), t3.start();
        t0.join(), t1.join(), t2.join(), t3.join();
    }

    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptr.count() != 1"));
}

}}
