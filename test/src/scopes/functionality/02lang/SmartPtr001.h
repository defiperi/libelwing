/*
 * SmartPtr001.h
 *
 *  Created on: Jan 19, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FunctionalityLangSmartPtr001_h__
#define FunctionalityLangSmartPtr001_h__

namespace functionality { namespace lang {

class SmartPtr001 :
    public utester::Test
{
public:
    std::string getName() const;
    std::string getDescription() const;

    void run();

public:
    SmartPtr001();
    virtual ~SmartPtr001();

protected:
    static test::ObjectPtr create();
    static void parameter_pass_with_ref(test::ObjectPtr& ptrObj);
    static void parameter_pass_with_obj(test::ObjectPtr ptrObj);

    static void scope(test::ObjectPtr& ptrObj);
    static void cast(test::ObjectPtr& ptrObj);

    static void recursive(test::ObjectPtr& ptrObj);
    static void exception(test::ObjectPtr& ptrObj);

    static void container(test::ObjectPtr& ptrObj);

    static void access(test::ObjectPtr& ptrObj);

};

}}

#endif /* SmartPtr001_h__ */
