/*
 * SmartPtr001.cpp
 *
 *  Created on: Jan 19, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "SmartPtr001.h"

namespace functionality { namespace lang {

SmartPtr001::SmartPtr001()
{
    // TODO Auto-generated constructor stub

}

SmartPtr001::~SmartPtr001()
{
    // TODO Auto-generated destructor stub
}

std::string SmartPtr001::getName() const
{
    return "SmartPtr1";
}

std::string SmartPtr001::getDescription() const
{
    return "SmartPtr functionality test";
}


void SmartPtr001::run()
{
    std::cout << "create" << std::endl;
    test::ObjectPtr ptrObj = create();
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "parameter pass with ref" << std::endl;
    parameter_pass_with_ref(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "scope" << std::endl;
    scope(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "parameter pass with obj" << std::endl;
    parameter_pass_with_obj(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "type cast" << std::endl;
    cast(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "recursive" << std::endl;
    recursive(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "exception" << std::endl;
    exception(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "std containers" << std::endl;
    container(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::cout << "access" << std::endl;
    access(ptrObj);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));
}

test::ObjectPtr SmartPtr001::create()
{
    elw::lang::SmartPtr<test::Object> ptrObj((test::Object*)0x12345678, 2);
    if (ptrObj != (test::Object*)0x12345678)
        elw_throw_exception(elw::lang::Exception("ptrObj != (test::Object*)0x12345678"));

    test::Object* p = new test::Object(666);
    ptrObj = p;
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));
    if (p != ptrObj)
        elw_throw_exception(elw::lang::Exception("p != ptrObj"));

    elw::lang::SmartPtr<test::Object> ptrObj2(ptrObj);
    if (ptrObj2 != ptrObj)
        elw_throw_exception(elw::lang::Exception("ptrObj2 != ptrObj"));

    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));

    return ptrObj;
}

void SmartPtr001::scope(test::ObjectPtr& ptrObj)
{
    test::ObjectPtr ptr1 = ptrObj;
    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));
    {
        test::ObjectPtr ptr2 = ptr1;
        if (ptrObj.count() != 3)
            elw_throw_exception(elw::lang::Exception("ptrObj.count() != 3"));
        {
            test::ObjectPtr ptr3 = ptr2;
            if (ptrObj.count() != 4)
                elw_throw_exception(elw::lang::Exception("ptrObj.count() != 4"));
            {
                test::ObjectPtr ptr4 = ptr3;
                if (ptrObj.count() != 5)
                    elw_throw_exception(elw::lang::Exception("ptrObj.count() != 5"));

                if (ptrObj.count() != ptr1.count())
                    elw_throw_exception(elw::lang::Exception("ptrObj.count() != ptr1.count()"));
                if (ptrObj.count() != ptr2.count())
                    elw_throw_exception(elw::lang::Exception("ptrObj.count() != ptr2.count()"));
                if (ptrObj.count() != ptr3.count())
                    elw_throw_exception(elw::lang::Exception("ptrObj.count() != ptr3.count()"));
                if (ptrObj.count() != ptr4.count())
                    elw_throw_exception(elw::lang::Exception("ptrObj.count() != ptr4.count()"));

                if (ptrObj != ptr1)
                    elw_throw_exception(elw::lang::Exception("ptrObj != ptr4"));
                if (ptrObj != ptr2)
                    elw_throw_exception(elw::lang::Exception("ptrObj != ptr4"));
                if (ptrObj != ptr3)
                    elw_throw_exception(elw::lang::Exception("ptrObj != ptr4"));
                if (ptrObj != ptr4)
                    elw_throw_exception(elw::lang::Exception("ptrObj != ptr4"));
            }

            if (ptrObj.count() != 4)
                elw_throw_exception(elw::lang::Exception("ptrObj.count() != 4"));
        }

        if (ptrObj.count() != 3)
            elw_throw_exception(elw::lang::Exception("ptrObj.count() != 3"));
    }

    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));
}

void SmartPtr001::parameter_pass_with_ref(test::ObjectPtr& ptrObj)
{
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));
}

void SmartPtr001::parameter_pass_with_obj(test::ObjectPtr ptrObj)
{
    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));
}

void SmartPtr001::cast(test::ObjectPtr& ptrObj)
{
    elw::lang::SmartPtr<elw::lang::IUnknown> ptrBase = ptrObj.cast_dynamic<elw::lang::IUnknown>();
    if (ptrBase == NULL)
        elw_throw_exception(elw::lang::Exception("ptrBase == NULL"));
    if (ptrBase.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrBase.count() != 2"));
    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));
}

int RecursiveFunction(test::ObjectPtr& ptrObj, int iCnt)
{
    test::ObjectPtr ptr = ptrObj;
    if (--iCnt <= 0)
        return ptrObj.count();
    return RecursiveFunction(ptrObj, iCnt);
}

void SmartPtr001::recursive(test::ObjectPtr& ptrObj)
{
    int iRslt = RecursiveFunction(ptrObj, 1000);
    if (iRslt != 1001)
        elw_throw_exception(elw::lang::Exception("iRslt != 1000"));
}

void SmartPtr001::exception(test::ObjectPtr& ptrObj)
{
    test::ObjectPtr ptr = ptrObj;

    try
    {
        test::ObjectPtr ptr1 = ptr;
        try
        {
            test::ObjectPtr ptr2 = ptr1;
            {
                test::ObjectPtr ptr3 = ptr2;
                throw elw::lang::IllegalStateException();
            }
        }
        catch (elw::lang::IllegalStateException& e)
        {
            if (ptrObj.count() != 3)
                elw_throw_exception(elw::lang::Exception("ptrObj.count() != 3"));
            throw;
        }
    }
    catch (elw::lang::IllegalStateException& e)
    {
    }

    if (ptrObj.count() != 2)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 2"));
}


void SmartPtr001::container(test::ObjectPtr& ptrObj)
{
    elw::lang::Array<test::ObjectPtr> arr(1000);
    for (int i = 0; i < 1000; ++i)
        arr[i] = ptrObj;
    if (ptrObj.count() != 1001)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1001"));
    if (arr[99].count() != 1001)
        elw_throw_exception(elw::lang::Exception("arr[99].count() != 1000"));

    arr[634] = NULL;
    if (ptrObj.count() != 1000)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1000"));
    if (arr[123].count() != 1000)
        elw_throw_exception(elw::lang::Exception("arr[123].count() != 1000"));

    arr.fill(NULL);
    if (ptrObj.count() != 1)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

    std::vector<test::ObjectPtr> lst;
    for (int i = 0; i < 100; ++i)
        lst.push_back(ptrObj);

    if (ptrObj.count() != 101)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 101"));

    {
        std::map<int, test::ObjectPtr> map;
        for (int i = 0; i < 65; ++i)
            map[i] = ptrObj;

        if ((*map.begin()).second.count() != 166)
            elw_throw_exception(elw::lang::Exception("(*map.begin()).second.count() != 166"));

        if (ptrObj.count() != 166)
            elw_throw_exception(elw::lang::Exception("(*map.begin()).second.count() != 166"));

        map.erase(map.begin());
        if (ptrObj.count() != 165)
            elw_throw_exception(elw::lang::Exception("(*map.begin()).second.count() != 165"));
        if ((*map.begin()).second.count() != 165)
            elw_throw_exception(elw::lang::Exception("(*map.begin()).second.count() != 165"));
    }

    if (ptrObj.count() != 101)
        elw_throw_exception(elw::lang::Exception("ptrObj.count() != 1"));

}

void SmartPtr001::access(test::ObjectPtr& ptrObj)
{
    if (ptrObj->value() != 666)
        elw_throw_exception(elw::lang::Exception("ptrObj->value() != 666"));

    ptrObj->value(1234);
    if (ptrObj->value() != 1234)
        elw_throw_exception(elw::lang::Exception("ptrObj->value() != 666"));

    test::ObjectPtr ptr(ptrObj);
    ptr->value(1234);
    if (ptr->value() != 1234)
        elw_throw_exception(elw::lang::Exception("ptr->value() != 1234"));

    ptr = NULL;

    try
    {
        ptr->value();
        elw_throw_exception(elw::lang::Exception("nullptr->value()"));
    }
    catch (elw::lang::NullPointerException& e)
    {
    }

    try
    {
        (*ptr).value();
        elw_throw_exception(elw::lang::Exception("(*nullptr).value()"));
    }
    catch (elw::lang::NullPointerException& e)
    {
    }
}


}}
