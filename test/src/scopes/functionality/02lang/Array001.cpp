/*
 * Array001.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Array001.h"

namespace functionality { namespace lang {


Array001::Array001()
{
    // TODO Auto-generated constructor stub

}

Array001::~Array001()
{
    // TODO Auto-generated destructor stub
}

std::string Array001::getName() const
{
    return "Array";
}

std::string Array001::getDescription() const
{
    return "array object functionality test";
}

namespace array_test {


template<class _Ty>
void run(_Ty const& first, std::vector<_Ty> const& vals)
{
    elw::lang::Array<_Ty> a0(vals.size());
    if (a0.size() != vals.size())
        elw_throw_exception(elw::lang::Exception("a0.size() != vals.size()"));

    a0.fill(first);
    for (uint i = 0; i < a0.size(); ++i)
        if (a0[i] != first)
            elw_throw_exception(elw::lang::Exception("a0[i] != first"));

    _Ty const& t0 = a0[0];
    _Ty& t1 = a0[0];
    _Ty t2 = a0[0];

    if (t1 != t0 || t0 != t2)
        elw_throw_exception(elw::lang::Exception("t1 != t0 || t0 != t2"));

    uint i = 0;
    typename std::vector<_Ty>::const_iterator it;
    for (it = vals.begin(); it != vals.end(); ++it, ++i)
    {
        a0[i] = (*it);
    }

    it = vals.begin();
    for (i = 0; i < a0.size(); ++i, ++it)
    {
        if (a0[i] != (*it))
            elw_throw_exception(elw::lang::Exception("a0[i] != (*it)"));
    }

    if (it != vals.end())
        elw_throw_exception(elw::lang::Exception("it != vals.end()"));

    elw::lang::Array<_Ty> a1(a0);
    if (a0.size() != a1.size())
        elw_throw_exception(elw::lang::Exception("a0.size() != a1.size()"));

    for (i = 0; i < a1.size(); ++i)
    {
        if (a0[i] != a1[i])
            elw_throw_exception(elw::lang::Exception("a0[i] != a1[i]"));
    }

    _Ty t = a1[0];
    a1[0] = first;
    if (a1[0] != first)
        elw_throw_exception(elw::lang::Exception("a1[0] != first"));
    a1[0] = t;
    if (a1[0] != t)
        elw_throw_exception(elw::lang::Exception("a1[0] != t"));

    elw::lang::Array<_Ty> a2(2);
    a2 = a1;

    if (a2.size() != vals.size())
        elw_throw_exception(elw::lang::Exception("a2.size() != vals.size()"));

    it = vals.begin();
    for (i = 0; i < a2.size(); ++i)
    {
        if (a0[i] != (*it))
            elw_throw_exception(elw::lang::Exception("a0[i] != (*it)"));
        ++it;
    }

    if (it != vals.end())
        elw_throw_exception(elw::lang::Exception("it != vals.end()"));
};

struct Element
{
    int ival;
    long lval;
    std::string sval;

    Element() :
        ival(0),
        lval(0)
    {
    }

    Element(int i, long l, std::string const& s) :
        ival(i),
        lval(l),
        sval(s)
    {
    }
};

std::ostream& operator <<(std::ostream& os, Element const& e)
{
    os << "Element(" << e.ival << ", " << e.lval << ", " << e.sval << ")";
    os.flush();
    return os;
}

bool operator ==(Element const& e0, Element const& e1)
{
    return (e0.ival == e1.ival) && (e0.lval == e1.lval) && (e0.sval == e1.sval);
}

bool operator !=(Element const& e0, Element const& e1)
{
    return !(e0 == e1);
}


}

void Array001::run()
{
    std::vector<int> lst0;
    std::vector<void*> lst1;
    std::vector<array_test::Element> lst2;

    for (int i = 1; i <= 1000; ++i)
    {
        lst0.push_back(i);
        lst1.push_back((void*)i);
        lst2.push_back(array_test::Element(i, (long)i, MKSTR(i)));
    }

    std::cout << "integer array" << std::endl;
    array_test::run<int>(0, lst0);
    std::cout << "pointer array" << std::endl;
    array_test::run<void*>(NULL, lst1);
    std::cout << "object array" << std::endl;
    array_test::run<array_test::Element>(array_test::Element(0, 0l, "0"), lst2);
}

}}
