/*
 * Buffer001.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Buffer001.h"

namespace functionality { namespace lang {

Buffer001::Buffer001()
{
}

Buffer001::~Buffer001()
{
}

std::string Buffer001::getName() const
{
    return "ByteBuffer";
}

std::string Buffer001::getDescription() const
{
    return "ByteBuffer object functionality test";
}

void Buffer001::run()
{
    elw::lang::ByteBuffer buff(10);
    std::string str("012345");

    buff.copy(str.c_str(), str.length());
    if (buff.length() != str.length())
        elw_throw_exception(elw::lang::Exception("length"));
    if ((buff.capacity()-buff.length()) != (10-str.length()))
        elw_throw_exception(elw::lang::Exception("length"));

    std::string str1;
    char const* pch = (char const*)buff.ptr();

    for (int i = 0; i < buff.length(); ++i)
        str1 += *pch++;

    std::cout << str << std::endl;
    std::cout << str1 << std::endl;

    if (str != str1)
        elw_throw_exception(elw::lang::Exception("compare"));

    str += str1;
    buff.append(str1.c_str(), str1.length());
    if (buff.length() != str.length())
        elw_throw_exception(elw::lang::Exception("length"));
    str1.clear();
    pch = (char const*)buff.ptr();
    for (int i = 0; i < buff.length(); ++i)
        str1 += *pch++;

    std::cout << str << std::endl;
    std::cout << str1 << std::endl;

    if (str != str1)
        elw_throw_exception(elw::lang::Exception("compare"));

    struct Hede
    {
        int iVal;
        long lVal;
        float fVal;

        Hede(int i, long l, float f) :
            iVal(i),
            lVal(l),
            fVal(f)
        {
        }
    };

    Hede h0(1, 2, 3.0);
    buff.clear();

    buff.copy(&h0, sizeof(h0));

    Hede h1 = *((Hede*)buff.ptr());
    std::cout << h0.iVal << "-" << h1.iVal << std::endl;
    std::cout << h0.lVal << "-" << h1.lVal << std::endl;
    std::cout << h0.fVal << "-" << h1.fVal << std::endl;

}

}}
