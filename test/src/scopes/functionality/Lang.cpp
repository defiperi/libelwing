/*
 * Lang.cpp
 *
 *  Created on: Jan 19, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Lang.h"
#include "02lang/Array001.h"
#include "02lang/Buffer001.h"
#include "02lang/SmartPtr001.h"

namespace functionality {

Lang::Lang()
{
}

Lang::~Lang()
{
}

std::string Lang::getName() const
{
    return "lang";
}

void Lang::ctor()
{
    addChildObject(new lang::Array001());
    addChildObject(new lang::Buffer001());
    addChildObject(new lang::SmartPtr001());
//    addChildObject(new lang::SmartPtr002());
}

void Lang::dtor()
{
}

}
