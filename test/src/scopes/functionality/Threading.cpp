/*
 * Threading.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Threading.h"
#include "03threading/Thread.h"
#include "03threading/Condition.h"
#include "03threading/FastMutex.h"
#include "03threading/RecursiveMutex.h"
#include "03threading/SpinLock.h"
#include "03threading/ReadWriteLock.h"
#include "03threading/ScopeGuard.h"
#include "03threading/Event.h"
#include "03threading/ThreadQueue.h"
#include "03threading/ThreadPool.h"

namespace functionality {

Threading::Threading()
{
    addChildObject(new threading::Thread());
    addChildObject(new threading::FastMutex());
    addChildObject(new threading::RecursiveMutex());
    addChildObject(new threading::SpinLock());
    addChildObject(new threading::ReadWriteLock());
//    addChildObject(new threading::ScopeGuard());
    addChildObject(new threading::Condition());
    addChildObject(new threading::Event());
    addChildObject(new threading::ThreadQueue());
    addChildObject(new threading::ThreadPool());
}

Threading::~Threading()
{
    // TODO Auto-generated destructor stub
}

std::string Threading::getName() const
{
    return "threading";
}


}
