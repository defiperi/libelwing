/*
 * Base.cpp
 *
 *  Created on: Dec 23, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Base.h"
#include "01base/Exception.h"
#include "01base/TickAndSleep.h"
#include "01base/AtomicCounter.h"
#include "01base/Properties.h"

namespace functionality {

Base::Base()
{
}

Base::~Base()
{
}

std::string Base::getName() const
{
    return "base";
}

void Base::ctor()
{
    addChildObject(new base::Exception());
    addChildObject(new base::TickAndSleep());
    addChildObject(new base::AtomicCounter());
    addChildObject(new base::Properties());

}

}
