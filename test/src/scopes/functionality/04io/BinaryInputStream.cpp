/*
 * BinaryInputStream.cpp
 *
 *  Created on: Jan 13, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "BinaryInputStream.h"

namespace functionality { namespace io {

MemoryInputStream::MemoryInputStream()
{
    // TODO Auto-generated constructor stub

}

MemoryInputStream::~MemoryInputStream()
{
    // TODO Auto-generated destructor stub
}

std::string MemoryInputStream::getName() const
{
    return "MemoryInputStream";
}

std::string MemoryInputStream::getDescription() const
{
    return "MemoryInputStream object functionality test";
}

void MemoryInputStream::run()
{
    elw::io::MemoryOutputStream os;
    elw::io::Writer w(os);
    w << (uint8)0xff;
    w << (uint16)0x00ff;
    w << (uint32)0x000000ff;
    w << 0.255f;
    w << double(255.255);
    os.flush();

    elw::lang::ByteBuffer bos0(*os.buffer());

    for (int i = 0; i < bos0.length(); ++i)
        printf("0x%02x ", bos0.ptr()[i]);
    printf("\n");


    elw::io::MemoryInputStream is(os.buffer());
    elw::io::Reader r(is);
    uint8 c;
    r >> c;
    uint16 s;
    r >> s;
    uint32 i;
    r >> i;
    float f;
    r >> f;
    double d;
    r >> d;

    os.clear();
    w << c << s << i << f << d;

    elw::lang::ByteBuffer& bos1 = *os.buffer();
    for (int i = 0; i < bos0.length(); ++i)
        printf("0x%02x ", bos0.ptr()[i]);
    printf("\n");
    for (int i = 0; i < bos1.length(); ++i)
        printf("0x%02x ", bos1.ptr()[i]);
    printf("\n");

    if (bos0 != bos1)
        elw_throw_exception(elw::lang::Exception());


}

}}
