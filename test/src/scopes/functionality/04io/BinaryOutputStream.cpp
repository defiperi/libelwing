/*
 * MemoryOutputStream.cpp
 *
 *  Created on: Jan 13, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "BinaryOutputStream.h"


namespace elw { namespace io {



class BinaryInputOutput
{
    bool m_fFlipByte;
protected:
    BinaryInputOutput(int order) :
        m_fFlipByte(false)
    {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        m_fFlipByte = (order == ByteOrder::big_endian);
#else
        m_fFlipByte = (order == ByteOrder::little_endian);
#endif
    }

    bool isFlipRequired() const throw()
    {
        return m_fFlipByte;
    }
};

class BinaryWriter :
    public BinaryInputOutput
{
    std::ostream& m_os;

public:
    BinaryWriter(std::ostream& os, int order = ByteOrder::native()) :
        BinaryInputOutput(order),
        m_os(os)
    {
    }

    BinaryWriter& operator <<(bool val)
    {
        m_os.write((char const*)&val, sizeof(val));
        return *this;
    }

    BinaryWriter& operator <<(unsigned char val)
    {
        m_os.write((char const*)&val, sizeof(val));
        return *this;
    }

    BinaryWriter& operator <<(char val)
    {
        m_os.write((char const*)&val, sizeof(val));
        return *this;
    }

    BinaryWriter& operator <<(unsigned short val)
    {
        unsigned short iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(short val)
    {
        short iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(unsigned int val)
    {
        unsigned int iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(int val)
    {
        int iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(unsigned long val)
    {
        unsigned long iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(long val)
    {
        long iVal = isFlipRequired() ? ByteOrder::flip(val) : val;
        m_os.write((char const*)&iVal, sizeof(iVal));
        return *this;
    }

    BinaryWriter& operator <<(float val)
    {
        char const* p = (char const*)&val;

        if (isFlipRequired())
        {
            p += sizeof(val);
            for (size_t i = 0; i < sizeof(val); ++i)
                m_os.write(--p, 1);
        }
        else
        {
            m_os.write(p, sizeof(val));
        }

        return *this;
    }

    BinaryWriter& operator <<(double val)
    {
        char const* p = (char const*)&val;

        if (isFlipRequired())
        {
            p += sizeof(val);
            for (size_t i = 0; i < sizeof(val); ++i)
                m_os.write(--p, 1);
        }
        else
        {
            m_os.write(p, sizeof(val));
        }

        return *this;
    }

    BinaryWriter& operator <<(std::string const& val)
    {
        operator <<((uint32)val.size());
        if (!val.empty())
            m_os.write(val.c_str(), val.size());
        return *this;
    }

    BinaryWriter& operator <<(char const* val)
    {
        uint32 u32 = (uint32)strlen(val);
        operator <<(u32);
        if (u32 != 0)
            m_os.write(val, u32);
        return *this;
    }
};

class BinaryReader :
    public BinaryInputOutput
{
    std::istream& m_is;
public:
    BinaryReader(std::istream& is, int order = ByteOrder::native()) :
        BinaryInputOutput(order),
        m_is(is)
    {
    }

    BinaryReader& operator >>(bool& val)
    {
        m_is.read((char*)&val, sizeof(val));
        return *this;
    }

    BinaryReader& operator >>(char& val)
    {
        m_is.read(&val, sizeof(val));
        return *this;
    }

    BinaryReader& operator >>(int& val)
    {
        m_is.read((char*)&val, sizeof(val));
        return *this;
    }

    BinaryReader& operator >>(long& val)
    {
        m_is.read((char*)&val, sizeof(val));
        return *this;
    }
};


}}



namespace functionality { namespace io {

MemoryOutputStream::MemoryOutputStream()
{
    // TODO Auto-generated constructor stub

}

MemoryOutputStream::~MemoryOutputStream()
{
    // TODO Auto-generated destructor stub
}

std::string MemoryOutputStream::getName() const
{
    return "MemoryOutputStream";
}

std::string MemoryOutputStream::getDescription() const
{
    return "MemoryOutputStream object functionality test";
}

void MemoryOutputStream::run()
{
    std::ostringstream oss0;
    elw::io::MemoryOutputStream osm1;


    {
    elw::io::Writer writer(oss0);

    writer << (char)1;
    writer << (unsigned char)2;
    writer << (uint8)3;
//    writer << 4;
    writer << std::string("123");

    oss0.flush();

    }
    {

    elw::io::Writer writer(osm1);

    writer << (char)1;
    writer << (unsigned char)2;
    writer << (uint8)3;
//    writer << 4;
    writer << std::string("123");

    osm1.flush();


    }

    elw::lang::ByteBuffer buff0(elw_buffer_from_str(oss0.str().c_str()));
    elw::lang::ByteBuffer& buff1 = *osm1.buffer();

    for (int i = 0; i < buff0.length(); ++i)
        printf("0x%02x ", ((char*)buff0.ptr())[i]%0xff);
    printf("\n");
    for (int i = 0; i < buff1.length(); ++i)
        printf("0x%02x ", ((char*)buff1.ptr())[i]%0xff);
    printf("\n");

    if (buff1 != buff0)
        elw_throw_exception(elw::lang::Exception("hobaaa"));
}


}}


