/*
 * SetGet.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "SetGet.h"

namespace functionality { namespace base { namespace prop {

SetGet::SetGet()
{
    // TODO Auto-generated constructor stub

}

SetGet::~SetGet()
{
    // TODO Auto-generated destructor stub
}

std::string SetGet::getName() const
{
    return "SetGet";
}

std::string SetGet::getDescription() const
{
    return "properties object set and get functionality test";
}

static void test(elw::util::Properties& p)
{
    for (int i = 0; i < 10; ++i)
    {
        std::string strKey = MKSTR("value" << i);
        if (!p.hasKey(strKey))
        {
            elw_throw_exception(elw::lang::Exception(strKey));
        }
    }

    elw::util::Properties::Keys keys;
    if (p.keys(keys).size() != 10)
        elw_throw_exception(elw::lang::Exception("size"));

    for (int i = 0; i < 10; ++i)
    {
        if (keys[i] != MKSTR("value" << i))
            elw_throw_exception(elw::lang::Exception("keys"));
    }

    for (int i = 0; i < 10; ++i)
    {
        if (MKSTR(i) != p.get(keys[i]))
            elw_throw_exception(elw::lang::Exception("value str"));

        if (i != p.get<int>(keys[i]))
            elw_throw_exception(elw::lang::Exception("value int"));

        if (float(i) != p.get<float>(keys[i]))
            elw_throw_exception(elw::lang::Exception("value float"));
    }
}

void SetGet::run()
{
    elw::util::Properties p0;
    for (int i = 0; i < 10; ++i)
        p0.put(MKSTR("value" << i), MKSTR(i));

    test(p0);
    elw::util::Properties p1(p0);
    test(p1);
    elw::util::Properties p2 = p1;
    test(p2);

    if (p2.hasKey("roket"))
        elw_throw_exception(elw::lang::Exception("key"));

    try
    {
        p2.get("roket");
        elw_throw_exception(elw::lang::Exception("get"));
    }
    catch (elw::lang::AttributeError& e)
    {
    }
}

}}}
