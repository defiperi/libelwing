/*
 * ToFileFromFile.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "ToFileFromFile.h"

namespace functionality { namespace base { namespace prop {

ToFileFromFile::ToFileFromFile()
{
    // TODO Auto-generated constructor stub

}

ToFileFromFile::~ToFileFromFile()
{
    // TODO Auto-generated destructor stub
}

std::string ToFileFromFile::getName() const
{
    return "ToFileFromFile";
}

std::string ToFileFromFile::getDescription() const
{
    return "properties object save to file and load from file functionality test";
}

void ToFileFromFile::run()
{
//    std::ifstream ofs("test.prop");
//    elw::util::Properties p;



}

}}}
