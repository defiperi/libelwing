/*
 * Exception.cpp
 *
 *  Created on: Dec 23, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Exception.h"

namespace functionality { namespace base {

class Error : public elw::lang::Exception
{
    int m_iError;
public:
    Error(int iError, std::string const& w) throw():
        elw::lang::Exception(w),
        m_iError(iError)
    {
    }

    int error() const throw()
    {
        return m_iError;
    }
};

class RuntimeError : public Error
{
public:
    RuntimeError(int iError) throw(): Error(iError, MKSTR("exception testcase. run-time error !!! err:" << iError))
    {
    }
};

class Error100 : public RuntimeError
{
public:
    Error100() throw(): RuntimeError(100)
    {
    }
};

class Error200 : public RuntimeError
{
public:
    Error200() throw(): RuntimeError(200)
    {
    }
};

static void Recursive(int iDeep, bool f);

Exception::Exception()
{
}

Exception::~Exception()
{
    // TODO Auto-generated destructor stub
}

std::string Exception::getName() const
{
    return "Exception";
}

std::string Exception::getDescription() const
{
    return "Basically exception test";
}

void Exception::run()
{
    std::string file = __FILE__;
    int line = 0;
    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(elw::lang::Exception("test"));
    }
    catch (elw::lang::Exception& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "test");
        if (strError != e.what())
            throw std::runtime_error(MKSTR("exception 1 <" << e.what() << ">"));
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (Error100& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 2");
        if (e.error() != 100)
            throw std::runtime_error("exception 2");
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (RuntimeError& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 3");
        if (e.error() != 100)
            throw std::runtime_error("exception 3");
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (Error& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 3");
        if (e.error() != 100)
            throw std::runtime_error("exception 3");
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (elw::lang::Exception& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 4");
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (std::exception& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 5");
    }

    try
    {
        line = __LINE__ + 1;
        elw_throw_exception(Error100());
    }
    catch (std::exception& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 5");
    }
    catch (elw::lang::Exception& e)
    {
       throw;
    }
    catch (Error& e)
    {
        throw;
    }
    catch (RuntimeError& e)
    {
        throw;
    }
    catch (Error100& e)
    {
        throw;
    }

    try
    {
        try
        {
            line = __LINE__ + 1;
            elw_throw_exception(Error100());
        }
        catch (elw::lang::Exception& e)
        {
            throw;
        }
    }
    catch (Error100& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 6");
    }

    try
    {
        try
        {
            line = __LINE__ + 1;
            elw_throw_exception(Error100());
        }
        catch (elw::lang::Exception& e)
        {
            throw e;
        }
    }
    catch (elw::lang::Exception& e)
    {
        std::string strError = MKSTR(file << "@" << line << ":" << "exception testcase. run-time error !!! err:100");
        if (strError != e.what())
            throw std::runtime_error("exception 7");
    }

    Recursive(100, false);

    try
    {
        Recursive(100, true);
        throw std::runtime_error("exception 8");
    }
    catch (Error& e)
    {
        if (e.error() != 1234)
            throw std::runtime_error("exception 9");
    }
}

void Recursive(int iDeep, bool f)
{
    int i = iDeep - 1;
    try
    {
        if (i >= 0)
        {
            Recursive(i, f);
        }

        if (i < 0 && f)
            throw Error(1234, "test error");

        throw RuntimeError(iDeep);
    }
    catch (RuntimeError& e)
    {
        if (iDeep != e.error())
            throw std::runtime_error("failed");
    }
}


}}
