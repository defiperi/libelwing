/*
 * TickAndSleep.cpp
 *
 *  Created on: Dec 23, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "TickAndSleep.h"

namespace functionality { namespace base {

TickAndSleep::TickAndSleep()
{
}

TickAndSleep::~TickAndSleep()
{
}

std::string TickAndSleep::getName() const
{
    return "TickAndSleep";
}

std::string TickAndSleep::getDescription() const
{
    return "sleep and tick count test";
}

void TickAndSleep::run()
{
    dword dw = elw::threading::tick();
    elw::threading::sleep(5);
    dword dw1 = elw::threading::tick()-dw;

    std::cout << "time span<5>:" << dw1 << std::endl;

    dw = elw::threading::tick();
    elw::threading::sleep(10);
    dword dw2 = elw::threading::tick()-dw;

    std::cout << "time span<10>:" << dw2 << std::endl;

    dw = elw::threading::tick();
    elw::threading::sleep(50);
    dword dw3 = elw::threading::tick()-dw;

    std::cout << "time span<50>:" << dw3 << std::endl;

    dw = elw::threading::tick();
    elw::threading::sleep(10000);
    dword dw4 = elw::threading::tick()-dw;

    std::cout << "time span<10000>:" << dw4 << std::endl;

    if (dw1 < 5 || dw1 > 8)
        elw_throw_exception(elw::lang::Exception("dw1 < 5 || dw1 > 7"));
    if (dw2 < 10 || dw2 > 13)
        elw_throw_exception(elw::lang::Exception("dw1 < 10 || dw1 > 12"));
    if (dw3 < 50 || dw3 > 53)
        elw_throw_exception(elw::lang::Exception("dw1 < 50 || dw1 > 52"));
    if (dw4 < 10000 || dw4 > 10003)
        elw_throw_exception(elw::lang::Exception("dw1 < 10000 || dw1 > 10002"));
}

}}
