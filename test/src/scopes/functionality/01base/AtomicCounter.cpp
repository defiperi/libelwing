/*
 * AtomicCounter.cpp
 *
 *  Created on: Jan 16, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "AtomicCounter.h"

namespace functionality { namespace base {

AtomicCounter::AtomicCounter()
{
    // TODO Auto-generated constructor stub

}

AtomicCounter::~AtomicCounter()
{
    // TODO Auto-generated destructor stub
}

std::string AtomicCounter::getName() const
{
    return "AtomicCounter";
}

std::string AtomicCounter::getDescription() const
{
    return "atomic counter functionality test";
}

void AtomicCounter::run()
{
    long lValue = 1765;
    long iRslt = elw::threading::atomic::set(&lValue, 774);
    if (iRslt != 1765)
        elw_throw_exception(elw::lang::Exception("atomic set"));
    if (lValue != 774)
        elw_throw_exception(elw::lang::Exception("atomic set"));

    elw::threading::AtomicCounter cnt(5);
    if (5 != (long)cnt)
        elw_throw_exception(elw::lang::Exception("5 != i"));

    if (6 != ++cnt)
        elw_throw_exception(elw::lang::Exception("++i"));
    if (6 != cnt++)
        elw_throw_exception(elw::lang::Exception("i++"));
    if (7 != (long)cnt)
        elw_throw_exception(elw::lang::Exception("7 != i"));

    if (6 != --cnt)
        elw_throw_exception(elw::lang::Exception("--i"));
    if (6 != cnt--)
        elw_throw_exception(elw::lang::Exception("i--"));
    if (5 != (long)cnt)
        elw_throw_exception(elw::lang::Exception("5 != i"));

    cnt = 0;
    if (0 != (long)cnt)
        elw_throw_exception(elw::lang::Exception("0 != i"));

//    test::runcounter<elw::threading::AtomicCounter>();
}

}}
