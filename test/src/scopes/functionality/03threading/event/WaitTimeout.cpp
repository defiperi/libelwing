/*
 * WaitTimeout.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "WaitTimeout.h"
#include "util/waitor.h"

namespace functionality { namespace threading { namespace event {

WaitTimeout::WaitTimeout()
{
}

WaitTimeout::~WaitTimeout()
{
}

std::string WaitTimeout::getName() const
{
    return "WaitTimeout";
}

std::string WaitTimeout::getDescription() const
{
    return "Event wait functionality test. timeout";
}

void WaitTimeout::run()
{
    elw::threading::Event c;

    Waitor<elw::threading::Event> w[3] { {c,100}, {c,100}, {c,100} };
    for (int i = 0; i < 3; ++i)
    {
        elw::threading::sleep(50);
        w[i].start();
    }

    for (int i = 0; i < 3; ++i)
        w[i].join();

    for (uint i = 0; i < 3; ++i)
    {
        if (elw::threading::wait::timeout != w[i].result())
            elw_throw_exception(elw::lang::Exception("result"));

        dword dwRuntime = w[i].runtime();
        std::cout << i << " running time:" << dwRuntime << std::endl;

        if (dwRuntime < 100 || dwRuntime > 105)
            elw_throw_exception(elw::lang::Exception("runtime"));

    }

}

}}}
