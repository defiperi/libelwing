/*
 * NotifyWait.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "NotifyWait.h"
#include "util/waitor.h"

namespace functionality { namespace threading { namespace event {

NotifyWait::NotifyWait()
{
}

NotifyWait::~NotifyWait()
{
}

std::string NotifyWait::getName() const
{
    return "NotifyWait";
}

std::string NotifyWait::getDescription() const
{
    return "Event wait functionality test. notified";
}

void NotifyWait::run()
{
    elw::threading::Event c;

    Waitor<elw::threading::Event> w[3] { c, c, c };
    for (int i = 0; i < 3; ++i)
        c.notify();
    for (int i = 0; i < 3; ++i)
        w[i].start();
    for (int i = 0; i < 3; ++i)
        w[i].join();

    for (uint i = 0; i < 3; ++i)
    {
        dword dwRuntime = w[i].runtime();
        std::cout << i << " running time:" << dwRuntime << std::endl;
        if (dwRuntime > 5)
            elw_throw_exception(elw::lang::Exception("runtime"));
        if (elw::threading::wait::signal != w[i].result())
            elw_throw_exception(elw::lang::Exception("result"));
    }

}

}}}
