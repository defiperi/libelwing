/*
 * Notify.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Notify.h"
#include "util/waitor.h"

namespace functionality { namespace threading { namespace condition {

Notify::Notify()
{
    // TODO Auto-generated constructor stub

}

Notify::~Notify()
{
    // TODO Auto-generated destructor stub
}

std::string Notify::getName() const
{
    return "Notify";
}

std::string Notify::getDescription() const
{
    return "Condition notify test.";
}

void Notify::run()
{
    elw::threading::Condition c;

    Waitor<elw::threading::Condition> w[3] { c, c, c };
    for (int i = 0; i < 3; ++i)
        w[i].start();

    for (int i = 0; i < 3; ++i)
        w[i].waitForStarted();

    for (int i = 0; i < 3; ++i)
    {
        elw::threading::sleep(100);
        c.signal();
    }

    dword dwTotalRuntime = 0;
    for (uint i = 0; i < 3; ++i)
    {
        w[i].join();

        if (elw::threading::wait::signal != w[i].result())
            elw_throw_exception(elw::lang::Exception("result"));

        dword dwRuntime = w[i].runtime();
        dwTotalRuntime += dwRuntime;
        std::cout << i << " running time:" << dwRuntime << std::endl;
    }

    std::cout << "total running time:" << dwTotalRuntime << std::endl;

    dword dwExpected = (100 + 200 + 300);
    if (dwTotalRuntime < dwExpected)
        elw_throw_exception(elw::lang::Exception("runtime"));
}

}}}
