/*
 * Broadcast.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Broadcast.h"
#include "util/waitor.h"

namespace functionality { namespace threading { namespace condition {

Broadcast::Broadcast()
{
    // TODO Auto-generated constructor stub

}

Broadcast::~Broadcast()
{
    // TODO Auto-generated destructor stub
}

std::string Broadcast::getName() const
{
    return "Broadcast";
}

std::string Broadcast::getDescription() const
{
    return "Condition broadcast functionality test.";
}

void Broadcast::run()
{
    elw::threading::Condition c;

    Waitor<elw::threading::Condition> w[3] { c, c, c };
    for (int i = 0; i < 3; ++i)
        w[i].start();
    for (int i = 0; i < 3; ++i)
        w[i].waitForStarted();

    elw::threading::sleep(120);
    c.broadcast();
    for (int i = 0; i < 3; ++i)
        w[i].join();

    for (int i = 0; i < 3; ++i)
    {
        if (elw::threading::wait::signal != w[i].result())
            elw_throw_exception(elw::lang::Exception("result"));

        dword dwRuntime = w[i].runtime();
        std::cout << i << " running time:" << dwRuntime << std::endl;

        if (dwRuntime < 120)
            elw_throw_exception(elw::lang::Exception("runtime"));

    }
}

}}}
