/*
 * FastMutex.h
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FunctionalityThreadingScopeGuardFastMutex_h__
#define FunctionalityThreadingScopeGuardFastMutex_h__

namespace functionality { namespace threading { namespace scopeguard {

class FastMutex :
    public utester::Test
{
public:
    std::string getName() const;
    std::string getDescription() const;

    void run();
public:
    FastMutex();
    virtual ~FastMutex();
};

}}}

#endif /* FastMutex_h__ */
