/*
 * Thread.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Thread.h"

namespace functionality { namespace threading {

Thread::Thread()
{
}

Thread::~Thread()
{
}

std::string Thread::getName() const
{
    return "Thread";
}

std::string Thread::getDescription() const
{
    return "Thread object functionality test";
}

void Thread::run()
{
    class ThreadImpl : public elw::threading::Thread
    {
        dword m_dwId;
    public:
        ThreadImpl() : m_dwId(0)
        {
        }

    public:
        void run()
        {
            m_dwId = elw::threading::selfid();
            elw::threading::sleep(10);
        }

        bool isStarted()
        {
            return m_dwId != 0;
        }

        dword id()
        {
            return m_dwId;
        }
    };

    ThreadImpl t;
    dword dw0 = elw::threading::tick();

    t.start();
    t.join();

    dword dwJoin = (elw::threading::tick() - dw0);

    std::cout << "start-stop " << dwJoin << std::endl;

    if (!t.isStarted())
        elw_throw_exception(elw::lang::Exception("thread.start()"));
    if (dwJoin < 10 || dwJoin > 30)
        elw_throw_exception(elw::lang::Exception("thread.join()"));
}

}}
