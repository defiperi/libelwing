/*
 * SingleReaderMultipleWriter.cpp
 *
 *  Created on: Mar 26, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "SingleReaderMultipleWriter.h"

namespace functionality { namespace threading { namespace rwlock {

SingleReaderMultipleWriter::SingleReaderMultipleWriter()
{
    // TODO Auto-generated constructor stub

}

SingleReaderMultipleWriter::~SingleReaderMultipleWriter()
{
    // TODO Auto-generated destructor stub
}

std::string SingleReaderMultipleWriter::getName() const
{
    return "SingleReaderMultipleWriter";
}

std::string SingleReaderMultipleWriter::getDescription() const
{
    return "ReadWriteLock functionality test. Single writer multiple reader";
}

void SingleReaderMultipleWriter::run()
{
    class WriterLockTester0 :
        public elw::threading::Thread
    {
        elw::threading::ReadWriteLock& m_lock;
        dword m_dwRunningTime;
        bool m_fStarted;
    public:
        WriterLockTester0(elw::threading::ReadWriteLock& l) :
            m_lock(l),
            m_dwRunningTime(0),
            m_fStarted(false)
        {
            start();
            while (!m_fStarted)
                elw::threading::yield();
        }

        void run()
        {
            m_fStarted = true;
            dword dw = elw::threading::tick();
            m_lock.wlock();
            m_dwRunningTime = elw::threading::tick()-dw;
            elw::threading::sleep(50);
            m_lock.unlock();
        }

        dword runtime() const
        {
            return m_dwRunningTime;
        }
    };

    elw::threading::ReadWriteLock m;
    m.rlock();

    dword dwBegin = elw::threading::tick();
    WriterLockTester0 t[2] = { m, m };

    elw::threading::sleep(50);
    m.unlock();

    for (int i = 0; i < 2; ++i)
        t[i].join();

    dword dwTotal = 0;
    for (int i = 0; i < 2; ++i)
    {
        dword dw = t[i].runtime();
        std::cout << "reader-" << i << " " << dw << std::endl;
        dwTotal += dw;
    }

    if (dwTotal < 150)
        elw_throw_exception(elw::lang::Exception("run time"));
}


}}}
