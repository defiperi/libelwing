/*
 * ScopeGuard.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "ScopeGuard.h"
#include "scopeguard/FastMutex.h"
#include "scopeguard/RecursiveMutex.h"
#include "scopeguard/SpinLock.h"
#include "scopeguard/ReadWriteLock.h"

namespace functionality { namespace threading {

ScopeGuard::ScopeGuard()
{
    addChildObject(new scopeguard::FastMutex());
    addChildObject(new scopeguard::RecursiveMutex());
    addChildObject(new scopeguard::SpinLock());
    addChildObject(new scopeguard::ReadWriteLock());
}

ScopeGuard::~ScopeGuard()
{
    // TODO Auto-generated destructor stub
}

std::string ScopeGuard::getName() const
{
    return "scopeguard";
}

}}
