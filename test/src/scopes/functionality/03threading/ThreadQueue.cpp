/*
 * ThreadQueue.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "ThreadQueue.h"
#include "queue/SingleConsumerSingleProducer.h"
#include "queue/SingleConsumerMultipleProducer.h"
#include "queue/MultipleConsumerSingleProducer.h"
#include "queue/MultipleConsumerMultipleProducer.h"

namespace functionality { namespace threading {

ThreadQueue::ThreadQueue()
{
    addChildObject(new queue::SingleConsumerSingleProducer());
    addChildObject(new queue::SingleConsumerMultipleProducer());
    addChildObject(new queue::MultipleConsumerSingleProducer());
    addChildObject(new queue::MultipleConsumerMultipleProducer());
}

ThreadQueue::~ThreadQueue()
{
    // TODO Auto-generated destructor stub
}

std::string ThreadQueue::getName() const
{
    return "queue";
}

}}
