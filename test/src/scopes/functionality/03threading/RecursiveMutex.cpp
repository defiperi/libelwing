/*
 * RecursiveMutex.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "RecursiveMutex.h"

namespace functionality { namespace threading {

RecursiveMutex::RecursiveMutex()
{
    // TODO Auto-generated constructor stub

}

RecursiveMutex::~RecursiveMutex()
{
    // TODO Auto-generated destructor stub
}

std::string RecursiveMutex::getName() const
{
    return "RecursiveMutex";
}

std::string RecursiveMutex::getDescription() const
{
    return "RecursiveMutex functionality test";
}

void RecursiveMutex::run()
{
    class RecursiveLockTester :
        public elw::threading::Thread
    {
        elw::threading::RecursiveMutex& m_lock;
        bool m_fStarted;
    public:
        RecursiveLockTester(elw::threading::RecursiveMutex& l) :
            m_lock(l),
            m_fStarted(false)
        {
            start();
            while (!m_fStarted)
                elw::threading::yield();
        }

        void run()
        {
            m_lock.lock();
            m_fStarted = true;
            elw::threading::sleep(100);
            m_lock.lock();
            elw::threading::sleep(50);
            m_lock.unlock();
            elw::threading::sleep(50);
            m_lock.unlock();
        }
    };

    elw::threading::RecursiveMutex m;
    RecursiveLockTester t(m);

    dword dw = elw::threading::tick();
    m.lock();
    dword dwTime = elw::threading::tick() - dw;
    std::cout << dwTime << std::endl;
    m.unlock();

    if (dwTime < 200)
        elw_throw_exception(elw::lang::Exception("lock time"));

    t.join();

}

}}
