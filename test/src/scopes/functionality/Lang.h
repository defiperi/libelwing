/*
 * Lang.h
 *
 *  Created on: Jan 19, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FunctionalityLang_h__
#define FunctionalityLang_h__

namespace functionality {

class Lang :
    public utester::Scope
{
public:
    std::string getName() const;

    void ctor();
    void dtor();

public:
    Lang();
    virtual ~Lang();
};

}

#endif /* Lang_h__ */
