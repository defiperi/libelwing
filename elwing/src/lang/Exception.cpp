/*
 * Exception.cpp
 *
 *  Created on: Sep 17, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace lang {

Exception::Exception() throw()
{
}

Exception::Exception(
        std::string const& str
        ) throw() :
    m_strDescription(str),
    m_strWhat(str)
{
}

Exception::~Exception() throw()
{
}

pcstr Exception::what() const throw()
{
    return m_strWhat.c_str();
}

void Exception::location(std::string const& strFile, int iLine) throw()
{
    m_strLocation = MKSTR(strFile << "@" << iLine);

    std::ostringstream os;
    os << m_strLocation << ":";
    os << m_strDescription;
    m_strWhat = os.str().c_str();
}

pcstr Exception::location() const throw()
{
    return m_strLocation.c_str();
}

pcstr Exception::description() const throw()
{
    return m_strDescription.c_str();
}

ClassNotFoundException::ClassNotFoundException(
        std::string const& str
        ) throw() :
    Exception("ClassNotFoundException <" + str + ">")
{
}


SystemErrorException::SystemErrorException() throw() :
    Exception(MKSTR("SystemErrorException errno:" << errno << " (" << strerror(errno) << ")"))
{
}

SystemErrorException::SystemErrorException(
        std::string const& str
        ) throw() :
    Exception(MKSTR("SystemErrorException <" << str << "> errno:" << errno << " (" << strerror(errno) << ")"))
{
}

OutOfBoundsException::OutOfBoundsException() throw() :
    Exception("OutOfBoundsException")
{
}

OutOfBoundsException::OutOfBoundsException(
        std::string const& str
        ) throw() :
    Exception(MKSTR("OutOfBoundsException msg:" << str))
{
}

IllegalStateException::IllegalStateException() throw() :
    Exception("IllegalStateException")
{

}

IllegalStateException::IllegalStateException(
        std::string const& msg
        ) throw() :
    Exception(MKSTR("IllegalStateException msg:" << msg))
{
}

}}


