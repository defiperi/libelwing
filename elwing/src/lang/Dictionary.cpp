/*
 * Dictionary.cpp
 *
 *  Created on: Sep 12, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace lang {

Dictionary::Dictionary()
{
}

Dictionary::Dictionary(Dictionary const& that)
{
    std::map<std::string, Any*>::const_iterator it;
    for (it = that.m_map.begin(); it != that.m_map.end(); ++it)
        m_map[(*it).first] = new Any(*(*it).second);
}

Dictionary::~Dictionary()
{
    std::map<std::string, Any*>::iterator it;
    for (it = m_map.begin(); it != m_map.end(); ++it)
        delete (*it).second;
    m_map.clear();
}

Any const& Dictionary::getObj(std::string const& key) const
{
    std::map<std::string, Any*>::const_iterator it = m_map.find(key);
    if (it == m_map.end())
        throw AttributeError(key);
    return *((*it).second);
}

bool Dictionary::hasKey(std::string const& key) const throw()
{
    return m_map.find(key) != m_map.end();
}

Dictionary::Keys& Dictionary::keys(Keys& lst) const throw()
{
    std::map<std::string, Any*>::const_iterator it;
    for (it = m_map.begin(); it != m_map.end(); ++it)
        lst.push_back((*it).first);
    return lst;
}

void Dictionary::remove(std::string const& key) throw()
{
    std::map<std::string, Any*>::iterator it = m_map.find(key);
    if (it == m_map.end())
        return;
    delete (*it).second;
    m_map.erase(it);
}

}}
