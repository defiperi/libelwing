/*
 * ByteBuffer.cpp
 *
 *  Created on: Jan 13, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace lang {

ByteBuffer::ByteBuffer(size_t nCapacity) :
    m_pvBuff(NULL),
    m_nCapacity(nCapacity),
    m_nLength(0)
{
    m_pvBuff = (uint8*)::malloc(m_nCapacity);
    if (NULL == m_pvBuff)
        throw elw::lang::Exception("Memory allocation failure");
}

ByteBuffer::ByteBuffer(ByteBuffer const& that) :
    m_pvBuff(NULL),
    m_nCapacity(0),
    m_nLength(0)
{
//    m_pvBuff = (uint8*)::malloc(m_nCapacity);
//    if (NULL == m_pvBuff)
//        throw elw::lang::Exception("Memory allocation failure");
    copy(that.m_pvBuff, that.m_nLength);
}

ByteBuffer::ByteBuffer(void const* pv, size_t len) :
    m_pvBuff(NULL),
    m_nCapacity(0),
    m_nLength(0)
{
    if (NULL == pv)
        throw elw::lang::Exception("Invalid pointer");
    if (0 == len)
        throw elw::lang::Exception("Invalid data length");
//    m_pvBuff = (uint8*)::malloc(m_nCapacity);
//    if (NULL == m_pvBuff)
//        throw elw::lang::Exception("Memory allocation failure");
    copy(pv, len);
}


ByteBuffer::~ByteBuffer()
{
    ::free(m_pvBuff);
}

size_t ByteBuffer::length() const throw()
{
    return m_nLength;
}

void ByteBuffer::count(size_t len)
{
    if (m_nCapacity < len)
        throw elw::lang::OutOfBoundsException();
    m_nLength = len;
}

size_t ByteBuffer::capacity() const throw()
{
    return m_nCapacity;
}

void ByteBuffer::clear() throw()
{
    m_nLength = 0;
}

uint8* ByteBuffer::ptr() throw()
{
    return m_pvBuff;
}

uint8 const* ByteBuffer::ptr() const throw()
{
    return m_pvBuff;
}

void ByteBuffer::copy(void const* pv, size_t len)
{
    if (NULL == pv)
        throw elw::lang::Exception("Invalid pointer");
    if (0 == len)
        throw elw::lang::Exception("Invalid data length");

    if (m_nCapacity < len)
        resize(len);
    ::memcpy(m_pvBuff, pv, len);
    m_nLength = len;
}

void ByteBuffer::append(void const* pv, size_t len)
{
    if (NULL == pv)
        throw elw::lang::Exception("Invalid pointer");
    if (0 == len)
        throw elw::lang::Exception("Invalid data length");

    size_t nFree = m_nCapacity-m_nLength;
    if (nFree < len)
        resize(m_nCapacity + std::max(m_nCapacity, len));
    ::memcpy((char*)m_pvBuff+m_nLength, pv, len);
    m_nLength += len;
}

ssize_t ByteBuffer::find(ByteBuffer const& needle, size_t begin, ssize_t end) const throw()
{
    if (begin >= length())
        return -1;
    size_t e = end != -1 ? end : length();
    if (e > length())
        e = length();
    if (begin >= e)
        return -1;
    if ((e - begin) < needle.length())
        return -1;
    uint8 const* pbStart = ptr();
    uint8* pbFind = (uint8*)memmem(pbStart+begin, e-begin, needle.ptr(), needle.length());
    if (pbFind == NULL)
        return -1;
    return pbFind - pbStart;
}


void ByteBuffer::resize(size_t nNewCapacity)
{
    if (nNewCapacity > m_nCapacity)
    {
        uint8* pv = (uint8*)::realloc(m_pvBuff, nNewCapacity);
        if (NULL == pv)
            throw elw::lang::Exception("Memory allocation failure");
        m_pvBuff = pv;
        m_nCapacity = nNewCapacity;
    }
    else if (nNewCapacity < m_nCapacity)
    {
        m_nCapacity = nNewCapacity;
        if (m_nLength > nNewCapacity)
            m_nLength = nNewCapacity;
    }
    else
    {
    }
}

ByteBuffer& ByteBuffer::operator =(ByteBuffer const& that)
{
    copy(that.m_pvBuff, that.m_nLength);
    return (*this);
}

ByteBuffer& ByteBuffer::operator +=(ByteBuffer const& that)
{
    append(that.m_pvBuff, that.m_nLength);
    return (*this);
}

bool operator ==(ByteBuffer const& b1, ByteBuffer const& b2)
{
    if (b1.length() != b2.length())
        return false;
    return 0 == memcmp(b1.ptr(), b2.ptr(), b1.length());
}

bool operator !=(ByteBuffer const& b1, ByteBuffer const& b2)
{
    return !(b1 == b2);
}

}}
