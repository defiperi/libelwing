/*
 * Writer.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace io {

Writer::Writer(std::ostream& os, int iByteOrder) :
    DataReadWrite(iByteOrder),
    m_os(os)
{
}

Writer& Writer::operator <<(bool val)
{
    m_os.write((char const*)&val, sizeof(val));
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(unsigned char val)
{
    m_os.write((char const*)&val, sizeof(val));
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(char val)
{
    m_os.write((char const*)&val, sizeof(val));
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(unsigned short val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(short val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(unsigned int val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(int val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(unsigned long val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(long val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(float val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(double val)
{
    if (isFlipRequired())
    {
        unsigned short iVal = ByteOrder::flip(val);
        m_os.write((char const*)&iVal, sizeof(iVal));
    }
    else
    {
        m_os.write((char const*)&val, sizeof(val));
    }
    m_os.flush();
    return *this;
}

Writer& Writer::operator <<(std::string const& val)
{
    if (!val.empty())
    {
        m_os.write(val.c_str(), val.size());
        m_os.flush();
    }
    return *this;
}

Writer& Writer::operator <<(lang::ByteBuffer const& val)
{
    if (val.length() != 0)
    {
        m_os.write((char const*)val.ptr(), val.length());
        m_os.flush();
    }
    return *this;
}

Writer& Writer::write(void* pv, size_t l)
{
    m_os.write((char*)pv, l);
    m_os.flush();
    return *this;
}


}}
