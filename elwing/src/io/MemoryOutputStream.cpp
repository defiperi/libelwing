/*
 * MemoryOutputStream.cpp
 *
 *  Created on: Dec 20, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace io {

class MemoryOutputStreamBuffer :
    public std::streambuf
{
    int m_nBlockSize;
    lang::SmartPtr<lang::ByteBuffer> m_ptrBuff;
    int m_iResizeCnt;
protected:
    MemoryOutputStreamBuffer(size_t nInitialBufferSize) :
        m_nBlockSize((nInitialBufferSize > 8 ? nInitialBufferSize : 8)),
        m_ptrBuff(new lang::ByteBuffer(m_nBlockSize)),
        m_iResizeCnt(0)
    {
        setp((char*)m_ptrBuff->ptr(), (char*)m_ptrBuff->ptr() + m_ptrBuff->capacity());
    }


    int_type overflow(int_type ch)
    {
        size_t nCrnt = m_ptrBuff->capacity();
        try
        {
            m_ptrBuff->resize(m_ptrBuff->capacity()+m_nBlockSize);
        }
        catch(...)
        {
            return traits_type::eof();
        }
        setp((char*)m_ptrBuff->ptr() + nCrnt, (char*)m_ptrBuff->ptr() + m_ptrBuff->capacity());
        *pptr() = traits_type::to_char_type(ch);
        setp((char*)m_ptrBuff->ptr() + nCrnt + 1, (char*)m_ptrBuff->ptr() + m_ptrBuff->capacity());
        ++m_iResizeCnt;
        return ch;

    }

    int sync()
    {
        size_t nCnt = static_cast<size_t>(pptr() - pbase());
        if (m_iResizeCnt > 0)
        {
            nCnt += m_nBlockSize*m_iResizeCnt;
            nCnt += 1; // overflow char
        }
        m_ptrBuff->count(nCnt);

        return 0;
    }

    lang::SmartPtr<lang::ByteBuffer> buffer()
    {
        return m_ptrBuff;
    }

    void clear()
    {
        setp((char*)m_ptrBuff->ptr(), (char*)m_ptrBuff->ptr() + m_ptrBuff->capacity());
        m_ptrBuff->clear();
    }

    friend class MemoryOutputStream;
};

MemoryOutputStream::MemoryOutputStream(size_t nInitialBufferSize) :
    std::ostream(new MemoryOutputStreamBuffer(nInitialBufferSize))
{
}

MemoryOutputStream::~MemoryOutputStream()
{
    delete rdbuf();
}

lang::SmartPtr<lang::ByteBuffer> MemoryOutputStream::buffer()
{
    return ((MemoryOutputStreamBuffer*)rdbuf())->buffer();
}

void MemoryOutputStream::clear()
{
    std::ostream::clear();
    return ((MemoryOutputStreamBuffer*)rdbuf())->clear();
}

}}
