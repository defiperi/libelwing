/*
 * ByteOrder.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace io {

namespace ByteOrder {

int native()
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
        return little_endian;
#else
        return big_endian;
#endif
}

uint16 flip(uint16 u16) throw()
{
    return ((u16 >> 8) & 0x00ff) | ((u16 << 8) & 0xff00);
}

int16 flip(int16 i16) throw()
{
    return (int16)flip((uint16)i16);
}

uint32 flip(uint32 u32) throw()
{
    return ((uint32)flip((uint16)(u32 >> 16))) |
            (((uint32)flip((uint16)(u32 & 0xffff))) << 16);
}

int32 flip(int32 i32) throw()
{
    return (int32)flip((uint32)i32);
}

uint64 flip(uint64 u64) throw()
{
    return ((uint64)flip((uint32)(u64 >> 32))) |
            (((uint64)flip((uint32)(u64 & 0xffffffff))) << 32);
}

int64 flip(int64 i64) throw()
{
    return (int64)flip((uint64)i64);
}

template <class _Ty>
_Ty __flip__(_Ty val) throw()
{
    _Ty target = val;
    char const* psrc = (char const*)&val + sizeof(val);
    char* pdst = (char*)&target;

    for (uint i = 0; i < sizeof(val); ++i)
        *pdst++ = *(--psrc);
    return target;
}

float flip(float fl) throw()
{
    return __flip__<float>(fl);
}

double flip(double db) throw()
{
    return __flip__<double>(db);
}


}

}}

