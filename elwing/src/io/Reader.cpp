/*
 * Reader.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace io {

Reader::Reader(std::istream& is, int iByteOrder) :
    DataReadWrite(iByteOrder),
    m_is(is)
{
}

bool Reader::eof()
{
    return m_is.peek() == EOF;
}

Reader& Reader::operator >>(bool& val)
{
    val = read<bool>();
    return *this;
}

Reader& Reader::operator >>(unsigned char& val)
{
    val = read<unsigned char>();
    return *this;
}

Reader& Reader::operator >>(char& val)
{
    val = read<char>();
    return *this;
}

Reader& Reader::operator >>(unsigned short& val)
{
    val = read<unsigned short>();
    return *this;
}

Reader& Reader::operator >>(short& val)
{
    val = read<short>();
    return *this;
}

Reader& Reader::operator >>(unsigned int& val)
{
    val = read<unsigned int>();
    return *this;
}

Reader& Reader::operator >>(int& val)
{
    val = read<int>();
    return *this;
}

Reader& Reader::operator >>(unsigned long& val)
{
    val = read<unsigned long>();
    return *this;
}

Reader& Reader::operator >>(long& val)
{
    val = read<long>();
    return *this;
}

Reader& Reader::operator >>(float& val)
{
    val = read<float>();
    return *this;
}

Reader& Reader::operator >>(double& val)
{
    val = read<double>();
    return *this;
}

Reader& Reader::operator >>(lang::ByteBuffer& val)
{
    read(val.ptr(), val.capacity());
    val.count(val.capacity());
    return *this;
}

Reader& Reader::read(void* pv, size_t l)
{
    size_t r = m_is.readsome((char*)pv, l);
    if (l != r)
        elw_throw_exception(elw::lang::Exception(MKSTR("failed to read input stream !!! " << r << ":" << l)));
    return *this;
}

}}
