/*
 * MemoryInputStream.cpp
 *
 *  Created on: Dec 20, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace io {


class MemoryInputStreamBuffer :
    public std::streambuf
{
    lang::SmartPtr<lang::ByteBuffer> m_ptrBuff;
protected:
    MemoryInputStreamBuffer(lang::SmartPtr<lang::ByteBuffer> const& ptrBuff) :
        m_ptrBuff(ptrBuff)
    {
        setBuff(ptrBuff);
    }

    int_type underflow()
    {
        return traits_type::eof();
    }

    void setBuff(lang::SmartPtr<lang::ByteBuffer> const& ptrBuff)
    {
        setg((char*)ptrBuff->ptr(), (char*)ptrBuff->ptr(), (char*)ptrBuff->ptr()+ptrBuff->length());
    }

    friend class MemoryInputStream;
};

MemoryInputStream::MemoryInputStream(const lang::ByteBuffer& rBuffer) :
    std::istream(new MemoryInputStreamBuffer(new lang::ByteBuffer(rBuffer)))
{
}

MemoryInputStream::MemoryInputStream(const lang::SmartPtr<lang::ByteBuffer>& ptrBuffer) :
    std::istream(new MemoryInputStreamBuffer(ptrBuffer))
{
}

MemoryInputStream::~MemoryInputStream()
{
    delete rdbuf();
}

lang::SmartPtr<lang::ByteBuffer>& MemoryInputStream::buffer()
{
    return ((MemoryInputStreamBuffer*)rdbuf())->m_ptrBuff;
}

void MemoryInputStream::buffer(lang::SmartPtr<lang::ByteBuffer>& ptrBuff)
{
    clear();
    return ((MemoryInputStreamBuffer*)rdbuf())->setBuff(ptrBuff);
}

}}

