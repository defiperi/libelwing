/*
 * Condition.cpp
 *
 *  Created on: Sep 30, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace threading {

Event::Event(Mutex* pMtx) :
    m_iNotifyCnt(0),
    m_iWaitCnt(0),
    m_pMtx(pMtx),
    m_fPrivateMutex(false),
    m_pCond(NULL)
{
    if (m_pMtx == NULL)
    {
        m_pMtx = new FastMutex();
        m_fPrivateMutex = true;
    }

    m_pCond = new Condition(m_pMtx);
}

Event::~Event()
{
}

int Event::wait()
{
    if (m_fPrivateMutex)
        m_pMtx->lock();

    ++m_iWaitCnt;
    while (m_iNotifyCnt == 0)
        m_pCond->wait();
    --m_iNotifyCnt;
    --m_iWaitCnt;

    if (m_fPrivateMutex)
        m_pMtx->unlock();

    return 0;
}

int Event::wait(dword dwTimeout)
{
    int iRslt = 0;

    if (m_fPrivateMutex)
        m_pMtx->lock();

    ++m_iWaitCnt;
    while (m_iNotifyCnt == 0)
    {
        iRslt = m_pCond->wait(dwTimeout);
        if (iRslt == 1)
            break;
    }

    if (iRslt == 0)
        --m_iNotifyCnt;
    --m_iWaitCnt;

    if (m_fPrivateMutex)
        m_pMtx->unlock();

    return iRslt;
}

bool Event::notify()
{
    if (m_fPrivateMutex)
        m_pMtx->lock();
    bool fHaveWaiters = (m_iWaitCnt > 0);
    ++m_iNotifyCnt;
    if (m_fPrivateMutex)
        m_pMtx->unlock();
    bool fRslt = true;
    if (fHaveWaiters)
        fRslt = m_pCond->signal();
    return fRslt;
}

bool Event::notifyAll()
{
    if (m_fPrivateMutex)
        m_pMtx->lock();
    m_iNotifyCnt = m_iWaitCnt;
    if (m_fPrivateMutex)
        m_pMtx->unlock();
    m_pCond->broadcast();

    return true;
}

}}
