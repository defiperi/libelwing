/*
 * Timer.cpp
 *
 *  Created on: Feb 13, 2013
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "timer/Table.h"

namespace elw { namespace threading {

Timer::Task::Task() :
    m_pTimer(NULL)
{
}

Timer::Task::~Task()
{
    timer::TimerContextPtr ptrTimer = timer::Table::instance().get(m_pTimer);
    if (NULL == ptrTimer)
        return;
    ScopeGuard g(*ptrTimer);
    timer::TaskContextPtr ptrTask = ptrTimer->getTask(this);
    if (ptrTask == NULL)
        return;
    ptrTask->lock();
    ptrTimer->removeTask(this);
    ptrTask->unlock();
}

void Timer::Task::setTimer(Timer* pTimer)
{
    m_pTimer = pTimer;
}

void Timer::Task::cancel() throw()
{
    timer::TimerContextPtr ptr = timer::Table::instance().get(m_pTimer);
    if (NULL != ptr)
        ptr->removeTask(this);
}

void Timer::Task::onExpire()
{
    std::cout << "Timer::Task::onExpire()" << std::endl;
}

Timer::Timer(dword dwResolution)
{
    timer::TimerContextPtr ptrTimer = timer::Table::instance().get(this);
    if (ptrTimer != NULL)
    {
        ptrTimer->cancel();
        timer::Table::instance().erase(this);
    }

    timer::Table::instance().put(this, new timer::TimerContext(dwResolution));
}

Timer::~Timer() throw()
{
    cancel();
}

void Timer::start(
        Task& task,
        dword dwInterval
        )
{
    dword dwint = dwInterval > 10 ? dwInterval : 10;

    timer::TimerContextPtr ptrTimer = timer::Table::instance().get(this);
    ptrTimer->lock();
    timer::TaskContextPtr ptrTask = ptrTimer->getTask(&task);
    if (ptrTask != NULL)
    {
        ScopeGuard g(*ptrTask);
        ptrTask->setInterval(dwint);
    }
    else
    {
        ptrTimer->addTask(new timer::TaskContext(task, dwint));
    }

    task.setTimer(this);

    ptrTimer->unlock();
}

void Timer::cancel() throw()
{
    timer::TimerContextPtr ptrTimer = timer::Table::instance().get(this);
    if (ptrTimer != NULL)
        ptrTimer->cancel();
}

}}
