/*
 * Thread.cpp
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace threading {

Thread::Thread(IRunnable* pRunnable) :
    m_pImpl(NULL),
    m_pRunnable(pRunnable)
{
    m_pImpl = (IThreadPolicy*)CreatePolicyImpl(ELW_THREAD_POLICY_IMPL_KEYWORD);
}

Thread::~Thread()
{
    delete m_pImpl;
}

void Thread::start()
{
    m_pImpl->start(
            m_pRunnable != NULL ? *m_pRunnable : *this
            );
}

void Thread::join() throw()
{
    m_pImpl->join();
}

dword Thread::getid() const throw()
{
    return m_pImpl->getid();
}

void Thread::run()
{
}

}}
