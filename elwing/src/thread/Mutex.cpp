/*
 * Mutex.cpp
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace threading {

namespace mutex { namespace type {

std::string tostr(Type t)
{
    if (t == recursive)
        return "recursive";
    else if (t == fast)
        return "fast";
    else if (t == timed)
        return "timed";
    return MKSTR("unknown (" << t << ")");
}
}}

Mutex::Mutex(mutex::Type t) :
    m_pImpl(NULL)
{
    m_pImpl = (IMutexPolicy*)CreatePolicyImpl(ELW_MUTEX_POLICY_IMPL_KEYWORD);
    if (!m_pImpl->create(t))
        throw elw::lang::SystemErrorException(
                MKSTR("failed to create mutex object. type:" << mutex::type::tostr(t))
                );
}

Mutex::~Mutex()
{
    delete m_pImpl;
}

void Mutex::lock()
{
    if (!m_pImpl->lock())
        throw elw::lang::SystemErrorException("failed to lock mutex !!!");
}

void Mutex::unlock()
{
    if (!m_pImpl->unlock())
        throw elw::lang::SystemErrorException("failed to unlock mutex !!!");
}

bool Mutex::trylock()
{
    mutex::Result rslt = m_pImpl->trylock();
    switch (rslt)
    {
    case mutex::success:
        return true;
    case mutex::busy:
        return false;
    default:
        break;
    }

    throw elw::lang::SystemErrorException("failed to lock mutex !!!");
}

///////////////////////////////////////////////////////////

FastMutex::FastMutex() :
    Mutex(mutex::type::fast)
{
}

///////////////////////////////////////////////////////////

RecursiveMutex::RecursiveMutex() :
    Mutex(mutex::type::recursive)
{
}

///////////////////////////////////////////////////////////

TimedMutex::TimedMutex() :
    Mutex(mutex::type::timed)
{
}

bool TimedMutex::trylock(dword dwmilis)
{
    int iRslt = m_pImpl->trylock(dwmilis);
    switch (iRslt)
    {
    case mutex::success:
        return true;
    case mutex::timedout:
        return false;
    default:
        break;
    }

    throw elw::lang::SystemErrorException("failed to lock mutex !!!");
}

}}
