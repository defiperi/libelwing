/*
 * ReadWriteLock.cpp
 *
 *  Created on: Dec 16, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace threading {


ReadWriteLock::ReadWriteLock() :
    m_cond(&m_lock),
    m_uLockCnt(0),
    m_uPendingReaders(0),
    m_uPendingWriters(0),
    m_dwWriterThread(0)
{
}

ReadWriteLock::~ReadWriteLock()
{

}

bool ReadWriteLock::isReadable(dword dwtid)
{
    if (m_uPendingWriters > 0)
        return false;
    if (m_dwWriterThread == 0 || m_dwWriterThread == dwtid)
        return true;
    return false;
}

bool ReadWriteLock::isWritable(dword dwtid)
{
    if (m_uLockCnt == 0)
        return true;
    if (m_dwWriterThread == dwtid)
        return true;
    return false;
}

void ReadWriteLock::notify()
{
    if (m_uPendingReaders > 0 || m_uPendingWriters > 0)
        m_cond.broadcast();
}

void ReadWriteLock::rlock()
{
    dword dwtid = selfid();
    ScopeGuard g(m_lock);
    if (!isReadable(dwtid))
    {
        ++m_uPendingReaders;
        do
        {
            m_cond.wait();
        }
        while (!isReadable(dwtid));
        --m_uPendingReaders;
    }

    ++m_uLockCnt;
}

void ReadWriteLock::wlock()
{
    dword dwtid = selfid();

    ScopeGuard g(m_lock);
    if (!isWritable(dwtid))
    {
        ++m_uPendingWriters;
        do
        {
            m_cond.wait();
        }
        while (!isWritable(dwtid));
        --m_uPendingWriters;
    }

    ++m_uLockCnt;
    m_dwWriterThread = dwtid;
}

void ReadWriteLock::unlock()
{
    ScopeGuard g(m_lock);
    if (m_uLockCnt == 0)
        return;
    if (m_dwWriterThread != 0)
    {
        if (m_dwWriterThread != selfid())
            return;
        if (--m_uLockCnt == 0)
        {
            m_dwWriterThread = 0;
            notify();
        }
    }
    else
    {
        if (--m_uLockCnt == 0)
            notify();
    }
}

bool ReadWriteLock::tryrlock()
{
    return false;
}

bool ReadWriteLock::trywlock()
{
    return false;
}


}}
