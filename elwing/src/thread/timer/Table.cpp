/*
 * Table.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "Table.h"

namespace elw { namespace threading { namespace timer {

Table& Table::instance() throw()
{
    static Table t;
    return t;
}

Table::Table() throw()
{
}

TimerContextPtr Table::get(Timer* pTimer) throw()
{
    ScopeReadGuard g(m_lock);
    TimerContextPtr ptrTimer;
    Timers::iterator it = m_timers.find(reinterpret_cast<ptr>(pTimer));
    if (it != m_timers.end())
        ptrTimer = (*it).second;
    return ptrTimer;
}

void Table::put(Timer* pTimer, TimerContextPtr ptrCntx) throw()
{
    ScopeWriteGuard g(m_lock);
    m_timers.insert(Timers::value_type(reinterpret_cast<ptr>(pTimer), ptrCntx));
}

void Table::erase(Timer* pTimer) throw()
{
    ScopeWriteGuard g(m_lock);
    m_timers.erase(reinterpret_cast<ptr>(pTimer));
}

}}}


