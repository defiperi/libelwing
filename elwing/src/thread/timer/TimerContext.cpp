/*
 * TimerContext.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "TimerContext.h"

namespace elw { namespace threading { namespace timer {

TimerContext::TimerContext(dword dwResolution) :
    m_dwResolution(dwResolution),
    m_state(idle)
{
    Thread::start();
}

TimerContext::~TimerContext()
{
    bool fIsIdle = m_state == idle;
    m_state = closing;
    if (fIsIdle)
        m_evnt.notify();
    join();
}

uint TimerContext::state() throw()
{
    ScopeGuard g(*this);
    return m_state;
}

void TimerContext::addTask(TaskContextPtr ptrCntx)
{
    ScopeGuard g(*this);
    m_tasks.insert(Tasks::value_type(ptrCntx->getid(), ptrCntx));
    if (m_state == idle)
    {
        m_state = running;
        m_evnt.notify();
    }
}

TaskContextPtr TimerContext::getTask(Timer::Task* pTask) throw()
{
    ScopeGuard g(*this);
    TaskContextPtr ptrTask;
    Tasks::const_iterator it = m_tasks.find(TaskContext::getid(pTask));
    if (it != m_tasks.end())
        ptrTask = (*it).second;
    return ptrTask;
}

void TimerContext::removeTask(Timer::Task* pTask) throw()
{
    ScopeGuard g(*this);
    m_tasks.erase(TaskContext::getid(pTask));
    if (m_tasks.empty())
        m_state = idle;
}

void TimerContext::cancel() throw()
{
    ScopeGuard g(*this);
    m_state = idle;
    m_tasks.clear();
}

void TimerContext::checkExpire()
{
    ScopeGuard g(*this);
    for (Tasks::iterator it = m_tasks.begin(); it != m_tasks.end(); ++it)
    {
        TaskContextPtr ptr = (*it).second;
        {
            ScopeGuard g(*ptr);
            if (ptr->hasExpire())
                ptr->getTask().onExpire();
        }
    }
}

void TimerContext::onIdle()
{
    m_evnt.wait();
}

void TimerContext::onRunning()
{
    do
    {
//        listOfTasks(tasks);
//        if (tasks.empty())
//            break;

        ::poll(NULL, 0, m_dwResolution);

        checkExpire();
    }
    while (state() == running);
}


void TimerContext::run()
{
    uint st;
    while ((st = state()) != closing)
    {
        switch (st)
        {
        case running:
            onRunning();
            break;
        case idle:
            onIdle();
            break;
        }
    }
}


}}}
