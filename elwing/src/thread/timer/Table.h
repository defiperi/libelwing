/*
 * Tables.h
 *
 *  Created on: Feb 14, 2013
 *      Author: bulentk
 *
 */

#ifndef ElwingThreadingTimerTables_h__
#define ElwingThreadingTimerTables_h__

#include "TimerContext.h"

namespace elw { namespace threading { namespace timer {

class ELW_HIDE Table
{
public:
    static Table& instance() throw();

private:
    typedef std::map<ptr, TimerContextPtr> Timers;
    Timers m_timers;
    ReadWriteLock m_lock;

public:
    TimerContextPtr get(
            Timer* pTimer
            ) throw();

    void put(
            Timer* pTimer,
            TimerContextPtr ptrCntx
            ) throw();

    void erase(
            Timer* pTimer
            ) throw();

protected:
    Table() throw();
};

}}}

#endif /* Tables_h__ */
