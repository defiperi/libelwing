/*
 * TimerContext.h
 *
 *  Created on: Aug 19, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThredingTimerTimerContext_h__
#define ElwingThredingTimerTimerContext_h__

#include "TaskContext.h"

namespace elw { namespace threading { namespace timer {

class ELW_HIDE TimerContext :
    public Thread,
    public RecursiveMutex
{
    enum
    {
        none,
        idle,
        running,
        closing
    };

    typedef std::map<ptr, TaskContextPtr> Tasks;

    dword m_dwResolution;
    Tasks m_tasks;
    uint m_state;
    Event m_evnt;

public:
    void cancel() throw();
public:
    void addTask(
            TaskContextPtr ptrCntx
            );
    TaskContextPtr getTask(
            Timer::Task* pTask
            ) throw();
    void removeTask(
            Timer::Task* pTask
            ) throw();
public:
    TimerContext(dword dwResolution);
    virtual ~TimerContext();

protected:
    void run();
    void onIdle();
    void onRunning();

    uint state() throw();

    void checkExpire();
//    void listOfTasks(std::vector<TaskContextPtr>& lst) throw();
};

typedef lang::SmartPtr<TimerContext> TimerContextPtr;

}}}

#endif /* TimerContext_h__ */
