/*
 * TaskContext.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"
#include "TaskContext.h"

namespace elw { namespace threading { namespace timer {

TaskContext::TaskContext(Timer::Task& task, dword dwInterval) :
    m_task(task),
    m_dwInterval(dwInterval),
    m_dwTick(tick()),
    m_fHasCancel(false)
{
}

TaskContext::~TaskContext()
{
}

void TaskContext::setInterval(dword dw) throw()
{
    m_dwInterval = dw;
}

bool TaskContext::hasExpire() throw()
{
    bool fRslt = false;
    dword dwTick = tick();
    dword dwInterval = dwTick >= m_dwTick ? dwTick - m_dwTick : (MAX_ULONG - m_dwTick) + dwTick;
    if (dwInterval >= m_dwInterval)
    {
        m_dwTick = dwTick;
        fRslt = true;
    }

    return fRslt;
}

ptr TaskContext::getid() const throw()
{
    return getid(&m_task);
}

ptr TaskContext::getid(Timer::Task* pTask) throw()
{
    return reinterpret_cast<ptr>(pTask);
}

Timer::Task& TaskContext::getTask() throw()
{
    return m_task;
}

}}}
