/*
 * Property.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace util {

Properties::Properties()
{
}

Properties::Properties(const Properties& that) :
    m_map(that.m_map)
{
}

Properties::~Properties()
{
    m_map.clear();
}

void Properties::put(
        std::string const& strKey,
        std::string const& strValue
        )
{
    m_map[strKey] = strValue;
}

std::string const& Properties::get(
        std::string const& strKey
        ) const
{
    Attributes::const_iterator it = m_map.find(strKey);
    if (it == m_map.end())
        throw lang::AttributeError(strKey);
    return (*it).second;
}

std::string const& Properties::get(
        std::string const& strKey,
        std::string const& strDefault
        ) const
{
    Attributes::const_iterator it = m_map.find(strKey);
    if (it == m_map.end())
        return strDefault;
    return (*it).second;
}

Properties::Keys& Properties::keys(Keys& keys) const
{
    Attributes::const_iterator it;
    for (it = m_map.begin(); it != m_map.end(); ++it)
        keys.push_back((*it).first);
    return keys;
}

bool Properties::hasKey(std::string const& strKey) const
{
    bool fRslt = false;
    Attributes::const_iterator it = m_map.find(strKey);
    if (it != m_map.end())
        fRslt = true;
    return fRslt;
}

std::ifstream& operator >>(std::ifstream& is, Properties& prop)
{
    enum {
        idle,
        value
    };

    int state = idle;
    std::string strLine;
    std::string strKey;
    std::vector<std::string> lstValues;

    while (!is.eof())
    {
        std::getline(is, strLine);
        strLine.erase(0, strLine.find_first_not_of("\t "));
        if (strLine.empty())
            continue;
        if (strLine.find("#") == 0)
            continue;

        if (state == idle)
        {
            size_t npos = strLine.find("=");
            if (npos == std::string::npos)
                continue;

            strKey = strLine.substr(0, npos);
            std::string strValue = strLine.substr(npos+1);

            strKey.erase(strKey.find_last_not_of("\t ") + 1);

            strValue.erase(0, strValue.find_first_not_of("\t "));
            strValue.erase(strValue.find_last_not_of("\t ") + 1);

            size_t npossl = strValue.find("\\");

            if (npossl != std::string::npos && npossl == strValue.length()-1)
            {
                strValue.erase(strValue.find_last_not_of("\\\t ") + 1);
                lstValues.push_back(strValue);
                state = value;
            }
            else
            {
                if (!strKey.empty() && !strValue.empty())
                    prop.put(strKey, strValue);
            }
        }
        else if (state == value)
        {
            size_t npossl = strLine.find("\\");
            if (npossl != std::string::npos && npossl == strLine.length()-1)
            {
                strLine.erase(strLine.find_last_not_of("\\\t ") + 1);
                lstValues.push_back(strLine);
            }
            else
            {
                std::string strValue;
                std::vector<std::string>::iterator it;
                for (it = lstValues.begin(); it != lstValues.end(); ++it)
                    strValue += (*it) + " ";

                strLine.erase(strLine.find_last_not_of("\\t ") + 1);
                strValue += strLine;
                prop.put(strKey, strValue);
                state = idle;
                lstValues.clear();
            }
        }
    }

    if (state == value && !lstValues.empty())
    {
        std::string strValue;
        std::vector<std::string>::iterator it;
        for (it = lstValues.begin(); it != lstValues.end(); ++it)
            strValue += (*it) + " ";
        strLine.erase(strLine.find_last_not_of("\\t ") + 1);
        strValue += strLine;

        prop.put(strKey, strValue);
    }

    return is;
}

}}
