/*
 * Inet4SockAddr.cpp
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net {

Inet4SockAddr::Inet4SockAddr() :
    m_strHost("localhost")
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
}

Inet4SockAddr::Inet4SockAddr(int iPort) :
    m_strHost("localhost")
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
    m_addr.sin_addr.s_addr = INADDR_ANY;
    m_addr.sin_port = htons((word)iPort);
}

Inet4SockAddr::Inet4SockAddr(std::string const& strHost, int iPort) :
    m_strHost(strHost)
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
    Resolver r;
    r.family(sock::af_ipv4).flags(sock::ai_all);
    r.resolve(strHost);
    Resolver::iterator it = r.begin();
    m_addr = *(sockaddr_in*)((*it).ai_addr);

    m_addr.sin_port = htons((word)iPort);
}

Inet4SockAddr::Inet4SockAddr(SockAddr const& addr)
{
    m_addr = addr.raw.in;
    m_strHost = getHostAddress();
}


Inet4SockAddr::~Inet4SockAddr()
{
}

std::string Inet4SockAddr::getHostName() const
{
    return m_strHost;
}

std::string Inet4SockAddr::getCanonicalHostName() const
{
    char szHost[NI_MAXHOST] = {0};
    ::getnameinfo(
            (sockaddr*)&m_addr,
            (size_t)sizeof(m_addr),
            szHost,
            (size_t)sizeof(szHost),
            NULL,
            0,
            0
            );
    return szHost;
}

std::string Inet4SockAddr::getHostAddress() const
{
    char sz[INET_ADDRSTRLEN] = {0};
    ::inet_ntop(
            sock::af_ipv4,
            &m_addr.sin_addr,
            sz,
            INET_ADDRSTRLEN
            );
    return sz;
}

void Inet4SockAddr::getAddress(std::vector<uint8>& lst) const
{
    uint8 b[4] = { 0 };
    ::memcpy(b, &m_addr.sin_addr, (size_t)sizeof(b));
    for (int i = 0; i < 4; ++i)
        lst.push_back(b[i]);
}

int Inet4SockAddr::getPort() const
{
    return ntohs(m_addr.sin_port);
}

sock::Family Inet4SockAddr::family() const
{
    return sock::af_ipv4;
}

Inet4SockAddr::operator SockAddr() const
{
    SockAddr addr;
    addr.raw.in = m_addr;
    addr.len = sizeof(m_addr);

    return addr;
}

std::string Inet4SockAddr::tostr() const
{
    std::ostringstream os;
    std::string str = getHostName();
    if (!str.empty())
        os << getHostName() << " ";
    os << "IPv4 (";
    str = getCanonicalHostName();
    if (!str.empty())
        os << str << " -> ";
    os << getHostAddress();
    os << ")";
    if (m_addr.sin_port)
        os << ":" << getPort();
    return os.str();
}

}
}
