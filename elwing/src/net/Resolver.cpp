/*
 * Resolver.cpp
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net {

Resolver::Result::Iterator::Iterator() :
    m_pNode(NULL)
{
}

Resolver::Result::Iterator::Iterator(addrinfo* pNode) :
    m_pNode(pNode)
{
}

Resolver::Result::Iterator::Iterator(Iterator const& that) :
    m_pNode(that.m_pNode)
{
}

Resolver::Result::Iterator& Resolver::Result::Iterator::operator =(
        Iterator const& that
        )
{
    m_pNode = that.m_pNode;
    return (*this);
}

bool Resolver::Result::Iterator::operator ==(Iterator const& that) const
{
    return m_pNode == that.m_pNode;
}

bool Resolver::Result::Iterator::operator !=(Iterator const& that) const
{
    return m_pNode != that.m_pNode;
}

addrinfo const& Resolver::Result::Iterator::operator *() const
{
    return *m_pNode;
}

addrinfo const* Resolver::Result::Iterator::operator ->()
{
    return m_pNode;
}

Resolver::Result::Iterator& Resolver::Result::Iterator::operator ++()
{
    m_pNode = m_pNode->ai_next;
    return *this;
}

Resolver::Result::Iterator Resolver::Result::Iterator::operator ++(int)
{
    Iterator it(m_pNode);
    m_pNode = m_pNode->ai_next;
    return it;
}

Resolver::Result::Result() :
    m_pHead(NULL),
    m_nSize(0)
{
}

Resolver::Result::Result(addrinfo* pHead) :
    m_pHead(pHead),
    m_nSize(0)
{
}

Resolver::Result::Result(Result const& that) :
    m_pHead(that.m_pHead),
    m_nSize(that.m_nSize)
{
}

Resolver::Result::~Result()
{
}

Resolver::Result& Resolver::Result::operator =(addrinfo* raw)
{
    m_pHead = raw;
    m_nSize = 0;
    return *this;
}

Resolver::Result::Iterator Resolver::Result::begin()
{
    return Iterator(m_pHead);
}

Resolver::Result::Iterator Resolver::Result::end()
{
    return Iterator();
}

size_t Resolver::Result::size()
{
    if (m_nSize > 0)
        return m_nSize;
    size_t nSize = 0;
    for (addrinfo* p = m_pHead; p != NULL; p = p->ai_next)
        ++nSize;
    m_nSize = nSize;
    return nSize;

}

bool Resolver::Result::empty() const
{
    return m_pHead == NULL;
}

void Resolver::Result::swap(Result& that)
{
    addrinfo* pHead = m_pHead;
    size_t nSize = m_nSize;
    m_pHead = that.m_pHead;
    m_nSize = that.m_nSize;
    that.m_pHead = pHead;
    that.m_nSize = nSize;
}

void Resolver::Result::reset()
{
    ::freeaddrinfo(m_pHead);
}

Resolver::Resolver() :
    m_result()
{
    memset(&m_hint, 0, (size_t)sizeof(m_hint));
}

Resolver::~Resolver()
{
    if (!m_result.empty())
        m_result.reset();
}

Resolver& Resolver::family(sock::Family f)
{
    m_hint.ai_family = f;
    return *this;
}

Resolver& Resolver::type(sock::Type t)
{
    m_hint.ai_socktype = t;
    return *this;
}

Resolver& Resolver::flags(sock::AddrInfo f)
{
    m_hint.ai_flags = f;
    return *this;
}

bool Resolver::resolve(
        std::string const& strHost,
        std::string const& strService
        )
{
    addrinfo* a = NULL;
    int iRslt = ::getaddrinfo(
            strHost.c_str(),
            strService.empty() ? NULL : strService.c_str(),
            &m_hint,
            &a
            );
    if (iRslt != 0)
        return false;
    if (!m_result.empty())
        m_result.reset();
    m_result = a;
    return true;
}

}
}
