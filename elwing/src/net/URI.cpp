
/*
 * URI.cpp
 *
 *  Created on : 2011/08/19  1:11
 *      Author : Bulent Kopuklu
 *      Contact: bulent.kopuklu@gmail.com
 */


#include "stdafx.h"

namespace elw { namespace net {

namespace uri {

static size_t scan(
    std::string::const_iterator itb,
    std::string::const_iterator ite,
    std::string const& strError,
    std::string const& strStop
    ) throw()
{
    size_t nRslt = std::string::npos;
    size_t nCrnt = 0;
    for (std::string::const_iterator it = itb; it != ite; ++it, ++nCrnt)
    {
        if (!strError.empty())
        {
            if (std::string::npos != strError.find((*it)))
                break;
        }

        if (std::string::npos != strStop.find((*it)))
        {
            nRslt = nCrnt;
            break;
        }
    }

    return nRslt;
}

}

URI::URI(void) throw() :
    m_wPort(0)
{
}

URI::URI(const std::string& strURI) throw():
    m_strURI(strURI),
    m_wPort(0)
{
    parse(strURI);
}

URI::~URI(void) throw()
{
}

std::string URI::tostr() const throw()
{
    return m_strURI;
}

URI& URI::operator =(const std::string& strURI) throw()
{
    clear();
    m_strURI = strURI;
    parse(strURI);
    return *this;
}

bool URI::parseAuthority(std::string& str) throw()
{ // [ userinfo "@" ] host [ ":" port ]

    size_t npos = uri::scan(str.begin(), str.end(), "", "]");
    if (npos != std::string::npos)
        return false;

    npos = uri::scan(str.begin(), str.end(), "/?#", "@");
    if (npos != std::string::npos)
    { // user info
        m_strUserInfo = str.substr(0, npos);
        str = str.substr(npos+1);
    }

    // host
    npos = uri::scan(str.begin(), str.end(), "/?#", ":");
    if (npos != std::string::npos)
    {
        m_strHost = str.substr(0, npos);
        m_wPort = (word) atoi(str.substr(npos+1).c_str());
    }
    else
    {
        m_strHost = str;
    }

    return true;
}

bool URI::parseHierarchical(std::string& str) throw()
{ // [//authority]<path>[?<query>]
    if ('/' == str.at(0) && '/' == str.at(1))
    {
        str = str.substr(2);
        size_t npos = uri::scan(str.begin(), str.end(), "", "/?#");
        if (npos != std::string::npos)
        {
            std::string str0 = str.substr(0, npos);
            parseAuthority(str0);
            str = str.substr(npos);
        }
    }

    size_t npos = uri::scan(str.begin(), str.end(), "", "?#");
    size_t nmax = npos == std::string::npos ? str.length() : npos;
    m_strPath = str.substr(0, nmax);
    str = str.substr(nmax);
    if (!str.empty())
    {
        npos = uri::scan(str.begin(), str.end(), "", "#");
        m_strQuery = str.substr(0, npos);
        str = str.substr(npos);
    }

    if (!str.empty())
    {
        m_strFragment = str;
        str = "";
    }

    return true;
}

bool URI::parse(const std::string& strURI) throw()
{ // [<scheme>:]<scheme-specific-part>[#<fragment>]
    std::string strTemp(strURI);
    size_t npos = uri::scan(strTemp.begin(), strTemp.end(), "/?#", ":");
    if (npos != std::string::npos)
    {
        m_strScheme = strTemp.substr(0, npos);
        strTemp = strTemp.substr(npos+1); // :
        if (strTemp.empty())
            return false;
    }

    parseHierarchical(strTemp);
//    if (!strTemp.empty())
//        std::cout << strTemp << std::endl;

    return true;
}

void URI::clear() throw()
{
    m_strURI.clear();
    m_strScheme.clear();
    m_strUserInfo.clear();
    m_strHost.clear();
    m_wPort = 0;
    m_strPath.clear();
    m_strQuery.clear();
    m_strFragment.clear();
}

}}
