/*
 * Socket.cpp
 *
 *  Created on: Oct 13, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net {

namespace sock {
    Handle create(
            Family f,
            Type t,
            Protocol p
            )
    {
        return socket(f, t, p);
    }
}

Socket::Socket(sock::Handle hSocket) :
    m_hSocket(hSocket),
    m_family(sock::Family(-1)),
    m_type(sock::Type(-1)),
    m_proto(sock::Protocol(-1))
{
}

Socket::Socket(Socket const& that) :
    m_hSocket(that.m_hSocket),
    m_family(that.m_family),
    m_type(that.m_type),
    m_proto(that.m_proto)
{
}

Socket::~Socket()
{
}

bool Socket::connect(SockAddr const& addr)
{
    return connect(S2SCADDR(addr));
}

bool Socket::connect(sockaddr const* pAddr, socklen_t nSize)
{
    if (!isValid())
        return false;
    return -1 != ::connect(m_hSocket, pAddr, nSize);
}

bool Socket::bind(SockAddr const& addr)
{
    return bind(S2SCADDR(addr));
}

bool Socket::bind(sockaddr const* pAddr, socklen_t nSize)
{
    return isValid() ? (-1 != ::bind(m_hSocket, pAddr, nSize)) : sock::invalid_handle;
}

bool Socket::listen(int iBacklog)
{
    return isValid() ? (-1 != ::listen(m_hSocket, iBacklog)) : sock::invalid_handle;
}

sock::Handle Socket::accept()
{
    SockAddr addr;
    return accept(addr);
}

sock::Handle Socket::accept(SockAddr& addr)
{
    return accept(S2SADDR(addr));
}

sock::Handle Socket::accept(sockaddr* pAddr, socklen_t* pnLen)
{
    return isValid() ? ::accept(m_hSocket, pAddr, pnLen) : sock::invalid_handle;
}

int Socket::send(lang::ByteBuffer const& buff, sock::Flag f)
{
    return send(
            buff.ptr(),
            buff.length(),
            f);
}

int Socket::send(void const* pvBuff, size_t nSize, sock::Flag f)
{
    int iRslt = -1;

    do
    {
        if (!isValid())
            return -1;
        iRslt = ::send(m_hSocket, pvBuff, nSize, f);
    }
    while (iRslt < 0 && sock::error() == sock::err_egain);
    return iRslt;
}

int Socket::sendto(lang::ByteBuffer const& buff, SockAddr const& addr, sock::Flag f)
{
    return sendto(
            buff.ptr(),
            buff.length(),
            addr,
            f
            );
}

int Socket::sendto(void const* pvBuff, size_t nBuffSize, SockAddr const& addr, sock::Flag f)
{
    return sendto(
            pvBuff,
            nBuffSize,
            S2SCADDR(addr),
            f
            );
}

int Socket::sendto(
        void const* pvBuff, size_t nBuffSize,
        sockaddr const* pAddr, socklen_t nLen,
        sock::Flag f
        )
{
    int iRslt = -1;
    do
    {
        if (!isValid())
            return -1;
        iRslt = ::sendto(m_hSocket, pvBuff, nBuffSize, f, pAddr, nLen);
    }
    while (iRslt < 0 && sock::error() == sock::err_egain);
    return iRslt;
}

int Socket::sendmsg(msghdr const* pMsg, sock::Flag f)
{
    return isValid() ? ::sendmsg(m_hSocket, pMsg, f) : -1;
}

int Socket::recv(lang::ByteBuffer& buff, sock::Flag f)
{
    int iRslt = recv(
            buff.ptr(),
            buff.capacity(),
            f
            );
    if (iRslt >= 0)
        buff.count(iRslt);
    return iRslt;
}

int Socket::recv(void* pvBuff, size_t nSize, sock::Flag f)
{
    return isValid() ? ::recv(m_hSocket, pvBuff, nSize, f) : -1;
}

int Socket::recvfrom(lang::ByteBuffer& buff, SockAddr& addr, sock::Flag f)
{
    int iRslt = recvfrom(
            buff.ptr(),
            buff.length(),
            addr,
            f
            );
    if (iRslt >= 0)
        buff.count(iRslt);
    return iRslt;

}

int Socket::recvfrom(void* pvBuff, size_t nBuffSize, SockAddr& addr, sock::Flag f)
{
    return recvfrom(
            pvBuff,
            nBuffSize,
            S2SADDR(addr),
            f
            );
}

int Socket::recvfrom(
        void* pvBuff, size_t nBuffSize,
        sockaddr* pAddr, socklen_t* pnLen,
        sock::Flag f
        )
{
    return isValid() ? ::recvfrom(m_hSocket, pvBuff, nBuffSize, f, pAddr, pnLen) : -1;
}

int Socket::recvmsg(msghdr* pMsg, sock::Flag f)
{
    return isValid() ? ::recvmsg(m_hSocket, pMsg, f) : -1;
}

bool Socket::shutdown(sock::Shutmode sm)
{
    return isValid() ? (-1 != ::shutdown(m_hSocket, sm)) : false;
}

bool Socket::close()
{
    if (!isValid())
        return true;
    bool fRslt = -1 != sock::close(m_hSocket);
    if (fRslt)
        m_hSocket = sock::invalid_handle;
    return fRslt;
}

bool Socket::getpeername(SockAddr& addr)
{
    return isValid() ? (-1 != ::getpeername(m_hSocket, S2SADDR(addr))) : false;
}

bool Socket::getsockname(SockAddr& addr) const
{
    return isValid() ? (-1 != ::getsockname(m_hSocket, S2SADDR(addr))) : false;
}

bool Socket::setopt(int iLevel, int iOption, void const* pvValue, socklen_t nSize)
{
    return isValid() ? (-1 != setsockopt(m_hSocket, iLevel, iOption, pvValue, nSize)) : false;
}

bool Socket::getopt(int iLevel, int iOption, void* pvValue, socklen_t* pnSize) const
{
    return isValid() ? (-1 != getsockopt(m_hSocket, iLevel, iOption, pvValue, pnSize)) : false;
}

bool Socket::ioctl(dword dwRequest, void* pvArgs)
{
    return isValid() ? (-1 != sock::ioctl(m_hSocket, dwRequest, pvArgs)) : false;
}

sock::Type Socket::type()
{
    if (m_type == -1)
    {
        int iVal = -1;
        socklen_t len = sizeof(iVal);
        if (getopt(SOL_SOCKET, SO_TYPE, &iVal, &len))
            m_type = sock::Type(iVal);
    }

    return m_type;
}

sock::Family Socket::family()
{
    if (m_family == -1)
    {
        SockAddr a;
        if (getsockname(a))
            m_family = a.family();
    }

    return m_family;
}

sock::Protocol Socket::protocol()
{
    if (m_proto == -1)
    {
        int iVal = -1;
        socklen_t len = sizeof(iVal);
        if (getopt(SOL_SOCKET, SO_PROTOCOL, &iVal, &len))
            m_proto = sock::Protocol(iVal);
    }

    return m_proto;
}

}}
