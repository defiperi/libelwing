/*
 * Inet6SockAddr.cpp
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net {

Inet6SockAddr::Inet6SockAddr()
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
}

Inet6SockAddr::Inet6SockAddr(int iPort)
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
    m_addr.sin6_addr= in6addr_any;
    m_addr.sin6_port = htons((word)iPort);
}

Inet6SockAddr::Inet6SockAddr(std::string const& strHost, int iPort) :
    m_strHost(strHost)
{
    memset(&m_addr, 0, (size_t)sizeof(m_addr));
    Resolver r;
    r.family(sock::af_ipv6).flags(sock::ai_all);
    r.resolve(strHost);
    Resolver::iterator it = r.begin();
    m_addr = *(sockaddr_in6*)((*it).ai_addr);

    m_addr.sin6_port = htons((word)iPort);
}

Inet6SockAddr::Inet6SockAddr(SockAddr const& addr)
{
    m_addr = addr.raw.in6;
}


Inet6SockAddr::~Inet6SockAddr()
{
}

std::string Inet6SockAddr::getHostName() const
{
    return m_strHost;
}

std::string Inet6SockAddr::getCanonicalHostName() const
{
    char szHost[NI_MAXHOST] = {0};
    ::getnameinfo(
            (sockaddr*)&m_addr,
            (size_t)sizeof(m_addr),
            szHost,
            (size_t)sizeof(szHost),
            NULL,
            0,
            0
            );
    return szHost;
}

std::string Inet6SockAddr::getHostAddress() const
{
    char sz[INET6_ADDRSTRLEN] = {0};
    ::inet_ntop(
            sock::af_ipv6,
            &m_addr.sin6_addr,
            sz,
            INET6_ADDRSTRLEN
            );
    return sz;
}

void Inet6SockAddr::getAddress(std::vector<uint8>& lst) const
{
    uint8 b[16] = { 0 };
    ::memcpy(b, &m_addr.sin6_addr.s6_addr, (size_t)sizeof(b));
    for (int i = 0; i < 4; ++i)
        lst.push_back(b[i]);
}

int Inet6SockAddr::getPort() const
{
    return ntohs(m_addr.sin6_port);
}

sock::Family Inet6SockAddr::family() const
{
    return sock::af_ipv6;
}

Inet6SockAddr::operator SockAddr() const
{
    SockAddr addr;
    addr.raw.in6 = m_addr;
    addr.len = sizeof(m_addr);

    return addr;
}

std::string Inet6SockAddr::tostr() const
{
    std::ostringstream os;
    std::string str = getHostName();
    if (!str.empty())
        os << getHostName() << " ";
    os << "IPv4 (";
    str = getCanonicalHostName();
    if (!str.empty())
        os << str << " -> ";
    os << getHostAddress();
    os << ")";
    if (m_addr.sin6_port)
        os << ":" << getPort();
    return os.str();
}

}
}
