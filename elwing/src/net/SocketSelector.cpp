/*
 * SocketSelector.cpp
 *
 *  Created on: Dec 23, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

namespace elw { namespace net {

#define MAX_OBSERVER  1024


SocketSelector::SocketSelector() throw() :
    m_state(stNONE)
{
}

SocketSelector::~SocketSelector() throw()
{
    if (state() == stRUNNING)
        stop();
}

SocketSelector::State SocketSelector::state() throw()
{
    return (State)(int)m_state;
}

void SocketSelector::state(State st) throw()
{
    m_state = st;
}

bool SocketSelector::isRunning() throw()
{
    return (state() == stRUNNING);
}

void SocketSelector::start()
{
    if (isRunning())
        throw lang::Exception("SocketSelector is already running");
    if (0 > ::pipe(m_pipes))
        throw lang::SystemErrorException("pipe");
    state(stRUNNING);
    threading::Thread::start();
}

void SocketSelector::stop() throw()
{
    if (!isRunning())
        return;
    state(stCLOSING);
    int iRslt = ::write(m_pipes[1], "1", 1);
    if (iRslt <= 0)
    {
        std::cerr << "SocketSelector::stop() > " << errno << " " << strerror(errno) << std::endl;

        m_lock.lock();
        m_lstObservers.clear();
        m_queue.clear();
        m_lock.unlock();

        return;
    }

    join();

    close(m_pipes[0]); close(m_pipes[1]);
    m_lstObservers.clear();
    m_queue.clear();

    state(stNONE);
}

void SocketSelector::attach(ObserverPtr ptrObserver)
{
    if (!isRunning())
        throw lang::Exception(MKSTR("Invalid state. state:" << state()));
    if (ptrObserver->sockfd() == net::sock::invalid_handle)
        throw lang::Exception("Invalid socket handle");
    if (m_lstObservers.size() == MAX_OBSERVER)
        throw lang::Exception("Capacity error");
    int iopt = ::fcntl(ptrObserver->sockfd(), F_GETFL);
    if (iopt < 0)
        throw lang::SystemErrorException("::fcntl(F_GETFL)");
    iopt |= O_NONBLOCK;
    if (0 > fcntl(ptrObserver->sockfd(), F_SETFL, iopt))
        throw lang::SystemErrorException("::fcntl(F_SETFL)");

    m_lock.lock();
    m_queue.push_back(ptrObserver);
    int iRslt = ::write(m_pipes[1], "1", 1);
    if (iRslt < 0)
    {
        throw lang::SystemErrorException("::write()");
        m_queue.erase(
                m_queue.end()-1
                );
    }
    m_lock.unlock();
}

void SocketSelector::run()
{
    pollfd files[MAX_OBSERVER];

    while (state() != stCLOSING)
    {
        memset(files, 0, sizeof(files));
        int iCnt = prepare(files);
        poll(files, iCnt);
    }
}

uint SocketSelector::prepare(pollfd lst[]) throw()
{
    lst[0].fd = m_pipes[0];
    lst[0].events = POLLIN;//|POLLPRI;
    lst[0].revents = 0;

    Observers lstObservers;
    uint i = 1;
    for (Observers::iterator it = m_lstObservers.begin(); it != m_lstObservers.end(); ++it)
    {
        int ifd = (*it)->sockfd();
        if (ifd != net::sock::invalid_handle)
        {
            lstObservers.push_back((*it));
            lst[i].fd = ifd;
            lst[i].events = POLLIN;//|POLLPRI;
            lst[i].revents = 0;

            ++i;
        }
    }

    {
        threading::ScopeGuard g(m_lock);
        for (Observers::iterator it = m_queue.begin(); it != m_queue.end(); ++it)
        {
            int ifd = (*it)->sockfd();
            if (ifd != net::sock::invalid_handle)
            {
                lstObservers.push_back((*it));
                lst[i].fd = ifd;
                lst[i].events = POLLIN;//|POLLPRI;
                lst[i].revents = 0;
                ++i;
            }
        }

        m_queue.clear();
    }

    m_lstObservers = lstObservers;

    return i;
}

void SocketSelector::poll(pollfd* pFiles, uint iCnt) throw()
{
    int iRemainingTime = 10000;

    int iRslt = 0;
    do
    {
        dword dw0 = threading::tick();
        iRslt = ::poll(pFiles, iCnt, iRemainingTime);
        if (iRslt < 0 && errno == EINTR)
        {
            threading::sleep(5);

            dword dw1 = threading::tick();
            int iWaited = (dw1 > dw0) ? (int)(dw1 - dw0) : (int)((MAX_ULONG - dw0) + dw1);
            if (iWaited < iRemainingTime)
                iRemainingTime -= iWaited;
            else
                iRemainingTime = 0;
        }
    }
    while (iRslt < 0 && errno == EINTR);

    if (iRslt > 0)
    {
        if ((pFiles[0].revents & POLLIN) == POLLIN)
        {
            char ch;
            if (0 > ::read(m_pipes[0], &ch, 1))
            {
            }
        }

        for (uint i = 1; i < iCnt; ++i)
        {
            if (pFiles[i].revents == 0)
                continue;
            if ((pFiles[i].revents & POLLIN) == POLLIN)
                onNotify(pFiles[i].fd);
            else
            {
                onError(pFiles[i].fd);
            }
        }
    }
    else if (iRslt < 0)
    {
        std::cerr << "error SocketSelector::poll. errno:" << errno << " errstr:" << strerror(errno) << std::endl;
    }
}

SocketSelector::ObserverPtr SocketSelector::observer(int ifd) const throw()
{
    for (Observers::const_iterator it = m_lstObservers.begin(); it != m_lstObservers.end(); ++it)
    {
        if ((*it)->sockfd() == ifd)
            return (*it);
    }

    return NULL;
}

void SocketSelector::onNotify(int ifd) throw()
{
    ObserverPtr ptr = observer(ifd);
    if (NULL == ptr)
        return;
    try
    {
        ptr->onNotify();
    }
    catch (std::exception& e)
    {
        std::cerr << "EXCEPTION: Observer::onNotify() -> " << e.what() << std::endl;
    }
}

void SocketSelector::onError(int ifd) throw()
{
    for (Observers::iterator it = m_lstObservers.begin(); it != m_lstObservers.end(); ++it)
    {
        if ((*it)->sockfd() == ifd)
        {
            m_lstObservers.erase(it);
            break;
        }
    }
}

}}

