/*
 * Variant.h
 *
 *  Created on: Dec 20, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingLangVariant_h__
#define ElwingLangVariant_h__

namespace elw { namespace lang {

class ELW_EXPORT Any
{
    class ELW_EXPORT IValue : public IUnknown
    {
    public:
        virtual IValue* clone() const = 0;
        virtual std::type_info const& type() const = 0;
    };

    template<class _Ty>
    struct ELW_EXPORT Value :
        public IValue
    {
        _Ty value;

        Value(_Ty val) :
            value(val)
        {
        }

        IValue* clone() const
        {
            return new Value<_Ty>(value);
        }

        std::type_info const& type() const
        {
            return typeid(_Ty);
        }
    };

    IValue* m_pVal;

public:
    template<class _Ty>
    _Ty const& get() const
    {
        return const_cast<Any&>(*this).get<_Ty>();
    }

    template<class _Ty>
    _Ty& get()
    {
        if (m_pVal == NULL)
            throw elw::lang::Exception("value not available");
        Value<_Ty>* pVal = dynamic_cast<Value<_Ty>*>(m_pVal);
        if (pVal == NULL)
            throw elw::lang::Exception(
                    MKSTR("invalid data type. src:" << m_pVal->type().name() << " dst:" << typeid(_Ty).name())
                    );
        return pVal->value;
    }

    template<class _Ty>
    void set(_Ty const& val)
    {
        if (m_pVal != NULL)
        {
            Value<_Ty>* pVal = dynamic_cast<Value<_Ty>*>(m_pVal);
            if (pVal == NULL)
            { // new type
                delete m_pVal;
                m_pVal = new Value<_Ty>(val);
            }
            else
            {
                pVal->value = val;
            }
        }
        else
        {
            m_pVal = new Value<_Ty>(val);
        }
    }

public:
    Any();

    Any(Any const& that);

    template<class _Ty>
    Any(_Ty const& val) :
        m_pVal(new Value<_Ty>(val))
    {
    }

    virtual ~Any();

    bool isAvailable() const;

    std::type_info const& type() const;

    template <class _Ty>
    bool instanceof() const
    {
        return m_pVal != NULL ?  (NULL != dynamic_cast<Value<_Ty>*>(m_pVal)) : false;
    }

//    template <>
//    bool instanceof<void*>() const
//    {
//        return m_pVal != NULL ?  (NULL != dynamic_cast<Value<void*>*>(m_pVal)) : true;
//    }

public:
    Any& operator =(Any const& that);
};

}}

#endif /* ElwingVariant_h__ */
