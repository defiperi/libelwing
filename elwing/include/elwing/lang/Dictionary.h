/*
 * Dictionary.h
 *
 *  Created on: Sep 12, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingLangDictionary_h__
#define ElwingLangDictionary_h__

#include "Any.h"

namespace elw { namespace lang {

class ELW_EXPORT AttributeError : public Exception
{
public:
    AttributeError(std::string const& key) :
        Exception(MKSTR("AttributeError attr:" << key))
    {
    }

    virtual ~AttributeError() throw()
    {
    }
};

class ELW_EXPORT Dictionary
{
    std::map<std::string, Any*> m_map;

public:
    typedef std::vector<std::string> Keys;

public:
    template <class _Ty>
    void put(std::string const& key, _Ty const& val)
    {
        remove(key);
        m_map[key] = new Any(val);
    }

    template <class _Ty>
    _Ty const& get(std::string const& key) const
    {
        return const_cast<Dictionary&>(*this).get<_Ty>(key);
    }

    template <class _Ty>
    _Ty& get(std::string const& key)
    {
        std::map<std::string, Any*>::const_iterator it = m_map.find(key);
        if (m_map.end() == it)
            throw AttributeError(key);
        return (*it).second->get<_Ty>();
    }

    template <class _Ty>
    _Ty const& get(std::string const& key, _Ty const& def) const
    {
        return const_cast<Dictionary&>(*this).get<_Ty>(key, def);
    }

    template <class _Ty>
    _Ty& get(std::string const& key, _Ty const& def)
    {
        std::map<std::string, Any*>::const_iterator it = m_map.find(key);
        if (m_map.end() == it)
            return const_cast<_Ty&>(def);
        return (*it).second->get<_Ty>();
    }

    Any const& getObj(
            std::string const& key
            ) const;

    Keys& keys(
            Keys& lst
            ) const throw();

    bool hasKey(
            std::string const& key
            ) const throw();

    void remove(
            std::string const& key
            ) throw();
public:
    Dictionary();
    Dictionary(Dictionary const& that);

    virtual ~Dictionary();
};

}}

#endif /* Dictionary_h__ */
