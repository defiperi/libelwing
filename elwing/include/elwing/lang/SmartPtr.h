/*
 * SmartPtr.h
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingLangSmartPtr_h__
#define ElwingLangSmartPtr_h__

namespace elw { namespace lang {


class ELW_EXPORT NullPointerException :
    public Exception
{
public:
    NullPointerException(const std::string& str = "") :
        Exception("NullPointerException<" + str + ">")
    {
    }
};

template <class _Ty>
class ELW_EXPORT SmartPtr
{
    _Ty* m_pPtr;
    threading::AtomicCounter* m_pCounter;
public:

    SmartPtr(void) :
        m_pPtr(NULL)
    {
        m_pCounter = new threading::AtomicCounter(1);
    }

    SmartPtr(_Ty const* pPtr, int iRefCnt = 1) :
        m_pPtr(const_cast<_Ty*>(pPtr))
    {
        m_pCounter = new threading::AtomicCounter(iRefCnt);
    }

    SmartPtr(SmartPtr const& rThat) :
        m_pPtr(rThat.m_pPtr),
        m_pCounter(rThat.m_pCounter)
    {
        ++*m_pCounter;
    }

    SmartPtr(_Ty const* pPtr, threading::AtomicCounter* pCounter) :
        m_pPtr(const_cast<_Ty*>(pPtr)),
        m_pCounter(pCounter)
    {
        ++*m_pCounter;
    }

    virtual ~SmartPtr(void)
    {
        if (NULL == m_pCounter)
            return;
        int iRefCnt = --(*m_pCounter);
        if (iRefCnt == 0)
        {
            delete m_pCounter;
            m_pCounter = NULL;
            if (NULL != m_pPtr)
            {
                delete m_pPtr;
                m_pPtr = NULL;
            }
        }
    }

    int count() const throw()
    {
        return *m_pCounter;
    }

    _Ty* raw() throw()
    {
        return m_pPtr;
    }

    SmartPtr& operator=(SmartPtr const& rRight) throw()
    {
        if (this == &rRight)
            return *this;
        SmartPtr<_Ty> tmp(rRight);
        tmp.swap(*this);
        return *this;
    }

    SmartPtr<_Ty>& operator=(_Ty const* pPtr) throw()
    {
        return (*this = SmartPtr<_Ty>(pPtr));
    }

    _Ty* operator->() const
    {
        if (NULL == m_pPtr)
            throw NullPointerException(typeid(_Ty).name());
        return m_pPtr;
    }

    _Ty& operator*() const
    {
        if (NULL == m_pPtr)
            throw NullPointerException(typeid(_Ty).name());
        return *m_pPtr;
    }

    bool operator==(SmartPtr const& rRight) const throw()
    {
        return m_pPtr == rRight.m_pPtr;
    }

    bool operator==(_Ty const* pRight) const throw()
    {
        return m_pPtr == pRight;
    }

    bool operator!=(SmartPtr const& rRight) const throw()
    {
        return m_pPtr != rRight.m_pPtr;
    }

    bool operator!=(_Ty const* pRight) const throw()
    {
        return m_pPtr != pRight;
    }

    template <class _TyNew>
    SmartPtr<_TyNew> cast_dynamic() throw()
    {
        if (NULL == m_pPtr)
            return NULL;
        _TyNew* p = dynamic_cast<_TyNew*>(m_pPtr);
        if (NULL == p)
            return NULL;
        return SmartPtr<_TyNew>(p, m_pCounter);
    }

    template <class _TyNew>
    SmartPtr<_TyNew> cast_static() throw()
    {
        if (NULL == m_pPtr)
            return NULL;
        return SmartPtr<_TyNew>(static_cast<_TyNew*>(m_pPtr), m_pCounter);
    }


protected:
    void swap(SmartPtr& rThat) throw()
    {
        std::swap(m_pCounter, rThat.m_pCounter);
        std::swap(m_pPtr, rThat.m_pPtr);
    }
};

}}

template <class _Ty>
inline ELW_EXPORT bool operator==(void* pLeft, elw::lang::SmartPtr<_Ty> const& ptrRight)
{
    return ptrRight == (_Ty*)pLeft;
}

template <class _Ty>
inline ELW_EXPORT bool operator!=(void* pLeft, elw::lang::SmartPtr<_Ty> const& ptrRight)
{
    return ptrRight != (_Ty*)pLeft;
}

#endif /* ElwingSmartPtr_h__ */
