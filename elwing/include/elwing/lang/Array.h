/*
 * Array.h
 *
 *  Created on: Aug 12, 2011
 *      Author: Bulent.Kopuklu
 */

#ifndef ElwingLangArray_h__
#define ElwingLangArray_h__

namespace elw { namespace lang {

template <class _TyElement>
class Array
{
    _TyElement *m_pData;
    uint m_uCapasity;
public:
    Array(uint uCapasity = 64) :
        m_pData(NULL),
        m_uCapasity(uCapasity)
    {
        m_pData = new _TyElement[uCapasity];
    }

    Array(Array const& that) :
        m_pData(NULL),
        m_uCapasity(that.m_uCapasity)
    {
        if (m_uCapasity > 0)
        {
            m_pData = new _TyElement[m_uCapasity];
            for (uint i = 0; i < m_uCapasity; ++i)
                m_pData[i] = that.m_pData[i];
        }
    }

    virtual ~Array()
    {
        if (m_pData != NULL)
            delete [] m_pData;
    }

    void fill(_TyElement const& v)
    {
        for (uint i = 0; i < m_uCapasity; ++i)
            m_pData[i] = v;
    }

    uint size() const throw()
    {
        return m_uCapasity;
    }

    _TyElement& at(uint i)
    {
        if (i > m_uCapasity-1)
            throw elw::lang::OutOfBoundsException();
        return m_pData[i];
    }

    _TyElement const& at(uint i) const
    {
        if (i > m_uCapasity-1)
            throw elw::lang::OutOfBoundsException();
        return m_pData[i];
    }

    _TyElement& operator[](uint i)
    {
        return at(i);
    }

    Array& operator =(Array const& that)
    {
        if (m_pData != NULL)
        {
            delete [] m_pData;
            m_pData = NULL;
        }

        m_uCapasity = that.size();

        if (m_uCapasity > 0)
        {
            m_pData = new _TyElement[m_uCapasity];
            for (uint i = 0; i < m_uCapasity; ++i)
                m_pData[i] = that.m_pData[i];
        }

        return *this;
    }
};

template <class _TyElement>
bool operator ==(Array<_TyElement> const& a0, Array<_TyElement> const& a1)
{
    if (a0.size() != a1.size())
        return false;
    for (uint i = 0; i < a0.size(); ++i)
    {
        if (a0[i] != a1[i])
            return false;
    }

    return true;
}

template <class _TyElement>
bool operator !=(Array<_TyElement> const& a0, Array<_TyElement> const& a1)
{
    return !(a0 == a1);
}


//template <class _TyElem>
//class ELW_EXPORT BasicArray
//{
//    typedef _TyElem* _TyPtr;
//    _TyPtr m_p;
//    size_t m_nLength;
//    size_t m_nMaxLenght;
//public:
//    BasicArray(size_t nInitialLength = 64) :
//        m_p(NULL),
//        m_nLength(0),
//        m_nMaxLenght(nInitialLength)
//    {
//        m_p = (_TyPtr) ::calloc(1, sizeOf());
//        if (NULL == m_p)
//            throw elw::lang::SystemErrorException("Memory allocation failure.");
//    }
//
//    BasicArray(const BasicArray& that) :
//        m_p(NULL),
//        m_nLength(that.m_nLength),
//        m_nMaxLenght(that.m_nMaxLenght)
//    {
//        m_p = (_TyPtr) ::calloc(1, sizeOf());
//        if (NULL == m_p)
//            throw elw::lang::SystemErrorException("Memory allocation failure.");
//        ::memcpy(m_p, that.m_p, that.length());
//    }
//
////    BasicArray(_TyElem* pData, size_t nSize) :
////        m_p(pData),
////        m_nLength(nSize),
////        m_nMaxLenght(nSize),
////        m_fMyPtr(false)
////    {
//////        m_p = (_TyPtr) ::malloc(that.sizeOf());
//////        ::memcpy(m_p, that.m_p, that.sizeOf());
////    }
//
//    ~BasicArray()
//    {
//        ::free(m_p);
//    }
//
//    _TyPtr ptr() const
//    {
//        return m_p;
//    }
//
//    operator _TyPtr()
//    {
//        return m_p;
//    }
//
//    size_t sizeOf() const
//    {
//        return m_nMaxLenght * sizeof(_TyElem);
//    }
//
//    size_t length() const
//    {
//        return m_nLength;
//    }
//
//    void count(size_t nLength)
//    {
//        m_nLength = nLength;
//    }
//
//    void reset()
//    {
//        m_nLength = 0;
//    }
//
//    bool resize(size_t nNewLength)
//    {
//        if (nNewLength > m_nMaxLenght)
//        {
//            _TyPtr p = (_TyPtr)::realloc(m_p, nNewLength*sizeof(_TyElem));
//            if (NULL == p)
//                return false;
//            m_p = p;
//            m_nMaxLenght = nNewLength;
//        }
//        else if (nNewLength < m_nMaxLenght)
//        {
//            m_nMaxLenght = nNewLength;
//            if (m_nLength > nNewLength)
//                m_nLength = nNewLength;
//        }
//        else
//        {
//        }
//
//        return true;
//    }
//    _TyElem const& at(size_t i) const
//    {
//        if (i > sizeOf())
//            throw lang::OutOfBoundsException();
//        return (*this)[i];
//    }
//
//    _TyElem& at(size_t i)
//    {
//        if (i > sizeOf())
//            throw lang::OutOfBoundsException();
//        return (*this)[i];
//    }
//
//    _TyElem const& operator[](size_t i) const
//    {
//        return *(m_p+(i*sizeof(_TyElem)));
//    }
//
//    _TyElem& operator[](size_t i)
//    {
//        return *(m_p+(i*sizeof(_TyElem)));
//    }
//
//    void operator =(BasicArray const& that)
//    {
//        resize(that.sizeOf());
//        ::memcpy(m_p, that.m_p, that.length());
//        count(that.length());
//    }
//
//    void operator +=(BasicArray const& that)
//    {
//        resize(length() + that.sizeOf());
//        ::memcpy(m_p + length(), that.m_p, that.length());
//        count(length() + that.length());
//    }
//
//};
//
//template <class _TyCh>
//inline ELW_EXPORT std::basic_ostream<_TyCh>& operator <<(std::basic_ostream<_TyCh>& os, const BasicArray<_TyCh>& rBuff)
//{
//    if (os.good() && rBuff.length() > 0)
//        os.write(rBuff.ptr(), rBuff.length());
//    return os;
//}
//
//template <class _TyCh>
//inline ELW_EXPORT std::basic_istream<_TyCh>& operator >>(std::basic_istream<_TyCh>& is, BasicArray<_TyCh>& rBuff)
//{
//    if (is.good() && rBuff.sizeOf() > 0)
//    {
//        rBuff.reset();
//        is.read(rBuff.ptr(), rBuff.sizeOf());
//        rBuff.count(is.gcount());
//    }
//
//    return is;
//}
//
//typedef BasicArray<char>        CharArray;
//typedef BasicArray<short>       ShortArray;
//typedef BasicArray<int>         IntegerArray;
//typedef BasicArray<long>        LongArray;
//
//typedef BasicArray<char>        Buffer;

}}

#endif /* ElwingArray_h__ */
