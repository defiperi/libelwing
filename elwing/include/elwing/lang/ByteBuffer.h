/*
 * ByteBuffer.h
 *
 *  Created on: Jan 13, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ByteBuffer_h__
#define ByteBuffer_h__

namespace elw { namespace lang {

class ByteBuffer
{
//    bool m_fOwner;
    uint8* m_pvBuff;
    size_t m_nCapacity;
    size_t m_nLength;

public:
    ByteBuffer(size_t nCapacity = 64);
    ByteBuffer(ByteBuffer const& that);
    ByteBuffer(void const* pv, size_t len);

    void copy(void const* pv, size_t len);
    void append(void const* pv, size_t len);

    uint8* ptr() throw();
    uint8 const* ptr() const throw();

    size_t capacity() const throw();
    size_t length() const throw();
    void count(size_t n);

    void clear() throw();

    ssize_t find(ByteBuffer const& needle, size_t b = 0, ssize_t e = -1) const throw();

public:
    ByteBuffer& operator=(ByteBuffer const& that);
    ByteBuffer& operator+=(ByteBuffer const& that);

public:
    void resize(size_t nNewCapacity);

public:
    virtual ~ByteBuffer();
};

bool operator ==(ByteBuffer const& b1, ByteBuffer const& b2);
bool operator !=(ByteBuffer const& b1, ByteBuffer const& b2);

}}

#define elw_buffer_from_str(s) ((uint8*)(s)), strlen(s)

#endif /* ByteBuffer_h__ */
