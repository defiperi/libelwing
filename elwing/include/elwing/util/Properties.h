/*
 * Property.h
 *
 *  Created on: Mar 6, 2013
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingUtilProperties_h__
#define ElwingUtilProperties_h__

namespace elw { namespace util {

class ELW_EXPORT Properties
{
public:
    typedef std::map<std::string, std::string> Attributes;
    typedef std::vector<std::string> Keys;
protected:
    Attributes m_map;
public:
    Properties();
    Properties(Properties const& that);

    virtual ~Properties();

public:
    void put(
            std::string const& strKey,
            std::string const& strValue
            );

    std::string const& get(
            std::string const& strKey
            ) const;

    std::string const& get(
            std::string const& strKey,
            std::string const& strDefault
            ) const;

    template <class _Ty>
    _Ty get(
            std::string const& strKey
            ) const
    {
        std::stringstream strm(get(strKey));

        _Ty t;
        if ((strm >> t).fail())
            throw elw::lang::Exception(MKSTR("Bad Conversion. key:[" << strKey << "]"));
        return t;
    }

    template <class _Ty>
    _Ty get(
            std::string const& strKey,
            _Ty const& def
            ) const
    {
        std::stringstream strm(get(strKey));

        _Ty t;
        if ((strm >> t).fail())
            return def;
        return t;
    }

    bool hasKey(std::string const& strKey) const;

public:
    Keys& keys(
            Keys& keys
            ) const;

/*
 * virtual int save(std::ostream& os);
 */

};

ELW_EXPORT std::ifstream& operator >>(std::ifstream& is, Properties& prop);
ELW_EXPORT std::ofstream& operator <<(std::ofstream& is, Properties const& prop);

}}


#endif /* ElwingUtilProperty_h__ */
