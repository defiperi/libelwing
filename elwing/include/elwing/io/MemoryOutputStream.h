/*
 * MemoryOutputStream.h
 *
 *  Created on: Dec 20, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIOMemoryOutputStream_h__
#define ElwingIOMemoryOutputStream_h__

namespace elw { namespace io {

class MemoryOutputStream :
    public std::ostream
{
public:
    MemoryOutputStream(size_t nBufferCapacity = 64);
    virtual ~MemoryOutputStream();

    lang::SmartPtr<lang::ByteBuffer> buffer();

    void clear();
};

}}

#endif /* ElwingMemoryOutputStream_h__ */
