/*
 * OutputStream.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIOOutputStream_h__
#define ElwingIOOutputStream_h__

namespace elw { namespace io {

class ELW_EXPORT OutputStream :
    public std::ostream
{
    lang::SmartPtr<IOutputChannel> m_ptrWriter;
public:
    OutputStream(lang::SmartPtr<IOutputChannel> ptrWriter);
    virtual ~OutputStream();

    bool isOpened();
    void close();
};

}}

#endif /* ElwingOutputStream_h__ */
