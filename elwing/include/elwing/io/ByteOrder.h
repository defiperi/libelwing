/*
 * ByteOrder.h
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIOByteOrder_h__
#define ElwingIOByteOrder_h__

namespace elw { namespace io {

namespace ByteOrder {

enum {
    big_endian,
    network = big_endian,
    little_endian
};

int native();

uint16 flip(uint16 u16) throw();
int16  flip(int16 i16)  throw();
uint32 flip(uint32 u32) throw();
int32  flip(int32 i32)  throw();
uint64 flip(uint64 u64) throw();
int64  flip(int64 i64)  throw();
float  flip(float fl)   throw();
double flip(double db)  throw();

}

class DataReadWrite
{
    bool m_fFlipRequired;
public:
    DataReadWrite(int iByteOrder) :
        m_fFlipRequired(false)
    {
        if (iByteOrder < ByteOrder::big_endian || iByteOrder > ByteOrder::little_endian)
            elw_throw_exception(elw::lang::Exception("Invalid parameter. <iByteOrder>"));
        m_fFlipRequired = ByteOrder::native() != iByteOrder;
    }

    bool isFlipRequired() const
    {
        return m_fFlipRequired;
    }
};

}}

#endif /* ByteOrder_h__ */
