/*
 * DataReader.h
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIODataReader_h__
#define ElwingIODataReader_h__

namespace elw { namespace io {


class Reader : public DataReadWrite
{
    std::istream& m_is;
public:
    Reader(std::istream& is, int iByteOrder = ByteOrder::native());

public:
    bool eof();
public:
    Reader& operator >>(bool& val);
    Reader& operator >>(char& val);
    Reader& operator >>(unsigned char& val);
    Reader& operator >>(short& val);
    Reader& operator >>(unsigned short& val);
    Reader& operator >>(int& val);
    Reader& operator >>(unsigned int& val);
    Reader& operator >>(long& val);
    Reader& operator >>(unsigned long& val);
    Reader& operator >>(float& val);
    Reader& operator >>(double& val);

    Reader& operator >> (lang::ByteBuffer& val);

public:
    template <class _Ty>
    _Ty read()
    {
        _Ty t;
        read(&t, sizeof(t));
        return (isFlipRequired() && sizeof(t) > 1) ? ByteOrder::flip(t) : t;
    }

    std::istream::int_type peek()
    {
        return m_is.peek();
    }

    Reader& read(void* pv, size_t l);
};


}}

#endif /* DataReader_h__ */
