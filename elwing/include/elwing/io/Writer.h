/*
 * DataWriter.h
 *
 *  Created on: Jan 15, 2015
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingIODataWriter_h__
#define ElwingIODataWriter_h__

namespace elw { namespace io {


class Writer : public DataReadWrite
{
    std::ostream& m_os;
public:
    Writer(std::ostream& os, int iByteOrder = ByteOrder::native());

    Writer& operator <<(bool val);
    Writer& operator <<(char val);
    Writer& operator <<(unsigned char val);
    Writer& operator <<(short val);
    Writer& operator <<(unsigned short val);
    Writer& operator <<(int val);
    Writer& operator <<(unsigned int val);
    Writer& operator <<(long val);
    Writer& operator <<(unsigned long val);
    Writer& operator <<(float val);
    Writer& operator <<(double val);
    Writer& operator <<(std::string const& val);

    Writer& operator << (lang::ByteBuffer const& val);

    Writer& write(void * pv, size_t l);
};


}}

#endif /* DataWriter_h__ */
