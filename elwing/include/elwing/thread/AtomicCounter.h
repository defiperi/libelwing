/*
 * AtomicCounter.h
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingAtomicCounter_h__
#define ElwingThreadingAtomicCounter_h__

namespace elw { namespace threading {

class ELW_EXPORT AtomicCounter
{
    long m_lValue;
public:
    AtomicCounter(long lFirstValue = 0);
//    AtomicCounter(AtomicCounter const& that);

public:
    long operator++() throw();
    long operator++(int) throw();

    long operator--() throw();
    long operator--(int) throw();

    long operator =(long) throw();
//    long operator = (AtomicCounter const& that) throw();

    operator long() throw();
};

}}

#endif /* ElwingAtomicCounter_h__ */
