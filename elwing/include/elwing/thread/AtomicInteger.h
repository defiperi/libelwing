/*
 * AtomicInteger.h
 *
 *  Created on: Oct 6, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingElwingAtomicInteger_h__
#define ElwingThreadingElwingAtomicInteger_h__

namespace elw { namespace threading {

class ELW_EXPORT AtomicInteger
{
    IAtomicIntegerPolicy* m_pImpl;
public:
    AtomicInteger(long iInitialValue = 0);
    AtomicInteger(AtomicInteger const& that);

    virtual ~AtomicInteger() throw();

//    int operator +(int iVal) throw();
//    int operator -(int iVal) throw();
//    int operator =(int iVal) throw();

    long operator +(long iVal) throw();
    long operator -(long iVal) throw();
    long operator =(long iVal) throw();

    operator long() throw();
};

}}

#endif /* ElwingAtomicInteger_h__ */
