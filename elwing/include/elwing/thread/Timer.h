/*
 * Timer.h
 *
 *  Created on: Feb 13, 2013
 *      Author: bulentk
 *
 */

#ifndef ElwingThreadingTimer_h__
#define ElwingThreadingTimer_h__

namespace elw { namespace threading {

class ELW_EXPORT Timer
{
public:
    class ELW_EXPORT Task :
        public lang::IUnknown
    {
        Timer* m_pTimer;
    public:
        virtual void onExpire();

        void cancel() throw();

    public:
        Task();
        virtual ~Task();

    protected:
        void setTimer(Timer* pTimer);
        friend class Timer;
    };

public:
    Timer(dword dwResolution = 10);

    virtual ~Timer() throw();

    void start(
            Task& task,
            dword dwInterval
            );

    void cancel() throw();
};

}
}

#endif /* Timer_h__ */
