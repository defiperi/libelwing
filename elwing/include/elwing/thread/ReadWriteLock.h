/*
 * RWLock.h
 *
 *  Created on: Dec 16, 2014
 *      Author: bulentk
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingRWLock_h__
#define ElwingRWLock_h__

namespace elw { namespace threading {

class ELW_EXPORT ReadWriteLock
{
    FastMutex m_lock;
    Condition m_cond;
    unsigned m_uLockCnt;
    unsigned m_uPendingWriters;
    unsigned m_uPendingReaders;
    dword m_dwWriterThread;
public:
    void rlock();
    void wlock();

    void unlock();

    bool tryrlock();
    bool trywlock();

protected:
    bool isReadable(dword dwtid);
    bool isWritable(dword dwtid);

    void notify();
public:
    ReadWriteLock();
    virtual ~ReadWriteLock();
};

}}

#endif /* RWLock_h__ */
