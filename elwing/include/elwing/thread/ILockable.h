/*
 * ILockable.h
 *
 *  Created on: Oct 7, 2011
 *      Author: altar
 */

#ifndef ElwingThreadingILockable_h__
#define ElwingThreadingILockable_h__

namespace elw { namespace threading {

class ELW_EXPORT ILockable :
    public lang::IUnknown
{
public:
    virtual void lock() = 0;
    virtual void unlock() = 0;
    virtual bool trylock() = 0;
};

}}

#endif /* ILOCKABLE_H_ */
