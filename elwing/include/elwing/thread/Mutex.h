/*
 * Mutex.h
 *
 *  Created on: Sep 29, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingMutex_h__
#define ElwingThreadingMutex_h__

namespace elw { namespace threading {

class ELW_EXPORT Mutex :
    public ILockable
{
protected:
    IMutexPolicy* m_pImpl;
    friend class Condition;

public:
    void lock();
    void unlock();
    bool trylock();

protected:
    Mutex(mutex::Type t);
public:
    virtual ~Mutex();
};

class ELW_EXPORT FastMutex :
    public Mutex
{
public:
    FastMutex();
};

class ELW_EXPORT RecursiveMutex :
    public Mutex
{
public:
    RecursiveMutex();
};

class ELW_EXPORT TimedMutex :
    public Mutex
{
public:
    bool trylock(dword dwmilis);
public:
    TimedMutex();
};

}}

#endif /* ElwingMutex_h__ */
