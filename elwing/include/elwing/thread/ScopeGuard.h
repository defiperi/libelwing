/*
 * ScopeLock.h
 *
 *  Created on: Oct 7, 2011
 *      Author: altar
 */

#ifndef ElwingThreadingScopeGuard_h__
#define ElwingThreadingScopeGuard_h__

namespace elw { namespace threading {

class ELW_EXPORT ScopeGuard
{
    ILockable& m_rLock;
public:
    ScopeGuard(ILockable& rLock) :
        m_rLock(rLock)
    {
        m_rLock.lock();
    }

    virtual ~ScopeGuard()
    {
        m_rLock.unlock();
    }
};

class ELW_EXPORT ScopeReadGuard
{
    ReadWriteLock& m_rLock;
public:
    ScopeReadGuard(ReadWriteLock& rLock) :
        m_rLock(rLock)
    {
        m_rLock.rlock();
    }

    virtual ~ScopeReadGuard()
    {
        m_rLock.unlock();
    }
};

class ELW_EXPORT ScopeWriteGuard
{
    ReadWriteLock& m_rLock;
public:
    ScopeWriteGuard(ReadWriteLock& rLock) :
        m_rLock(rLock)
    {
        m_rLock.wlock();
    }

    virtual ~ScopeWriteGuard()
    {
        m_rLock.unlock();
    }
};

}}

#endif /* SCOPELOCK_H_ */
