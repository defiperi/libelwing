/*
 * ThreadPool.h
 *
 *  Created on: Oct 20, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingThreadingThreadPool_h__
#define ElwingThreadingThreadPool_h__

namespace elw { namespace threading {


template <
    class _TyKey,
    class _TyMessage
    >
class ELW_EXPORT ThreadPool
{
public:
    class ELW_EXPORT IConsumer :
        public lang::IUnknown
    {
    public:
        virtual void onMessage(_TyMessage msg) = 0;
    };

protected:
    class Message
    {
        _TyMessage m_msg;
        IConsumer& m_rConsumer;
    public:
        Message(_TyMessage msg, IConsumer& rConsumer) :
            m_msg(msg),
            m_rConsumer(rConsumer)
        {
        }

        _TyMessage getUserMessage() const
        {
            return m_msg;
        }

        IConsumer& getConsumer() const
        {
            return m_rConsumer;
        }
    };

    class Dispatcher :
        public Thread
    {
        ThreadQueue<Message*> m_queue;
    public:
        void run()
        {
            Message* p = NULL;
            while ((p = m_queue.pop()) != NULL)
            {
                p->getConsumer().onMessage(p->getUserMessage());
                delete p;
            }
        }

        void post(Message* pMsg)
        {
            m_queue.push(pMsg);
        }
    };

    class Pair
    {
        IConsumer& m_rConsumer;
        Dispatcher& m_rDispatcher;
    public:
        Pair(IConsumer& rConsumer, Dispatcher& rDispatcher) :
            m_rConsumer(rConsumer),
            m_rDispatcher(rDispatcher)
        {
        }

        virtual ~Pair()
        {
        }

        IConsumer& getConsumer() const
        {
            return m_rConsumer;
        }

        Dispatcher& getDispatcher() const
        {
            return m_rDispatcher;
        }
    };

    typedef std::list<Dispatcher*> ThreadList;
    typedef typename ThreadList::iterator ThreadIterator;
    typedef std::map<_TyKey, lang::SmartPtr<Pair> > PairMap;
    typedef typename PairMap::iterator PairIterator;

    ThreadList m_lstThread;
    ThreadIterator m_itCrnt;
    PairMap m_mapPair;
    ReadWriteLock m_lock;
    bool m_fRunning;

public:
    void start(int iThreadCount)
    {
        if (m_lstThread.size() > 0)
            return;
        iThreadCount = std::min<int>(iThreadCount, 64);
        iThreadCount = std::max<int>(iThreadCount, 1);
        for (int i = 0; i < iThreadCount; ++i)
        {
            Dispatcher* p = new Dispatcher();
            try
            {
                p->start();
            }
            catch (std::exception const& e)
            {
                killAll();
                delete p;
                throw;
            }

            m_lstThread.push_back(p);
        }

        m_itCrnt = m_lstThread.begin();
        m_fRunning = true;
    }

    void killAll()
    {
        m_lock.wlock();
        m_fRunning = false;
//        for (PairIterator it = m_mapPair.begin(); it != m_mapPair.end(); ++it)
//            (*it).second.release();
        m_mapPair.clear();
        m_lock.unlock();

        for (ThreadIterator it = m_lstThread.begin(); it != m_lstThread.end(); ++it)
        {
            (*it)->post(NULL);
            (*it)->join();
            delete (*it);
        }

        m_lstThread.clear();
    }

    bool addConsumer(_TyKey key, IConsumer& rConsumer)
    {
        ScopeWriteGuard g(m_lock);
        if (!m_fRunning)
            return false;
        PairIterator it = m_mapPair.find(key);
        if (it != m_mapPair.end())
            return false;
        m_mapPair[key] = lang::SmartPtr<Pair>(new Pair(rConsumer, getDispatcher()));
        return true;
    }

    void removeConsumer(_TyKey key)
    {
        ScopeWriteGuard g(m_lock);
//        PairIterator it = m_mapPair.find(key);
//        if (it == m_mapPair.end())
//            return;
//        (*it).second.release();
        m_mapPair.erase(key);
    }

    bool post(_TyKey key, _TyMessage msg)
    {
        lang::SmartPtr<Pair> ptrPair = getPair(key);
        if (ptrPair == NULL)
            return false;
        ptrPair->getDispatcher().post(
                new Message(msg, ptrPair->getConsumer())
                );
        return true;
    }

public:
    ThreadPool() :
        m_fRunning(false)
    {
    }

protected:
    Dispatcher& getDispatcher()
    {
        if (++m_itCrnt == m_lstThread.end())
            m_itCrnt = m_lstThread.begin();
        return *(*m_itCrnt);
    }

    lang::SmartPtr<Pair> getPair(_TyKey key)
    {
        ScopeReadGuard g(m_lock);
        if (!m_fRunning)
            return NULL;
        PairIterator it = m_mapPair.find(key);
        if (it == m_mapPair.end())
        {
            std::cout << "ERROR -> " << "key:" << key << " table size:" << m_mapPair.size() << " this:" << (void*) this<<  std::endl;
            for (it = m_mapPair.begin(); it != m_mapPair.end(); ++it)
            {
                std::cout << "ERROR -> " << "key:" << (*it).first << std::endl;
            }

            return NULL;
        }
        return (*it).second;
    }
};

}}

#endif /* ElwingThreadPool_h__ */
