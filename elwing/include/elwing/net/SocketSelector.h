/*
 * SocketSelector.h
 *
 *  Created on: Dec 23, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingNetSocketSelector_h__
#define ElwingNetSocketSelector_h__

namespace elw { namespace net {

class ELW_EXPORT SocketSelector :
    protected threading::Thread
{
    enum State
    {
        stNONE,
        stRUNNING,
        stCLOSING
    };

    typedef lang::SmartPtr<ISocketObserver> ObserverPtr;
    typedef std::vector<ObserverPtr> Observers;

//    threading::AtomicInteger m_state;
    int m_state;

    threading::RecursiveMutex m_lock;
    Observers m_queue;
    Observers m_lstObservers;
    int m_pipes[2];

public:
    void start();
    void stop() throw();
public:
    void attach(
            ObserverPtr ptr
            );

protected:
    State state() throw();
    void state(
            State st
            ) throw();

    bool isRunning() throw();

    ObserverPtr observer(
            int ifd
            ) const throw();

protected:
    void onNotify(
            int ifd
            ) throw();
    void onError(
            int ifd
            ) throw();

protected:
    uint prepare(pollfd files[]) throw();
    void poll(pollfd* files, uint iCnt) throw();

public:
    SocketSelector() throw();
    virtual ~SocketSelector() throw();

protected:
    void run();
};


}
}

#endif /* ElwingSocketSelector_h__ */
