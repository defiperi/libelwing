

/*
 * URI.h
 *
 *  Created on : 2011/08/19  1:11
 *      Author : Bulent Kopuklu
 *      Contact: bulent.kopuklu@gmail.com
 */


#ifndef ElwingNetURI_h__
#define ElwingNetURI_h__

namespace elw { namespace net {

class ELW_EXPORT URI
{
    std::string m_strURI;
    std::string m_strScheme;
    std::string m_strUserInfo;
    std::string m_strHost;
    word        m_wPort;
    std::string m_strPath;
    std::string m_strQuery;
    std::string m_strFragment;

public:
    URI(void) throw();
    URI(std::string const& strURI) throw();

    URI& operator =(std::string const& strURI) throw();

    virtual ~URI(void) throw();

public:
    std::string const& scheme() const throw()
    {
        return m_strScheme;
    }

    std::string const& userInfo() const throw()
    {
        return m_strUserInfo;
    }

    std::string const& host() const throw()
    {
        return m_strHost;
    }

    std::string const& path() const throw()
    {
        return m_strPath;
    }

    std::string const& query() const throw()
    {
        return m_strQuery;
    }

    std::string const& fragment() const throw()
    {
        return m_strFragment;
    }

    word port() const throw()
    {
        return m_wPort;
    }

    std::string tostr() const throw();

protected:
    bool parse(std::string const& strURI) throw();
    bool parseHierarchical(std::string& str) throw();
    bool parseAuthority(std::string& str) throw();

    void clear() throw();
};

}}

#endif // ElwingURI_h__
