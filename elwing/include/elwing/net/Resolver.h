/*
 * Resolver.h
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingNetResolver_h__
#define ElwingNetResolver_h__

namespace elw { namespace net {

class ELW_EXPORT Resolver
{
public:
    class ELW_EXPORT Result
    {
    public:
        class ELW_EXPORT Iterator
        {
        public:
            Iterator();
            Iterator(addrinfo* pNode);
            Iterator(Iterator const& that);

        public:
            Iterator& operator =(Iterator const& that);
            bool operator ==(Iterator const& that) const;
            bool operator !=(Iterator const& that) const;

        public:
            addrinfo const& operator *() const;
            addrinfo const* operator ->();

        public:
            Iterator& operator ++();
            Iterator operator++ (int);

        private:
            addrinfo* m_pNode;
        };

    public:
        Result();
        Result(addrinfo* pHead);
        Result(Result const& that);

        ~Result();

    public:
        Result& operator =(addrinfo* raw);

    public:
        Iterator begin();
        Iterator end();
        size_t size();
        bool empty() const;
        void swap(Result& that);

    public:
        void reset();

    private:
        addrinfo* m_pHead;
        size_t m_nSize;
    };

public:
    typedef Result::Iterator iterator;

    Resolver();
    virtual ~Resolver();

public:
    Resolver& family(sock::Family f);
    Resolver& type(sock::Type t);
    Resolver& flags(sock::AddrInfo f);

public:
    bool resolve(
            std::string const& strHost,
            std::string const& strService = ""
            );

    iterator begin()
    {
        return m_result.begin();
    }

    iterator end()
    {
        return m_result.end();
    }

private:
    Result m_result;
    addrinfo m_hint;
};

}
}

#endif /* Resolver_h__ */
