/*
 * SockAddr.h
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingNetSockAddr_h__
#define ElwingNetSockAddr_h__

namespace elw { namespace net {

struct ELW_EXPORT SockAddr
{
    union sock_addr
    {
        sockaddr_in in;
        sockaddr_in6 in6;
        sockaddr_un un;
        sockaddr_can can;
        sockaddr_nl nl;
    } raw;
    socklen_t len;

    SockAddr() :
        raw(),
        len(sizeof(raw))
    {
        memset(&raw, 0, (size_t)sizeof(raw));
    }

    sock::Family family() const
    {
        return sock::Family(
                ((sockaddr*)&raw)->sa_family
                );
    }

    std::string tostr() const;
};

class ELW_EXPORT ISocketAddress :
    public lang::IUnknown
{
public:
    virtual operator SockAddr() const = 0;
};

}
}

#define RAWCADDR(x) ((sockaddr const*)(&((elw::net::SockAddr&)(x)).raw))
#define RAWCLEN(x)  (((elw::net::SockAddr&)(x)).len)

#define RAWADDR(x)  ((sockaddr*)(&((elw::net::SockAddr&)(x)).raw))
#define RAWLEN(x)   (&((elw::net::SockAddr&)(x)).len)

#define S2SADDR(x)  RAWADDR(x), RAWLEN(x)
#define S2SCADDR(x) RAWCADDR(x), RAWCLEN(x)

#endif /* ElwingSockAddr_h__ */
