/*
 * Inet4SockAddr.h
 *
 *  Created on: Aug 24, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingNetInet4SockAddr_h__
#define ElwingNetInet4SockAddr_h__

namespace elw { namespace net {

class ELW_EXPORT Inet4SockAddr :
    public ISocketAddress
{
    std::string m_strHost;
    sockaddr_in m_addr;
public:
    Inet4SockAddr();
    Inet4SockAddr(int iPort);
    Inet4SockAddr(std::string const& strHost, int iPort = 0);
    Inet4SockAddr(SockAddr const& addr);
    virtual ~Inet4SockAddr();

public:
    std::string getHostName() const;
    std::string getCanonicalHostName() const;
    std::string getHostAddress() const;
    void getAddress(std::vector<uint8>& lst) const;
    int getPort() const;

public:
    sock::Family family() const;
    std::string tostr() const;
public:
    operator SockAddr() const;
};


}}

#endif /* ElwingInet4SockAddr_h__ */
