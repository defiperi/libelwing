/*
 * Socket.h
 *
 *  Created on: Oct 13, 2011
 *      Author: Bulent.Kopuklu
 *
 *  Copyright (C) 2010 by Bulent Kopuklu (bulent.kopuklu@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ElwingNetSocket_h__
#define ElwingNetSocket_h__

namespace elw { namespace net {

namespace sock {
    ELW_EXPORT  Handle create(
                    Family f = af_ipv4,
                    Type t = sock_stream,
                    Protocol p = ipproto_tcp
                    );
}

class ELW_EXPORT Socket
{
protected:
    sock::Handle m_hSocket;
    sock::Family m_family;
    sock::Type m_type;
    sock::Protocol m_proto;

public:
    Socket(sock::Handle hSocket = sock::invalid_handle);
    Socket(Socket const& that);
    virtual ~Socket();

public:
    bool connect(
            SockAddr const& addr
            );
    bool connect(
            sockaddr const* pAddr,
            socklen_t nLen
            );
    bool bind(
            SockAddr const& addr
            );
    bool bind(
            sockaddr const* pAddr,
            socklen_t nLen
            );
    bool listen(
            int iBacktrace = 5
            );

    sock::Handle accept();

    sock::Handle accept(
            SockAddr& addr
            );

    sock::Handle accept(
            sockaddr* pAddr,
            socklen_t* pnLen
            );
public:
    int send(
            lang::ByteBuffer const& buff,
            sock::Flag f = sock::msg_none
            );
    int send(
            void const* pvBuff,
            size_t nSize,
            sock::Flag f = sock::msg_none
            );
    int sendto(
            lang::ByteBuffer const& buff,
            SockAddr const& addr,
            sock::Flag f = sock::msg_none
            );
    int sendto(
            void const* pvBuff,
            size_t nBuffSize,
            SockAddr const& addr,
            sock::Flag f = sock::msg_none
            );
    int sendto(
            void const* pvBuff, size_t nBuffSize,
            sockaddr const* pAddr, socklen_t nLen,
            sock::Flag f
            );
    int sendmsg(
            msghdr const* pMsg,
            sock::Flag f = sock::msg_none
            );

    int recv(
            lang::ByteBuffer& buff,
            sock::Flag f = sock::msg_none
            );
    int recv(
            void* pvBuff,
            size_t nSize,
            sock::Flag f = sock::msg_none
            );
    int recvfrom(
            lang::ByteBuffer& buff,
            SockAddr& addr,
            sock::Flag f = sock::msg_none
            );
    int recvfrom(
            void* pvBuff,
            size_t nBuffSize,
            SockAddr& addr,
            sock::Flag f = sock::msg_none
            );
    int recvfrom(
            void* pvBuff,
            size_t nBuffSize,
            sockaddr* pAddr,
            socklen_t* pnLen,
            sock::Flag f = sock::msg_none
            );
    int recvmsg(
            msghdr* pMsg,
            sock::Flag f = sock::msg_none
            );

public:
    bool shutdown(
            sock::Shutmode sm = sock::sm_rw
            );

public:
    bool close();

public:
    bool getpeername(
            SockAddr& addr
            );
    bool getsockname(
            SockAddr& addr
            ) const;

public:
    bool setopt(
            int iLevel,
            int iOption,
            void const* pvValue,
            socklen_t nSizeOf
            );

    bool getopt(
            int iLevel,
            int iOption,
            void* pvValue,
            socklen_t* pnSizeOf
            ) const;

    bool ioctl(
            dword dwRequest,
            void* pvArgs
            );

public:
    sock::Type type();
    sock::Family family();
    sock::Protocol protocol();

public:
    sock::Handle handle() const;
    bool isValid() const;
};

inline
sock::Handle Socket::handle() const
{
    return m_hSocket;
}

inline
bool Socket::isValid() const
{
    return m_hSocket != sock::invalid_handle;
}

}
}

#endif /* ElwingSocket_h__ */
